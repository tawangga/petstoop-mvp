package com.tawangga.petstoopmvp.ModelNew;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetGroomingService {

    /**
     * STATUS_CODE : 200
     * MESSAGE : Success
     * DATA : {"page":1,"last_page":1,"total_item":3,"limit":"10","items":[{"grooming_id":1,"grooming_name":"SPA Kucing biasa","grooming_price":"100000.00","grooming_desc":"- Memandikan Kucing","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-22 15:50:08","updated_at":"2019-05-22 15:55:25"},{"grooming_id":2,"grooming_name":"SPA Kucing + Shampo anti kutu","grooming_price":"250000.00","grooming_desc":"- Mandi Kucing\n- Sisir Kucing\n- Shampoan anti kutu","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-22 15:54:41","updated_at":"2019-05-22 15:55:36"},{"grooming_id":3,"grooming_name":"SPA Kucing full Paket","grooming_price":"450000.00","grooming_desc":"-","created_by":4,"updated_by":0,"deleted_at":null,"created_at":"2019-05-22 15:56:10","updated_at":"2019-05-22 15:56:10"}]}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * page : 1
         * last_page : 1
         * total_item : 3
         * limit : 10
         * items : [{"grooming_id":1,"grooming_name":"SPA Kucing biasa","grooming_price":"100000.00","grooming_desc":"- Memandikan Kucing","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-22 15:50:08","updated_at":"2019-05-22 15:55:25"},{"grooming_id":2,"grooming_name":"SPA Kucing + Shampo anti kutu","grooming_price":"250000.00","grooming_desc":"- Mandi Kucing\n- Sisir Kucing\n- Shampoan anti kutu","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-22 15:54:41","updated_at":"2019-05-22 15:55:36"},{"grooming_id":3,"grooming_name":"SPA Kucing full Paket","grooming_price":"450000.00","grooming_desc":"-","created_by":4,"updated_by":0,"deleted_at":null,"created_at":"2019-05-22 15:56:10","updated_at":"2019-05-22 15:56:10"}]
         */

        @SerializedName("page")
        private int page;
        @SerializedName("last_page")
        private int lastPage;
        @SerializedName("total_item")
        private int totalItem;
        @SerializedName("limit")
        private String limit;
        @SerializedName("items")
        private List<ItemsBean> items;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public int getTotalItem() {
            return totalItem;
        }

        public void setTotalItem(int totalItem) {
            this.totalItem = totalItem;
        }

        public String getLimit() {
            return limit;
        }

        public void setLimit(String limit) {
            this.limit = limit;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class ItemsBean {
            /**
             * grooming_id : 1
             * grooming_name : SPA Kucing biasa
             * grooming_price : 100000.00
             * grooming_desc : - Memandikan Kucing
             * created_by : 4
             * updated_by : 4
             * deleted_at : null
             * created_at : 2019-05-22 15:50:08
             * updated_at : 2019-05-22 15:55:25
             */

            @SerializedName("grooming_id")
            private int groomingId;
            @SerializedName("grooming_name")
            private String groomingName;
            @SerializedName("grooming_price")
            private String groomingPrice;
            @SerializedName("grooming_desc")
            private String groomingDesc;
            @SerializedName("created_by")
            private int createdBy;
            @SerializedName("updated_by")
            private int updatedBy;
            @SerializedName("deleted_at")
            private String deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
            private boolean isAdded = false;

            public int getGroomingId() {
                return groomingId;
            }

            public void setGroomingId(int groomingId) {
                this.groomingId = groomingId;
            }

            public String getGroomingName() {
                return groomingName;
            }

            public void setGroomingName(String groomingName) {
                this.groomingName = groomingName;
            }

            public String getGroomingPrice() {
                return groomingPrice;
            }

            public void setGroomingPrice(String groomingPrice) {
                this.groomingPrice = groomingPrice;
            }

            public String getGroomingDesc() {
                return groomingDesc;
            }

            public void setGroomingDesc(String groomingDesc) {
                this.groomingDesc = groomingDesc;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(String deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public void setAdded(boolean added) {
                isAdded = added;
            }

            public boolean isAdded() {
                return isAdded;
            }
        }
    }
}
