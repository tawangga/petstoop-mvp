package com.tawangga.petstoopmvp.ModelNew;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetAppVersion {
    /**
     * STATUS_CODE : 200
     * MESSAGE : Success
     * DATA : {"page":1,"last_page":1,"total_item":3,"limit":10,"items":[{"version_id":3,"version_name":"Version 1.2","change_log":"- Fix previous bugs\n- Change some  logical","created_by":4,"updated_by":0,"deleted_at":null,"created_at":"2019-06-20 15:20:45","updated_at":"2019-06-20 15:20:45"},{"version_id":2,"version_name":"Version 1.1","change_log":"- Fix issues\n- Add some bug for next maintance","created_by":4,"updated_by":0,"deleted_at":null,"created_at":"2019-06-20 15:19:53","updated_at":"2019-06-20 15:19:53"},{"version_id":1,"version_name":"Version 1.0","change_log":"Just the begining","created_by":4,"updated_by":0,"deleted_at":null,"created_at":"2019-06-20 15:14:32","updated_at":"2019-06-20 15:14:32"}]}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * page : 1
         * last_page : 1
         * total_item : 3
         * limit : 10
         * items : [{"version_id":3,"version_name":"Version 1.2","change_log":"- Fix previous bugs\n- Change some  logical","created_by":4,"updated_by":0,"deleted_at":null,"created_at":"2019-06-20 15:20:45","updated_at":"2019-06-20 15:20:45"},{"version_id":2,"version_name":"Version 1.1","change_log":"- Fix issues\n- Add some bug for next maintance","created_by":4,"updated_by":0,"deleted_at":null,"created_at":"2019-06-20 15:19:53","updated_at":"2019-06-20 15:19:53"},{"version_id":1,"version_name":"Version 1.0","change_log":"Just the begining","created_by":4,"updated_by":0,"deleted_at":null,"created_at":"2019-06-20 15:14:32","updated_at":"2019-06-20 15:14:32"}]
         */

        @SerializedName("page")
        private int page;
        @SerializedName("last_page")
        private int lastPage;
        @SerializedName("total_item")
        private int totalItem;
        @SerializedName("limit")
        private int limit;
        @SerializedName("items")
        private List<ItemsBean> items;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public int getTotalItem() {
            return totalItem;
        }

        public void setTotalItem(int totalItem) {
            this.totalItem = totalItem;
        }

        public int getLimit() {
            return limit;
        }

        public void setLimit(int limit) {
            this.limit = limit;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class ItemsBean {
            /**
             * version_id : 3
             * version_name : Version 1.2
             * change_log : - Fix previous bugs
             - Change some  logical
             * created_by : 4
             * updated_by : 0
             * deleted_at : null
             * created_at : 2019-06-20 15:20:45
             * updated_at : 2019-06-20 15:20:45
             */

            @SerializedName("version_id")
            private int versionId;
            @SerializedName("version_name")
            private String versionName;
            @SerializedName("change_log")
            private String changeLog;
            @SerializedName("created_by")
            private int createdBy;
            @SerializedName("updated_by")
            private int updatedBy;
            @SerializedName("deleted_at")
            private Object deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;

            public int getVersionId() {
                return versionId;
            }

            public void setVersionId(int versionId) {
                this.versionId = versionId;
            }

            public String getVersionName() {
                return versionName;
            }

            public void setVersionName(String versionName) {
                this.versionName = versionName;
            }

            public String getChangeLog() {
                return changeLog;
            }

            public void setChangeLog(String changeLog) {
                this.changeLog = changeLog;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public Object getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(Object deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }
        }
    }
}
