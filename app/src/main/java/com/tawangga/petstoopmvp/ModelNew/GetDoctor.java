package com.tawangga.petstoopmvp.ModelNew;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetDoctor {
    /**
     * STATUS_CODE : 200
     * MESSAGE : Success
     * DATA : {"page":1,"last_page":1,"total_item":2,"limit":"7","items":[{"staff_id":3,"name":"Edinofri Karizal Caniago","time":"12:00","date":"2019-05-01","is_available":true},{"staff_id":3,"name":"Edinofri Karizal Caniago","time":"13:00","date":"2019-05-01","is_available":true},{"staff_id":4,"name":"Salena Salihun","time":"13:05","date":"2019-05-01","is_available":true},{"staff_id":3,"name":"Edinofri Karizal Caniago","time":"14:00","date":"2019-05-01","is_available":true},{"staff_id":4,"name":"Salena Salihun","time":"14:05","date":"2019-05-01","is_available":true},{"staff_id":3,"name":"Edinofri Karizal Caniago","time":"15:00","date":"2019-05-01","is_available":true},{"staff_id":4,"name":"Salena Salihun","time":"15:05","date":"2019-05-01","is_available":true},{"staff_id":3,"name":"Edinofri Karizal Caniago","time":"16:00","date":"2019-05-01","is_available":true},{"staff_id":4,"name":"Salena Salihun","time":"16:05","date":"2019-05-01","is_available":true},{"staff_id":3,"name":"Edinofri Karizal Caniago","time":"17:00","date":"2019-05-01","is_available":true},{"staff_id":4,"name":"Salena Salihun","time":"17:05","date":"2019-05-01","is_available":true},{"staff_id":3,"name":"Edinofri Karizal Caniago","time":"18:00","date":"2019-05-01","is_available":true},{"staff_id":4,"name":"Salena Salihun","time":"18:05","date":"2019-05-01","is_available":true},{"staff_id":3,"name":"Edinofri Karizal Caniago","time":"19:00","date":"2019-05-01","is_available":true},{"staff_id":4,"name":"Salena Salihun","time":"19:05","date":"2019-05-01","is_available":true},{"staff_id":3,"name":"Edinofri Karizal Caniago","time":"20:00","date":"2019-05-01","is_available":true},{"staff_id":4,"name":"Salena Salihun","time":"20:05","date":"2019-05-01","is_available":true},{"staff_id":4,"name":"Salena Salihun","time":"21:05","date":"2019-05-01","is_available":true},{"staff_id":4,"name":"Salena Salihun","time":"22:05","date":"2019-05-01","is_available":true}]}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * page : 1
         * last_page : 1
         * total_item : 2
         * limit : 7
         * items : [{"staff_id":3,"name":"Edinofri Karizal Caniago","time":"12:00","date":"2019-05-01","is_available":true},{"staff_id":3,"name":"Edinofri Karizal Caniago","time":"13:00","date":"2019-05-01","is_available":true},{"staff_id":4,"name":"Salena Salihun","time":"13:05","date":"2019-05-01","is_available":true},{"staff_id":3,"name":"Edinofri Karizal Caniago","time":"14:00","date":"2019-05-01","is_available":true},{"staff_id":4,"name":"Salena Salihun","time":"14:05","date":"2019-05-01","is_available":true},{"staff_id":3,"name":"Edinofri Karizal Caniago","time":"15:00","date":"2019-05-01","is_available":true},{"staff_id":4,"name":"Salena Salihun","time":"15:05","date":"2019-05-01","is_available":true},{"staff_id":3,"name":"Edinofri Karizal Caniago","time":"16:00","date":"2019-05-01","is_available":true},{"staff_id":4,"name":"Salena Salihun","time":"16:05","date":"2019-05-01","is_available":true},{"staff_id":3,"name":"Edinofri Karizal Caniago","time":"17:00","date":"2019-05-01","is_available":true},{"staff_id":4,"name":"Salena Salihun","time":"17:05","date":"2019-05-01","is_available":true},{"staff_id":3,"name":"Edinofri Karizal Caniago","time":"18:00","date":"2019-05-01","is_available":true},{"staff_id":4,"name":"Salena Salihun","time":"18:05","date":"2019-05-01","is_available":true},{"staff_id":3,"name":"Edinofri Karizal Caniago","time":"19:00","date":"2019-05-01","is_available":true},{"staff_id":4,"name":"Salena Salihun","time":"19:05","date":"2019-05-01","is_available":true},{"staff_id":3,"name":"Edinofri Karizal Caniago","time":"20:00","date":"2019-05-01","is_available":true},{"staff_id":4,"name":"Salena Salihun","time":"20:05","date":"2019-05-01","is_available":true},{"staff_id":4,"name":"Salena Salihun","time":"21:05","date":"2019-05-01","is_available":true},{"staff_id":4,"name":"Salena Salihun","time":"22:05","date":"2019-05-01","is_available":true}]
         */

        @SerializedName("page")
        private int page;
        @SerializedName("last_page")
        private int lastPage;
        @SerializedName("total_item")
        private int totalItem;
        @SerializedName("limit")
        private String limit;
        @SerializedName("items")
        private List<ItemsBean> items;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public int getTotalItem() {
            return totalItem;
        }

        public void setTotalItem(int totalItem) {
            this.totalItem = totalItem;
        }

        public String getLimit() {
            return limit;
        }

        public void setLimit(String limit) {
            this.limit = limit;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class ItemsBean {
            /**
             * staff_id : 3
             * name : Edinofri Karizal Caniago
             * time : 12:00
             * date : 2019-05-01
             * is_available : true
             */

            @SerializedName("staff_id")
            private int staffId;
            @SerializedName("name")
            private String name;
            @SerializedName("time")
            private String time;
            @SerializedName("date")
            private String date;
            @SerializedName("is_available")
            private boolean isAvailable;

            public int getStaffId() {
                return staffId;
            }

            public void setStaffId(int staffId) {
                this.staffId = staffId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public boolean isIsAvailable() {
                return isAvailable;
            }

            public void setIsAvailable(boolean isAvailable) {
                this.isAvailable = isAvailable;
            }
        }
    }
}
