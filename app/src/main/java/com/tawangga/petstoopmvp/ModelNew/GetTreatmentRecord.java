package com.tawangga.petstoopmvp.ModelNew;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetTreatmentRecord {
    /**
     * STATUS_CODE : 200
     * MESSAGE : Success
     * DATA : [{"pet_id":1,"order_code":"R#1559019316-1","order_date":"2019-06-19","service_type":"Medical","service_name":"Tricat vaccine","service_id":2},{"pet_id":1,"order_code":"R#1559019316-1","order_date":"2019-06-19","service_type":"Grooming","service_name":"SPA Kucing biasa","service_id":1},{"pet_id":1,"order_code":"R#1559019316-1","order_date":"2019-06-19","service_type":"Grooming","service_name":"SPA Kucing + Shampo anti kutu","service_id":2},{"pet_id":1,"order_code":"R#1559019316-1","order_date":"2019-06-19","service_type":"Boarding","service_name":"Kandang Kucing S","service_id":1},{"pet_id":1,"order_code":"R#1559019316-1","order_date":"2019-06-19","service_type":"Medical","service_name":"Tetracat vaccine","service_id":3}]
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private List<DATABean> DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public List<DATABean> getDATA() {
        return DATA;
    }

    public void setDATA(List<DATABean> DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * pet_id : 1
         * order_code : R#1559019316-1
         * order_date : 2019-06-19
         * service_type : Medical
         * service_name : Tricat vaccine
         * service_id : 2
         */

        @SerializedName("pet_id")
        private int petId;
        @SerializedName("order_code")
        private String orderCode;
        @SerializedName("order_date")
        private String orderDate;
        @SerializedName("service_type")
        private String serviceType;
        @SerializedName("service_name")
        private String serviceName;
        @SerializedName("service_id")
        private int serviceId;
        @SerializedName("service_text")
        private String serviceText;
        @SerializedName("customer_name")
        private String customerName;

        public int getPetId() {
            return petId;
        }

        public void setPetId(int petId) {
            this.petId = petId;
        }

        public String getOrderCode() {
            return orderCode;
        }

        public void setOrderCode(String orderCode) {
            this.orderCode = orderCode;
        }

        public String getOrderDate() {
            return orderDate;
        }

        public void setOrderDate(String orderDate) {
            this.orderDate = orderDate;
        }

        public String getServiceType() {
            return serviceType;
        }

        public void setServiceType(String serviceType) {
            this.serviceType = serviceType;
        }

        public String getServiceName() {
            return serviceName;
        }

        public void setServiceName(String serviceName) {
            this.serviceName = serviceName;
        }

        public int getServiceId() {
            return serviceId;
        }

        public void setServiceId(int serviceId) {
            this.serviceId = serviceId;
        }

        public String getServiceText() {
            return serviceText;
        }

        public void setServiceText(String serviceText) {
            this.serviceText = serviceText;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }
    }
}
