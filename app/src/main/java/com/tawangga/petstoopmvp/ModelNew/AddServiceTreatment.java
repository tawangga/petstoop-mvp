package com.tawangga.petstoopmvp.ModelNew;

import com.google.gson.annotations.SerializedName;

public class AddServiceTreatment {
    /**
     * STATUS_CODE : 200
     * MESSAGE : SUCCESS ADDED
     * DATA : {"cart_service_id":"3","service_id":"1","service_type":"2","service_name":"SPA Kucing biasa","service_price":"100000.00","created_by":"1","updated_at":"2019-05-27 05:44:59","created_at":"2019-05-27 05:44:59","cart_service_detail_id":20}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private CreateCart.DATABean.ServiceBean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public CreateCart.DATABean.ServiceBean getDATA() {
        return DATA;
    }

    public void setDATA(CreateCart.DATABean.ServiceBean DATA) {
        this.DATA = DATA;
    }


}
