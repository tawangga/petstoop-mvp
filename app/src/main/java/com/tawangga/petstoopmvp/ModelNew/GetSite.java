package com.tawangga.petstoopmvp.ModelNew;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetSite {
    /**
     * STATUS_CODE : 200
     * MESSAGE : Success
     * DATA : {"page":1,"last_page":1,"total_item":3,"limit":10,"items":[{"user_id":37,"username":"client1site1","email":"client1site1@gmail.com","user_group_id":3,"photo":"user-1565343561.png","created_by":34,"updated_by":1,"created_at":"2019-09-10 09:55:51","updated_at":"2019-09-10 09:55:51","user_site_id":1,"client_id":34,"site_name":"Site 1","site_address":"Jl.Buntu no 123","site_city":"Aceh","site_manager":"Lorem","remark":"-","role":{"user_group_id":3,"user_group_name":"Site"}},{"user_id":38,"username":"client1site2","email":"client1site2@gmail.com","user_group_id":3,"photo":"user-1565343717.png","created_by":34,"updated_by":0,"created_at":"2019-08-09 09:41:57","updated_at":"2019-08-09 09:41:57","user_site_id":2,"client_id":34,"site_name":"Site 2","site_address":"-","site_city":"Medan","site_manager":"Lorem","remark":"-","role":{"user_group_id":3,"user_group_name":"Site"}},{"user_id":48,"username":"tesss","email":"tess@gmail.com","user_group_id":3,"photo":"user-1568099811.png","created_by":34,"updated_by":0,"created_at":"2019-09-10 07:16:51","updated_at":"2019-09-10 07:16:51","user_site_id":9,"client_id":34,"site_name":"tesss","site_address":"fhfmghmmhgmh","site_city":"fgnfnn","site_manager":"ghmgmhm","remark":"ghmhgmhgm","role":{"user_group_id":3,"user_group_name":"Site"}}]}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * page : 1
         * last_page : 1
         * total_item : 3
         * limit : 10
         * items : [{"user_id":37,"username":"client1site1","email":"client1site1@gmail.com","user_group_id":3,"photo":"user-1565343561.png","created_by":34,"updated_by":1,"created_at":"2019-09-10 09:55:51","updated_at":"2019-09-10 09:55:51","user_site_id":1,"client_id":34,"site_name":"Site 1","site_address":"Jl.Buntu no 123","site_city":"Aceh","site_manager":"Lorem","remark":"-","role":{"user_group_id":3,"user_group_name":"Site"}},{"user_id":38,"username":"client1site2","email":"client1site2@gmail.com","user_group_id":3,"photo":"user-1565343717.png","created_by":34,"updated_by":0,"created_at":"2019-08-09 09:41:57","updated_at":"2019-08-09 09:41:57","user_site_id":2,"client_id":34,"site_name":"Site 2","site_address":"-","site_city":"Medan","site_manager":"Lorem","remark":"-","role":{"user_group_id":3,"user_group_name":"Site"}},{"user_id":48,"username":"tesss","email":"tess@gmail.com","user_group_id":3,"photo":"user-1568099811.png","created_by":34,"updated_by":0,"created_at":"2019-09-10 07:16:51","updated_at":"2019-09-10 07:16:51","user_site_id":9,"client_id":34,"site_name":"tesss","site_address":"fhfmghmmhgmh","site_city":"fgnfnn","site_manager":"ghmgmhm","remark":"ghmhgmhgm","role":{"user_group_id":3,"user_group_name":"Site"}}]
         */

        @SerializedName("page")
        private int page;
        @SerializedName("last_page")
        private int lastPage;
        @SerializedName("total_item")
        private int totalItem;
        @SerializedName("limit")
        private int limit;
        @SerializedName("items")
        private List<ItemsBean> items;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public int getTotalItem() {
            return totalItem;
        }

        public void setTotalItem(int totalItem) {
            this.totalItem = totalItem;
        }

        public int getLimit() {
            return limit;
        }

        public void setLimit(int limit) {
            this.limit = limit;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class ItemsBean {
            /**
             * user_id : 37
             * username : client1site1
             * email : client1site1@gmail.com
             * user_group_id : 3
             * photo : user-1565343561.png
             * created_by : 34
             * updated_by : 1
             * created_at : 2019-09-10 09:55:51
             * updated_at : 2019-09-10 09:55:51
             * user_site_id : 1
             * client_id : 34
             * site_name : Site 1
             * site_address : Jl.Buntu no 123
             * site_city : Aceh
             * site_manager : Lorem
             * remark : -
             * role : {"user_group_id":3,"user_group_name":"Site"}
             */

            @SerializedName("user_id")
            private int userId;
            @SerializedName("username")
            private String username;
            @SerializedName("email")
            private String email;
            @SerializedName("user_group_id")
            private int userGroupId;
            @SerializedName("photo")
            private String photo;
            @SerializedName("created_by")
            private int createdBy;
            @SerializedName("updated_by")
            private int updatedBy;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
            @SerializedName("user_site_id")
            private int userSiteId;
            @SerializedName("client_id")
            private int clientId;
            @SerializedName("site_name")
            private String siteName;
            @SerializedName("site_address")
            private String siteAddress;
            @SerializedName("site_city")
            private String siteCity;
            @SerializedName("site_manager")
            private String siteManager;
            @SerializedName("remark")
            private String remark;
            @SerializedName("role")
            private RoleBean role;

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public int getUserGroupId() {
                return userGroupId;
            }

            public void setUserGroupId(int userGroupId) {
                this.userGroupId = userGroupId;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public int getUserSiteId() {
                return userSiteId;
            }

            public void setUserSiteId(int userSiteId) {
                this.userSiteId = userSiteId;
            }

            public int getClientId() {
                return clientId;
            }

            public void setClientId(int clientId) {
                this.clientId = clientId;
            }

            public String getSiteName() {
                return siteName;
            }

            public void setSiteName(String siteName) {
                this.siteName = siteName;
            }

            public String getSiteAddress() {
                return siteAddress;
            }

            public void setSiteAddress(String siteAddress) {
                this.siteAddress = siteAddress;
            }

            public String getSiteCity() {
                return siteCity;
            }

            public void setSiteCity(String siteCity) {
                this.siteCity = siteCity;
            }

            public String getSiteManager() {
                return siteManager;
            }

            public void setSiteManager(String siteManager) {
                this.siteManager = siteManager;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public RoleBean getRole() {
                return role;
            }

            public void setRole(RoleBean role) {
                this.role = role;
            }

            public static class RoleBean {
                /**
                 * user_group_id : 3
                 * user_group_name : Site
                 */

                @SerializedName("user_group_id")
                private int userGroupId;
                @SerializedName("user_group_name")
                private String userGroupName;

                public int getUserGroupId() {
                    return userGroupId;
                }

                public void setUserGroupId(int userGroupId) {
                    this.userGroupId = userGroupId;
                }

                public String getUserGroupName() {
                    return userGroupName;
                }

                public void setUserGroupName(String userGroupName) {
                    this.userGroupName = userGroupName;
                }
            }
        }
    }
}
