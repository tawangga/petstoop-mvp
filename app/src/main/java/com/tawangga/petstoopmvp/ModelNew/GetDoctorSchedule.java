package com.tawangga.petstoopmvp.ModelNew;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetDoctorSchedule {
    /**
     * STATUS_CODE : 200
     * MESSAGE : Success
     * DATA : {"page":1,"last_page":1,"total_item":2,"limit":"15","items":[{"staff_id":3,"name":"Edinofri Karizal Caniago","email":"tukangbasic@gmail.com","phone":"082368338355","photo":"http://178.128.62.50/petv2/api/public/images/staff-1557976079.png","address":"-\n-","doctor":{"staff_doctor_id":5,"staff_id":3,"sun":0,"mon":1,"tue":1,"wed":0,"thu":0,"fri":0,"sat":0,"time_operational_start":"12:00:00","time_operational_end":"20:00:00","workingdays":"Monday,Tuesday"}},{"staff_id":4,"name":"Salena Salihun","email":"salena.salihun@mail.com","phone":"05889666","photo":"http://178.128.62.50/petv2/api/public/images/staff-1557976123.png","address":"-","doctor":{"staff_doctor_id":6,"staff_id":4,"sun":0,"mon":1,"tue":1,"wed":1,"thu":0,"fri":0,"sat":0,"time_operational_start":"13:05:00","time_operational_end":"22:50:00","workingdays":"Monday,Tuesday,Wednesday"}}]}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * page : 1
         * last_page : 1
         * total_item : 2
         * limit : 15
         * items : [{"staff_id":3,"name":"Edinofri Karizal Caniago","email":"tukangbasic@gmail.com","phone":"082368338355","photo":"http://178.128.62.50/petv2/api/public/images/staff-1557976079.png","address":"-\n-","doctor":{"staff_doctor_id":5,"staff_id":3,"sun":0,"mon":1,"tue":1,"wed":0,"thu":0,"fri":0,"sat":0,"time_operational_start":"12:00:00","time_operational_end":"20:00:00","workingdays":"Monday,Tuesday"}},{"staff_id":4,"name":"Salena Salihun","email":"salena.salihun@mail.com","phone":"05889666","photo":"http://178.128.62.50/petv2/api/public/images/staff-1557976123.png","address":"-","doctor":{"staff_doctor_id":6,"staff_id":4,"sun":0,"mon":1,"tue":1,"wed":1,"thu":0,"fri":0,"sat":0,"time_operational_start":"13:05:00","time_operational_end":"22:50:00","workingdays":"Monday,Tuesday,Wednesday"}}]
         */

        @SerializedName("page")
        private int page;
        @SerializedName("last_page")
        private int lastPage;
        @SerializedName("total_item")
        private int totalItem;
        @SerializedName("limit")
        private String limit;
        @SerializedName("items")
        private List<ItemsBean> items;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public int getTotalItem() {
            return totalItem;
        }

        public void setTotalItem(int totalItem) {
            this.totalItem = totalItem;
        }

        public String getLimit() {
            return limit;
        }

        public void setLimit(String limit) {
            this.limit = limit;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class ItemsBean {
            /**
             * staff_id : 3
             * name : Edinofri Karizal Caniago
             * email : tukangbasic@gmail.com
             * phone : 082368338355
             * photo : http://178.128.62.50/petv2/api/public/images/staff-1557976079.png
             * address : -
             -
             * doctor : {"staff_doctor_id":5,"staff_id":3,"sun":0,"mon":1,"tue":1,"wed":0,"thu":0,"fri":0,"sat":0,"time_operational_start":"12:00:00","time_operational_end":"20:00:00","workingdays":"Monday,Tuesday"}
             */

            @SerializedName("staff_id")
            private int staffId;
            @SerializedName("name")
            private String name;
            @SerializedName("email")
            private String email;
            @SerializedName("phone")
            private String phone;
            @SerializedName("photo")
            private String photo;
            @SerializedName("address")
            private String address;
            @SerializedName("doctor")
            private DoctorBean doctor;

            public int getStaffId() {
                return staffId;
            }

            public void setStaffId(int staffId) {
                this.staffId = staffId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public DoctorBean getDoctor() {
                return doctor;
            }

            public void setDoctor(DoctorBean doctor) {
                this.doctor = doctor;
            }

            public static class DoctorBean {
                /**
                 * staff_doctor_id : 5
                 * staff_id : 3
                 * sun : 0
                 * mon : 1
                 * tue : 1
                 * wed : 0
                 * thu : 0
                 * fri : 0
                 * sat : 0
                 * time_operational_start : 12:00:00
                 * time_operational_end : 20:00:00
                 * workingdays : Monday,Tuesday
                 */

                @SerializedName("staff_doctor_id")
                private int staffDoctorId;
                @SerializedName("staff_id")
                private int staffId;
                @SerializedName("sun")
                private int sun;
                @SerializedName("mon")
                private int mon;
                @SerializedName("tue")
                private int tue;
                @SerializedName("wed")
                private int wed;
                @SerializedName("thu")
                private int thu;
                @SerializedName("fri")
                private int fri;
                @SerializedName("sat")
                private int sat;
                @SerializedName("time_operational_start")
                private String timeOperationalStart;
                @SerializedName("time_operational_end")
                private String timeOperationalEnd;

                @SerializedName("sun_time_operational_start")
                private String sunTimeOperationalStart;
                @SerializedName("mon_time_operational_start")
                private String monTimeOperationalStart;
                @SerializedName("tue_time_operational_start")
                private String tueTimeOperationalStart;
                @SerializedName("wed_time_operational_start")
                private String wedTimeOperationalStart;
                @SerializedName("thu_time_operational_start")
                private String thuTimeOperationalStart;
                @SerializedName("fri_time_operational_start")
                private String friTimeOperationalStart;
                @SerializedName("sat_time_operational_start")
                private String satTimeOperationalStart;

                @SerializedName("sun_time_operational_end")
                private String sunTimeOperationalEnd;
                @SerializedName("mon_time_operational_end")
                private String monTimeOperationalEnd;
                @SerializedName("tue_time_operational_end")
                private String tueTimeOperationalEnd;
                @SerializedName("wed_time_operational_end")
                private String wedTimeOperationalEnd;
                @SerializedName("thu_time_operational_end")
                private String thuTimeOperationalEnd;
                @SerializedName("fri_time_operational_end")
                private String friTimeOperationalEnd;
                @SerializedName("sat_time_operational_end")
                private String satTimeOperationalEnd;
                @SerializedName("workingdays")
                private String workingdays;

                public String getSunTimeOperationalStart() {
                    return sunTimeOperationalStart;
                }

                public String getMonTimeOperationalStart() {
                    return monTimeOperationalStart;
                }

                public String getTueTimeOperationalStart() {
                    return tueTimeOperationalStart;
                }

                public String getWedTimeOperationalStart() {
                    return wedTimeOperationalStart;
                }

                public String getThuTimeOperationalStart() {
                    return thuTimeOperationalStart;
                }

                public String getFriTimeOperationalStart() {
                    return friTimeOperationalStart;
                }

                public String getSatTimeOperationalStart() {
                    return satTimeOperationalStart;
                }

                public String getSunTimeOperationalEnd() {
                    return sunTimeOperationalEnd;
                }

                public String getMonTimeOperationalEnd() {
                    return monTimeOperationalEnd;
                }

                public String getTueTimeOperationalEnd() {
                    return tueTimeOperationalEnd;
                }

                public String getWedTimeOperationalEnd() {
                    return wedTimeOperationalEnd;
                }

                public String getThuTimeOperationalEnd() {
                    return thuTimeOperationalEnd;
                }

                public String getFriTimeOperationalEnd() {
                    return friTimeOperationalEnd;
                }

                public String getSatTimeOperationalEnd() {
                    return satTimeOperationalEnd;
                }

                public int getStaffDoctorId() {
                    return staffDoctorId;
                }

                public void setStaffDoctorId(int staffDoctorId) {
                    this.staffDoctorId = staffDoctorId;
                }

                public int getStaffId() {
                    return staffId;
                }

                public void setStaffId(int staffId) {
                    this.staffId = staffId;
                }

                public int getSun() {
                    return sun;
                }

                public void setSun(int sun) {
                    this.sun = sun;
                }

                public int getMon() {
                    return mon;
                }

                public void setMon(int mon) {
                    this.mon = mon;
                }

                public int getTue() {
                    return tue;
                }

                public void setTue(int tue) {
                    this.tue = tue;
                }

                public int getWed() {
                    return wed;
                }

                public void setWed(int wed) {
                    this.wed = wed;
                }

                public int getThu() {
                    return thu;
                }

                public void setThu(int thu) {
                    this.thu = thu;
                }

                public int getFri() {
                    return fri;
                }

                public void setFri(int fri) {
                    this.fri = fri;
                }

                public int getSat() {
                    return sat;
                }

                public void setSat(int sat) {
                    this.sat = sat;
                }

                public String getTimeOperationalStart() {
                    return timeOperationalStart;
                }

                public void setTimeOperationalStart(String timeOperationalStart) {
                    this.timeOperationalStart = timeOperationalStart;
                }

                public String getTimeOperationalEnd() {
                    return timeOperationalEnd;
                }

                public void setTimeOperationalEnd(String timeOperationalEnd) {
                    this.timeOperationalEnd = timeOperationalEnd;
                }

                public String getWorkingdays() {
                    return workingdays;
                }

                public void setWorkingdays(String workingdays) {
                    this.workingdays = workingdays;
                }
            }
        }
    }
}
