package com.tawangga.petstoopmvp.ModelNew;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetMedicalService {
    /**
     * STATUS_CODE : 200
     * MESSAGE : Success
     * DATA : {"page":1,"last_page":1,"total_item":1,"limit":"10","items":[{"medical_id":1,"medical_name":"Konsultasi Dokter","medical_price":"165000.00","medical_desc":"-","created_by":4,"updated_by":4,"deleted_at":"","created_at":"2019-05-24 09:40:35","updated_at":"2019-05-24 09:42:39"}]}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * page : 1
         * last_page : 1
         * total_item : 1
         * limit : 10
         * items : [{"medical_id":1,"medical_name":"Konsultasi Dokter","medical_price":"165000.00","medical_desc":"-","created_by":4,"updated_by":4,"deleted_at":"","created_at":"2019-05-24 09:40:35","updated_at":"2019-05-24 09:42:39"}]
         */

        @SerializedName("page")
        private int page;
        @SerializedName("last_page")
        private int lastPage;
        @SerializedName("total_item")
        private int totalItem;
        @SerializedName("limit")
        private String limit;
        @SerializedName("items")
        private List<ItemsBean> items;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public int getTotalItem() {
            return totalItem;
        }

        public void setTotalItem(int totalItem) {
            this.totalItem = totalItem;
        }

        public String getLimit() {
            return limit;
        }

        public void setLimit(String limit) {
            this.limit = limit;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class ItemsBean {
            /**
             * medical_id : 1
             * medical_name : Konsultasi Dokter
             * medical_price : 165000.00
             * medical_desc : -
             * created_by : 4
             * updated_by : 4
             * deleted_at :
             * created_at : 2019-05-24 09:40:35
             * updated_at : 2019-05-24 09:42:39
             */

            @SerializedName("medical_id")
            private int medicalId;
            @SerializedName("medical_name")
            private String medicalName;
            @SerializedName("medical_price")
            private String medicalPrice;
            @SerializedName("medical_desc")
            private String medicalDesc;
            @SerializedName("created_by")
            private int createdBy;
            @SerializedName("updated_by")
            private int updatedBy;
            @SerializedName("deleted_at")
            private String deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
            private boolean isAdded = false;

            public int getMedicalId() {
                return medicalId;
            }

            public void setMedicalId(int medicalId) {
                this.medicalId = medicalId;
            }

            public String getMedicalName() {
                return medicalName;
            }

            public void setMedicalName(String medicalName) {
                this.medicalName = medicalName;
            }

            public String getMedicalPrice() {
                return medicalPrice;
            }

            public void setMedicalPrice(String medicalPrice) {
                this.medicalPrice = medicalPrice;
            }

            public String getMedicalDesc() {
                return medicalDesc;
            }

            public void setMedicalDesc(String medicalDesc) {
                this.medicalDesc = medicalDesc;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(String deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public boolean isAdded() {
                return isAdded;
            }

            public void setAdded(boolean added) {
                isAdded = added;
            }
        }
    }
}
