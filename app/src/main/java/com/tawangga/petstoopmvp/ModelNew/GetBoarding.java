package com.tawangga.petstoopmvp.ModelNew;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetBoarding {
    /**
     * STATUS_CODE : 200
     * MESSAGE : Success
     * DATA : {"page":1,"last_page":1,"total_item":4,"limit":"10","items":[{"cage_id":1,"cage_name":"Kandang Kucing S","cage_size":"1","cage_price":"150000","cage_desc":"-","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-22 11:30:41","updated_at":"2019-05-22 11:39:08","is_available":false},{"cage_id":2,"cage_name":"Kandang Kucing L","cage_size":"2","cage_price":"200000","cage_desc":"-","created_by":4,"updated_by":0,"deleted_at":null,"created_at":"2019-05-22 11:31:03","updated_at":"2019-05-22 11:31:03","is_available":true},{"cage_id":3,"cage_name":"Kandang Kucing XL","cage_size":"3","cage_price":"300000","cage_desc":"-","created_by":4,"updated_by":0,"deleted_at":null,"created_at":"2019-05-22 11:31:23","updated_at":"2019-05-22 11:31:23","is_available":true},{"cage_id":4,"cage_name":"Kandang Kucing S Komplit","cage_size":"1","cage_price":"230000","cage_desc":"Komplit mainan dan pasir pup","created_by":4,"updated_by":0,"deleted_at":"","created_at":"2019-05-22 11:31:58","updated_at":"2019-05-22 11:31:58","is_available":true}]}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * page : 1
         * last_page : 1
         * total_item : 4
         * limit : 10
         * items : [{"cage_id":1,"cage_name":"Kandang Kucing S","cage_size":"1","cage_price":"150000","cage_desc":"-","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-22 11:30:41","updated_at":"2019-05-22 11:39:08","is_available":false},{"cage_id":2,"cage_name":"Kandang Kucing L","cage_size":"2","cage_price":"200000","cage_desc":"-","created_by":4,"updated_by":0,"deleted_at":null,"created_at":"2019-05-22 11:31:03","updated_at":"2019-05-22 11:31:03","is_available":true},{"cage_id":3,"cage_name":"Kandang Kucing XL","cage_size":"3","cage_price":"300000","cage_desc":"-","created_by":4,"updated_by":0,"deleted_at":null,"created_at":"2019-05-22 11:31:23","updated_at":"2019-05-22 11:31:23","is_available":true},{"cage_id":4,"cage_name":"Kandang Kucing S Komplit","cage_size":"1","cage_price":"230000","cage_desc":"Komplit mainan dan pasir pup","created_by":4,"updated_by":0,"deleted_at":"","created_at":"2019-05-22 11:31:58","updated_at":"2019-05-22 11:31:58","is_available":true}]
         */

        @SerializedName("page")
        private int page;
        @SerializedName("last_page")
        private int lastPage;
        @SerializedName("total_item")
        private int totalItem;
        @SerializedName("limit")
        private String limit;
        @SerializedName("items")
        private List<ItemsBean> items;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public int getTotalItem() {
            return totalItem;
        }

        public void setTotalItem(int totalItem) {
            this.totalItem = totalItem;
        }

        public String getLimit() {
            return limit;
        }

        public void setLimit(String limit) {
            this.limit = limit;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class ItemsBean {
            /**
             * cage_id : 1
             * cage_name : Kandang Kucing S
             * cage_size : 1
             * cage_price : 150000
             * cage_desc : -
             * created_by : 4
             * updated_by : 4
             * deleted_at : null
             * created_at : 2019-05-22 11:30:41
             * updated_at : 2019-05-22 11:39:08
             * is_available : false
             */

            @SerializedName("cage_id")
            private int cageId;
            @SerializedName("cage_name")
            private String cageName;
            @SerializedName("cage_size")
            private String cageSize;
            @SerializedName("cage_price")
            private String cagePrice;
            @SerializedName("cage_desc")
            private String cageDesc;
            @SerializedName("created_by")
            private int createdBy;
            @SerializedName("updated_by")
            private int updatedBy;
            @SerializedName("deleted_at")
            private String deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
            @SerializedName("is_available")
            private boolean isAvailable;
            @SerializedName("cart_service_id")
            private int cartServiceId;

            public int getCageId() {
                return cageId;
            }

            public void setCageId(int cageId) {
                this.cageId = cageId;
            }

            public String getCageName() {
                return cageName;
            }

            public void setCageName(String cageName) {
                this.cageName = cageName;
            }

            public String getCageSize() {
                return cageSize;
            }

            public void setCageSize(String cageSize) {
                this.cageSize = cageSize;
            }

            public String getCagePrice() {
                return cagePrice;
            }

            public void setCagePrice(String cagePrice) {
                this.cagePrice = cagePrice;
            }

            public String getCageDesc() {
                return cageDesc;
            }

            public void setCageDesc(String cageDesc) {
                this.cageDesc = cageDesc;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(String deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public boolean isIsAvailable() {
                return isAvailable;
            }

            public void setIsAvailable(boolean isAvailable) {
                this.isAvailable = isAvailable;
            }

            public int getCartServiceId() {
                return cartServiceId;
            }

            public void setCartServiceId(int cartServiceId) {
                this.cartServiceId = cartServiceId;
            }
        }
    }
}
