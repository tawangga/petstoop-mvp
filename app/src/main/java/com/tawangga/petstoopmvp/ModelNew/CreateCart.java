package com.tawangga.petstoopmvp.ModelNew;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CreateCart {
    /**
     * STATUS_CODE : 200
     * MESSAGE : SUCCESS, Exist data
     * DATA : {"cart_service_id":2,"cart_service_type":"1","order_code":"R#1558928560-2","pet_id":2,"doctor_schedule_id":"","grooming_location_schedule_id":"","boarding_schedule_id":"","payment_complete":0,"has_anamnesis":0,"take_action":0,"created_by":3,"updated_by":0,"deleted_at":"","created_at":"2019-05-27 10:42:40","updated_at":"2019-05-27 10:42:40","pet":{"customer_name":"Henry S. Browne","pet_id":2,"customer_id":3,"pet_name":"Fitch","pet_race":"Angora","pet_birthday":"2019-01-02","pet_gender":"1","pet_photo":"customer-1557898290.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":"","created_at":"2019-05-15 12:31:30","updated_at":"2019-05-15 12:31:30"},"service_medical":[{"cart_service_detail_id":16,"cart_service_id":2,"service_type":"1","service_id":1,"service_name":"Kolsultasi Dokter","service_price":"125000.00","order_status":"1","is_invoice":0,"created_by":1,"updated_by":0,"deleted_at":"","created_at":"2019-05-27 04:06:36","updated_at":"2019-05-27 04:06:36"}],"service_grooming":[{"cart_service_detail_id":17,"cart_service_id":2,"service_type":"2","service_id":1,"service_name":"SPA Kucing biasa","service_price":"100000.00","order_status":"1","is_invoice":0,"created_by":1,"updated_by":0,"deleted_at":"","created_at":"2019-05-27 04:07:29","updated_at":"2019-05-27 04:07:29"}],"service_boarding":[]}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * cart_service_id : 2
         * cart_service_type : 1
         * order_code : R#1558928560-2
         * pet_id : 2
         * doctor_schedule_id :
         * grooming_location_schedule_id :
         * boarding_schedule_id :
         * payment_complete : 0
         * has_anamnesis : 0
         * take_action : 0
         * created_by : 3
         * updated_by : 0
         * deleted_at :
         * created_at : 2019-05-27 10:42:40
         * updated_at : 2019-05-27 10:42:40
         * pet : {"customer_name":"Henry S. Browne","pet_id":2,"customer_id":3,"pet_name":"Fitch","pet_race":"Angora","pet_birthday":"2019-01-02","pet_gender":"1","pet_photo":"customer-1557898290.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":"","created_at":"2019-05-15 12:31:30","updated_at":"2019-05-15 12:31:30"}
         * service_medical : [{"cart_service_detail_id":16,"cart_service_id":2,"service_type":"1","service_id":1,"service_name":"Kolsultasi Dokter","service_price":"125000.00","order_status":"1","is_invoice":0,"created_by":1,"updated_by":0,"deleted_at":"","created_at":"2019-05-27 04:06:36","updated_at":"2019-05-27 04:06:36"}]
         * service_grooming : [{"cart_service_detail_id":17,"cart_service_id":2,"service_type":"2","service_id":1,"service_name":"SPA Kucing biasa","service_price":"100000.00","order_status":"1","is_invoice":0,"created_by":1,"updated_by":0,"deleted_at":"","created_at":"2019-05-27 04:07:29","updated_at":"2019-05-27 04:07:29"}]
         * service_boarding : []
         */

        @SerializedName("cart_service_id")
        private int cartServiceId;
        @SerializedName("cart_service_type")
        private String cartServiceType;
        @SerializedName("order_code")
        private String orderCode;
        @SerializedName("pet_id")
        private int petId;
        @SerializedName("doctor_schedule_id")
        private String doctorScheduleId;
        @SerializedName("grooming_location_schedule_id")
        private String groomingLocationScheduleId;
        @SerializedName("boarding_schedule_id")
        private String boardingScheduleId;
        @SerializedName("payment_complete")
        private int paymentComplete;
        @SerializedName("has_anamnesis")
        private int hasAnamnesis;
        @SerializedName("take_action")
        private int takeAction;
        @SerializedName("created_by")
        private int createdBy;
        @SerializedName("updated_by")
        private int updatedBy;
        @SerializedName("deleted_at")
        private String deletedAt;
        @SerializedName("created_at")
        private String createdAt;
        @SerializedName("updated_at")
        private String updatedAt;
        @SerializedName("pet")
        private PetBean pet;
        @SerializedName("service_medical")
        private List<ServiceBean> serviceMedical;
        @SerializedName("service_grooming")
        private List<ServiceBean> serviceGrooming;
        @SerializedName("service_boarding")
        private List<ServiceBean> serviceBoarding;
        @SerializedName("doctor_schedule")
        private DoctorScheduleBean doctorSchedule;
        @SerializedName("grooming_location_schedule")
        private GroomingLocationScheduleBean groomingLocationSchedule;
        @SerializedName("boarding_schedule")
        private BoardingScheduleBean boardingSchedule;

        public int getCartServiceId() {
            return cartServiceId;
        }

        public void setCartServiceId(int cartServiceId) {
            this.cartServiceId = cartServiceId;
        }

        public String getCartServiceType() {
            return cartServiceType;
        }

        public void setCartServiceType(String cartServiceType) {
            this.cartServiceType = cartServiceType;
        }

        public String getOrderCode() {
            return orderCode;
        }

        public void setOrderCode(String orderCode) {
            this.orderCode = orderCode;
        }

        public int getPetId() {
            return petId;
        }

        public void setPetId(int petId) {
            this.petId = petId;
        }

        public String getDoctorScheduleId() {
            return doctorScheduleId;
        }

        public void setDoctorScheduleId(String doctorScheduleId) {
            this.doctorScheduleId = doctorScheduleId;
        }

        public String getGroomingLocationScheduleId() {
            return groomingLocationScheduleId;
        }

        public void setGroomingLocationScheduleId(String groomingLocationScheduleId) {
            this.groomingLocationScheduleId = groomingLocationScheduleId;
        }

        public String getBoardingScheduleId() {
            return boardingScheduleId;
        }

        public void setBoardingScheduleId(String boardingScheduleId) {
            this.boardingScheduleId = boardingScheduleId;
        }

        public int getPaymentComplete() {
            return paymentComplete;
        }

        public void setPaymentComplete(int paymentComplete) {
            this.paymentComplete = paymentComplete;
        }

        public int getHasAnamnesis() {
            return hasAnamnesis;
        }

        public void setHasAnamnesis(int hasAnamnesis) {
            this.hasAnamnesis = hasAnamnesis;
        }

        public int getTakeAction() {
            return takeAction;
        }

        public void setTakeAction(int takeAction) {
            this.takeAction = takeAction;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public int getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(String deletedAt) {
            this.deletedAt = deletedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public PetBean getPet() {
            return pet;
        }

        public void setPet(PetBean pet) {
            this.pet = pet;
        }

        public List<ServiceBean> getServiceMedical() {
            return serviceMedical;
        }

        public void setServiceMedical(List<ServiceBean> serviceMedical) {
            this.serviceMedical = serviceMedical;
        }

        public List<ServiceBean> getServiceGrooming() {
            return serviceGrooming;
        }

        public void setServiceGrooming(List<ServiceBean> serviceGrooming) {
            this.serviceGrooming = serviceGrooming;
        }

        public List<ServiceBean> getServiceBoarding() {
            return serviceBoarding;
        }

        public void setServiceBoarding(List<ServiceBean> serviceBoarding) {
            this.serviceBoarding = serviceBoarding;
        }

        public DoctorScheduleBean getDoctorSchedule() {
            return doctorSchedule;
        }

        public void setDoctorSchedule(DoctorScheduleBean doctorSchedule) {
            this.doctorSchedule = doctorSchedule;
        }

        public GroomingLocationScheduleBean getGroomingLocationSchedule() {
            return groomingLocationSchedule;
        }

        public void setGroomingLocationSchedule(GroomingLocationScheduleBean groomingLocationSchedule) {
            this.groomingLocationSchedule = groomingLocationSchedule;
        }

        public BoardingScheduleBean getBoardingSchedule() {
            return boardingSchedule;
        }

        public void setBoardingSchedule(BoardingScheduleBean boardingSchedule) {
            this.boardingSchedule = boardingSchedule;
        }

        public static class PetBean {
            /**
             * customer_name : Henry S. Browne
             * pet_id : 2
             * customer_id : 3
             * pet_name : Fitch
             * pet_race : Angora
             * pet_birthday : 2019-01-02
             * pet_gender : 1
             * pet_photo : customer-1557898290.png
             * created_on : 1
             * updated_on : 1
             * created_by : 4
             * updated_by : 4
             * deleted_at :
             * created_at : 2019-05-15 12:31:30
             * updated_at : 2019-05-15 12:31:30
             */

            @SerializedName("customer_name")
            private String customerName;
            @SerializedName("pet_id")
            private int petId;
            @SerializedName("customer_id")
            private int customerId;
            @SerializedName("pet_name")
            private String petName;
            @SerializedName("pet_race")
            private String petRace;
            @SerializedName("pet_birthday")
            private String petBirthday;
            @SerializedName("pet_gender")
            private String petGender;
            @SerializedName("pet_photo")
            private String petPhoto;
            @SerializedName("created_on")
            private String createdOn;
            @SerializedName("updated_on")
            private String updatedOn;
            @SerializedName("created_by")
            private int createdBy;
            @SerializedName("updated_by")
            private int updatedBy;
            @SerializedName("deleted_at")
            private String deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;

            public String getCustomerName() {
                return customerName;
            }

            public void setCustomerName(String customerName) {
                this.customerName = customerName;
            }

            public int getPetId() {
                return petId;
            }

            public void setPetId(int petId) {
                this.petId = petId;
            }

            public int getCustomerId() {
                return customerId;
            }

            public void setCustomerId(int customerId) {
                this.customerId = customerId;
            }

            public String getPetName() {
                return petName;
            }

            public void setPetName(String petName) {
                this.petName = petName;
            }

            public String getPetRace() {
                return petRace;
            }

            public void setPetRace(String petRace) {
                this.petRace = petRace;
            }

            public String getPetBirthday() {
                return petBirthday;
            }

            public void setPetBirthday(String petBirthday) {
                this.petBirthday = petBirthday;
            }

            public String getPetGender() {
                return petGender;
            }

            public void setPetGender(String petGender) {
                this.petGender = petGender;
            }

            public String getPetPhoto() {
                return petPhoto;
            }

            public void setPetPhoto(String petPhoto) {
                this.petPhoto = petPhoto;
            }

            public String getCreatedOn() {
                return createdOn;
            }

            public void setCreatedOn(String createdOn) {
                this.createdOn = createdOn;
            }

            public String getUpdatedOn() {
                return updatedOn;
            }

            public void setUpdatedOn(String updatedOn) {
                this.updatedOn = updatedOn;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(String deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }
        }

        public static class ServiceBean {
            /**
             * cart_service_detail_id : 16
             * cart_service_id : 2
             * service_type : 1
             * service_id : 1
             * service_name : Kolsultasi Dokter
             * service_price : 125000.00
             * order_status : 1
             * is_invoice : 0
             * created_by : 1
             * updated_by : 0
             * deleted_at :
             * created_at : 2019-05-27 04:06:36
             * updated_at : 2019-05-27 04:06:36
             */

            @SerializedName("cart_service_detail_id")
            private int cartServiceDetailId;
            @SerializedName("cart_service_id")
            private int cartServiceId;
            @SerializedName("service_type")
            private String serviceType;
            @SerializedName("service_id")
            private int serviceId;
            @SerializedName("service_name")
            private String serviceName;
            @SerializedName("service_price")
            private String servicePrice;
            @SerializedName("order_status")
            private String orderStatus;
            @SerializedName("is_invoice")
            private int isInvoice;
            @SerializedName("created_by")
            private int createdBy;
            @SerializedName("updated_by")
            private int updatedBy;
            @SerializedName("deleted_at")
            private String deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;

            public int getCartServiceDetailId() {
                return cartServiceDetailId;
            }

            public void setCartServiceDetailId(int cartServiceDetailId) {
                this.cartServiceDetailId = cartServiceDetailId;
            }

            public int getCartServiceId() {
                return cartServiceId;
            }

            public void setCartServiceId(int cartServiceId) {
                this.cartServiceId = cartServiceId;
            }

            public String getServiceType() {
                return serviceType;
            }

            public void setServiceType(String serviceType) {
                this.serviceType = serviceType;
            }

            public int getServiceId() {
                return serviceId;
            }

            public void setServiceId(int serviceId) {
                this.serviceId = serviceId;
            }

            public String getServiceName() {
                return serviceName;
            }

            public void setServiceName(String serviceName) {
                this.serviceName = serviceName;
            }

            public String getServicePrice() {
                return servicePrice;
            }

            public void setServicePrice(String servicePrice) {
                this.servicePrice = servicePrice;
            }

            public String getOrderStatus() {
                return orderStatus;
            }

            public void setOrderStatus(String orderStatus) {
                this.orderStatus = orderStatus;
            }

            public int getIsInvoice() {
                return isInvoice;
            }

            public void setIsInvoice(int isInvoice) {
                this.isInvoice = isInvoice;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(String deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }
        }
        public static class DoctorScheduleBean {
            /**
             * name : Siraja raja
             * doctor_schedule_id : 29
             * staff_id : 24
             * reservation_id : 3
             * pet_id : 3
             * schedule_date : 2019-05-02
             * schedule_time : 10:00:00
             * created_by : 3
             * updated_by : 0
             * deleted_at :
             * created_at : 2019-05-27 06:40:05
             * updated_at : 2019-05-27 06:40:05
             */

            @SerializedName("name")
            private String name;
            @SerializedName("doctor_schedule_id")
            private int doctorScheduleId;
            @SerializedName("staff_id")
            private int staffId;
            @SerializedName("reservation_id")
            private int reservationId;
            @SerializedName("pet_id")
            private int petId;
            @SerializedName("schedule_date")
            private String scheduleDate;
            @SerializedName("schedule_time")
            private String scheduleTime;
            @SerializedName("created_by")
            private int createdBy;
            @SerializedName("updated_by")
            private int updatedBy;
            @SerializedName("deleted_at")
            private String deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getDoctorScheduleId() {
                return doctorScheduleId;
            }

            public void setDoctorScheduleId(int doctorScheduleId) {
                this.doctorScheduleId = doctorScheduleId;
            }

            public int getStaffId() {
                return staffId;
            }

            public void setStaffId(int staffId) {
                this.staffId = staffId;
            }

            public int getReservationId() {
                return reservationId;
            }

            public void setReservationId(int reservationId) {
                this.reservationId = reservationId;
            }

            public int getPetId() {
                return petId;
            }

            public void setPetId(int petId) {
                this.petId = petId;
            }

            public String getScheduleDate() {
                return scheduleDate;
            }

            public void setScheduleDate(String scheduleDate) {
                this.scheduleDate = scheduleDate;
            }

            public String getScheduleTime() {
                return scheduleTime;
            }

            public void setScheduleTime(String scheduleTime) {
                this.scheduleTime = scheduleTime;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(String deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }
        }
        public static class GroomingLocationScheduleBean {
            /**
             * location_name : Ruangan #2
             * grooming_location_schedule_id : 23
             * grooming_location_id : 2
             * reservation_id : 3
             * customer_id : 0
             * pet_id : 3
             * schedule_date : 2019-05-02
             * schedule_time : 10:00:00
             * created_by : 3
             * updated_by : 0
             * deleted_at :
             * created_at : 2019-05-27 06:40:05
             * updated_at : 2019-05-27 06:40:05
             */

            @SerializedName("location_name")
            private String locationName;
            @SerializedName("grooming_location_schedule_id")
            private int groomingLocationScheduleId;
            @SerializedName("grooming_location_id")
            private int groomingLocationId;
            @SerializedName("reservation_id")
            private int reservationId;
            @SerializedName("customer_id")
            private int customerId;
            @SerializedName("pet_id")
            private int petId;
            @SerializedName("schedule_date")
            private String scheduleDate;
            @SerializedName("schedule_time")
            private String scheduleTime;
            @SerializedName("created_by")
            private int createdBy;
            @SerializedName("updated_by")
            private int updatedBy;
            @SerializedName("deleted_at")
            private String deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;

            public String getLocationName() {
                return locationName;
            }

            public void setLocationName(String locationName) {
                this.locationName = locationName;
            }

            public int getGroomingLocationScheduleId() {
                return groomingLocationScheduleId;
            }

            public void setGroomingLocationScheduleId(int groomingLocationScheduleId) {
                this.groomingLocationScheduleId = groomingLocationScheduleId;
            }

            public int getGroomingLocationId() {
                return groomingLocationId;
            }

            public void setGroomingLocationId(int groomingLocationId) {
                this.groomingLocationId = groomingLocationId;
            }

            public int getReservationId() {
                return reservationId;
            }

            public void setReservationId(int reservationId) {
                this.reservationId = reservationId;
            }

            public int getCustomerId() {
                return customerId;
            }

            public void setCustomerId(int customerId) {
                this.customerId = customerId;
            }

            public int getPetId() {
                return petId;
            }

            public void setPetId(int petId) {
                this.petId = petId;
            }

            public String getScheduleDate() {
                return scheduleDate;
            }

            public void setScheduleDate(String scheduleDate) {
                this.scheduleDate = scheduleDate;
            }

            public String getScheduleTime() {
                return scheduleTime;
            }

            public void setScheduleTime(String scheduleTime) {
                this.scheduleTime = scheduleTime;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(String deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }
        }

        public static class BoardingScheduleBean {
            /**
             * cage_name : Kandang Kucing S
             * cage_schedule_id : 20
             * cage_id : 1
             * reservation_id : 3
             * customer_id : 0
             * pet_id : 3
             * schedule_date_from : 2019-05-02
             * schedule_date_until : 2019-05-03
             * created_by : 3
             * updated_by : 0
             * deleted_at :
             * created_at : 2019-05-27 06:40:06
             * updated_at : 2019-05-27 06:40:06
             */

            @SerializedName("cage_name")
            private String cageName;
            @SerializedName("cage_schedule_id")
            private int cageScheduleId;
            @SerializedName("cage_id")
            private int cageId;
            @SerializedName("reservation_id")
            private int reservationId;
            @SerializedName("customer_id")
            private int customerId;
            @SerializedName("pet_id")
            private int petId;
            @SerializedName("schedule_date_from")
            private String scheduleDateFrom;
            @SerializedName("schedule_date_until")
            private String scheduleDateUntil;
            @SerializedName("created_by")
            private int createdBy;
            @SerializedName("updated_by")
            private int updatedBy;
            @SerializedName("deleted_at")
            private String deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
            @SerializedName("cage_price")
            private String cagePrice;

            public String getCageName() {
                return cageName;
            }

            public void setCageName(String cageName) {
                this.cageName = cageName;
            }

            public int getCageScheduleId() {
                return cageScheduleId;
            }

            public void setCageScheduleId(int cageScheduleId) {
                this.cageScheduleId = cageScheduleId;
            }

            public int getCageId() {
                return cageId;
            }

            public void setCageId(int cageId) {
                this.cageId = cageId;
            }

            public int getReservationId() {
                return reservationId;
            }

            public void setReservationId(int reservationId) {
                this.reservationId = reservationId;
            }

            public int getCustomerId() {
                return customerId;
            }

            public void setCustomerId(int customerId) {
                this.customerId = customerId;
            }

            public int getPetId() {
                return petId;
            }

            public void setPetId(int petId) {
                this.petId = petId;
            }

            public String getScheduleDateFrom() {
                return scheduleDateFrom;
            }

            public void setScheduleDateFrom(String scheduleDateFrom) {
                this.scheduleDateFrom = scheduleDateFrom;
            }

            public String getScheduleDateUntil() {
                return scheduleDateUntil;
            }

            public void setScheduleDateUntil(String scheduleDateUntil) {
                this.scheduleDateUntil = scheduleDateUntil;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(String deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public String getCagePrice() {
                return this.cagePrice;
            }

            public void setCagePrice(String cagePrice) {
                this.cagePrice = cagePrice;
            }
        }

    }
}
