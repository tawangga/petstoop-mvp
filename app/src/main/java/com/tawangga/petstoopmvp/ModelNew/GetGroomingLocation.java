package com.tawangga.petstoopmvp.ModelNew;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetGroomingLocation {
    /**
     * STATUS_CODE : 200
     * MESSAGE : Success
     * DATA : {"page":1,"last_page":1,"total_item":3,"limit":"10","items":[{"grooming_location_id":1,"name":"Ruangan #1","time":"09:00","date":"2019-05-05","is_available":true},{"grooming_location_id":3,"name":"Ruangan #3","time":"09:00","date":"2019-05-05","is_available":true},{"grooming_location_id":3,"name":"Ruangan #3","time":"10:00","date":"2019-05-05","is_available":true},{"grooming_location_id":1,"name":"Ruangan #1","time":"10:00","date":"2019-05-05","is_available":false},{"grooming_location_id":3,"name":"Ruangan #3","time":"11:00","date":"2019-05-05","is_available":true},{"grooming_location_id":1,"name":"Ruangan #1","time":"11:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"11:00","date":"2019-05-05","is_available":true},{"grooming_location_id":1,"name":"Ruangan #1","time":"12:00","date":"2019-05-05","is_available":true},{"grooming_location_id":3,"name":"Ruangan #3","time":"12:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"12:00","date":"2019-05-05","is_available":true},{"grooming_location_id":1,"name":"Ruangan #1","time":"13:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"13:00","date":"2019-05-05","is_available":true},{"grooming_location_id":3,"name":"Ruangan #3","time":"13:00","date":"2019-05-05","is_available":true},{"grooming_location_id":1,"name":"Ruangan #1","time":"14:00","date":"2019-05-05","is_available":true},{"grooming_location_id":3,"name":"Ruangan #3","time":"14:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"14:00","date":"2019-05-05","is_available":true},{"grooming_location_id":3,"name":"Ruangan #3","time":"15:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"15:00","date":"2019-05-05","is_available":true},{"grooming_location_id":1,"name":"Ruangan #1","time":"15:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"16:00","date":"2019-05-05","is_available":true},{"grooming_location_id":1,"name":"Ruangan #1","time":"16:00","date":"2019-05-05","is_available":true},{"grooming_location_id":3,"name":"Ruangan #3","time":"16:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"17:00","date":"2019-05-05","is_available":true},{"grooming_location_id":1,"name":"Ruangan #1","time":"17:00","date":"2019-05-05","is_available":true},{"grooming_location_id":3,"name":"Ruangan #3","time":"17:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"18:00","date":"2019-05-05","is_available":true},{"grooming_location_id":1,"name":"Ruangan #1","time":"18:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"19:00","date":"2019-05-05","is_available":true},{"grooming_location_id":1,"name":"Ruangan #1","time":"19:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"20:00","date":"2019-05-05","is_available":true},{"grooming_location_id":1,"name":"Ruangan #1","time":"20:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"21:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"22:00","date":"2019-05-05","is_available":true}]}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * page : 1
         * last_page : 1
         * total_item : 3
         * limit : 10
         * items : [{"grooming_location_id":1,"name":"Ruangan #1","time":"09:00","date":"2019-05-05","is_available":true},{"grooming_location_id":3,"name":"Ruangan #3","time":"09:00","date":"2019-05-05","is_available":true},{"grooming_location_id":3,"name":"Ruangan #3","time":"10:00","date":"2019-05-05","is_available":true},{"grooming_location_id":1,"name":"Ruangan #1","time":"10:00","date":"2019-05-05","is_available":false},{"grooming_location_id":3,"name":"Ruangan #3","time":"11:00","date":"2019-05-05","is_available":true},{"grooming_location_id":1,"name":"Ruangan #1","time":"11:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"11:00","date":"2019-05-05","is_available":true},{"grooming_location_id":1,"name":"Ruangan #1","time":"12:00","date":"2019-05-05","is_available":true},{"grooming_location_id":3,"name":"Ruangan #3","time":"12:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"12:00","date":"2019-05-05","is_available":true},{"grooming_location_id":1,"name":"Ruangan #1","time":"13:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"13:00","date":"2019-05-05","is_available":true},{"grooming_location_id":3,"name":"Ruangan #3","time":"13:00","date":"2019-05-05","is_available":true},{"grooming_location_id":1,"name":"Ruangan #1","time":"14:00","date":"2019-05-05","is_available":true},{"grooming_location_id":3,"name":"Ruangan #3","time":"14:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"14:00","date":"2019-05-05","is_available":true},{"grooming_location_id":3,"name":"Ruangan #3","time":"15:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"15:00","date":"2019-05-05","is_available":true},{"grooming_location_id":1,"name":"Ruangan #1","time":"15:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"16:00","date":"2019-05-05","is_available":true},{"grooming_location_id":1,"name":"Ruangan #1","time":"16:00","date":"2019-05-05","is_available":true},{"grooming_location_id":3,"name":"Ruangan #3","time":"16:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"17:00","date":"2019-05-05","is_available":true},{"grooming_location_id":1,"name":"Ruangan #1","time":"17:00","date":"2019-05-05","is_available":true},{"grooming_location_id":3,"name":"Ruangan #3","time":"17:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"18:00","date":"2019-05-05","is_available":true},{"grooming_location_id":1,"name":"Ruangan #1","time":"18:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"19:00","date":"2019-05-05","is_available":true},{"grooming_location_id":1,"name":"Ruangan #1","time":"19:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"20:00","date":"2019-05-05","is_available":true},{"grooming_location_id":1,"name":"Ruangan #1","time":"20:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"21:00","date":"2019-05-05","is_available":true},{"grooming_location_id":2,"name":"Ruangan #2","time":"22:00","date":"2019-05-05","is_available":true}]
         */

        @SerializedName("page")
        private int page;
        @SerializedName("last_page")
        private int lastPage;
        @SerializedName("total_item")
        private int totalItem;
        @SerializedName("limit")
        private String limit;
        @SerializedName("items")
        private List<ItemsBean> items;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public int getTotalItem() {
            return totalItem;
        }

        public void setTotalItem(int totalItem) {
            this.totalItem = totalItem;
        }

        public String getLimit() {
            return limit;
        }

        public void setLimit(String limit) {
            this.limit = limit;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class ItemsBean {
            /**
             * grooming_location_id : 1
             * name : Ruangan #1
             * time : 09:00
             * date : 2019-05-05
             * is_available : true
             */

            @SerializedName("grooming_location_id")
            private int groomingLocationId;
            @SerializedName("name")
            private String name;
            @SerializedName("time")
            private String time;
            @SerializedName("date")
            private String date;
            @SerializedName("is_available")
            private boolean isAvailable;

            public int getGroomingLocationId() {
                return groomingLocationId;
            }

            public void setGroomingLocationId(int groomingLocationId) {
                this.groomingLocationId = groomingLocationId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public boolean isIsAvailable() {
                return isAvailable;
            }

            public void setIsAvailable(boolean isAvailable) {
                this.isAvailable = isAvailable;
            }
        }
    }
}
