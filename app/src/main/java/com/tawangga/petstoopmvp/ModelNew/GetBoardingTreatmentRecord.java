package com.tawangga.petstoopmvp.ModelNew;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetBoardingTreatmentRecord {
    /**
     * STATUS_CODE : 200
     * MESSAGE : Success
     * DATA : {"page":1,"last_page":1,"total_item":2,"limit":10,"items":[{"pet_id":1,"cart_service_id":1,"boarding_schedule_id":1,"order_code":"R#1560936965-1","order_date":"2019-06-19","service_type":"3","service_name":"Kandang Kucing S","service_id":1,"boarding_schedule":{"cage_name":"Kandang Kucing S","cage_price":"150000","cage_schedule_id":1,"cage_id":1,"reservation_id":1,"customer_id":0,"pet_id":1,"schedule_date_from":"2019-06-19","schedule_date_until":"2019-06-21","created_by":1,"updated_by":0,"deleted_at":null,"created_at":"2019-06-19 09:43:09","updated_at":"2019-06-19 09:43:09"}},{"pet_id":1,"cart_service_id":5,"boarding_schedule_id":4,"order_code":"R#1560976994-1","order_date":"2019-05-02","service_type":"3","service_name":"Test","service_id":1,"boarding_schedule":{"cage_name":"Kandang Kucing S","cage_price":"150000","cage_schedule_id":4,"cage_id":1,"reservation_id":5,"customer_id":0,"pet_id":1,"schedule_date_from":"2019-05-02","schedule_date_until":"2019-05-02","created_by":3,"updated_by":0,"deleted_at":null,"created_at":"2019-06-19 20:45:53","updated_at":"2019-06-19 20:45:53"}}]}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * page : 1
         * last_page : 1
         * total_item : 2
         * limit : 10
         * items : [{"pet_id":1,"cart_service_id":1,"boarding_schedule_id":1,"order_code":"R#1560936965-1","order_date":"2019-06-19","service_type":"3","service_name":"Kandang Kucing S","service_id":1,"boarding_schedule":{"cage_name":"Kandang Kucing S","cage_price":"150000","cage_schedule_id":1,"cage_id":1,"reservation_id":1,"customer_id":0,"pet_id":1,"schedule_date_from":"2019-06-19","schedule_date_until":"2019-06-21","created_by":1,"updated_by":0,"deleted_at":null,"created_at":"2019-06-19 09:43:09","updated_at":"2019-06-19 09:43:09"}},{"pet_id":1,"cart_service_id":5,"boarding_schedule_id":4,"order_code":"R#1560976994-1","order_date":"2019-05-02","service_type":"3","service_name":"Test","service_id":1,"boarding_schedule":{"cage_name":"Kandang Kucing S","cage_price":"150000","cage_schedule_id":4,"cage_id":1,"reservation_id":5,"customer_id":0,"pet_id":1,"schedule_date_from":"2019-05-02","schedule_date_until":"2019-05-02","created_by":3,"updated_by":0,"deleted_at":null,"created_at":"2019-06-19 20:45:53","updated_at":"2019-06-19 20:45:53"}}]
         */

        @SerializedName("page")
        private int page;
        @SerializedName("last_page")
        private int lastPage;
        @SerializedName("total_item")
        private int totalItem;
        @SerializedName("limit")
        private int limit;
        @SerializedName("items")
        private List<ItemsBean> items;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public int getTotalItem() {
            return totalItem;
        }

        public void setTotalItem(int totalItem) {
            this.totalItem = totalItem;
        }

        public int getLimit() {
            return limit;
        }

        public void setLimit(int limit) {
            this.limit = limit;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class ItemsBean {
            /**
             * pet_id : 1
             * cart_service_id : 1
             * boarding_schedule_id : 1
             * order_code : R#1560936965-1
             * order_date : 2019-06-19
             * service_type : 3
             * service_name : Kandang Kucing S
             * service_id : 1
             * boarding_schedule : {"cage_name":"Kandang Kucing S","cage_price":"150000","cage_schedule_id":1,"cage_id":1,"reservation_id":1,"customer_id":0,"pet_id":1,"schedule_date_from":"2019-06-19","schedule_date_until":"2019-06-21","created_by":1,"updated_by":0,"deleted_at":null,"created_at":"2019-06-19 09:43:09","updated_at":"2019-06-19 09:43:09"}
             */

            @SerializedName("pet_id")
            private int petId;
            @SerializedName("cart_service_id")
            private int cartServiceId;
            @SerializedName("boarding_schedule_id")
            private int boardingScheduleId;
            @SerializedName("order_code")
            private String orderCode;
            @SerializedName("order_date")
            private String orderDate;
            @SerializedName("service_type")
            private String serviceType;
            @SerializedName("service_name")
            private String serviceName;
            @SerializedName("service_id")
            private int serviceId;
            @SerializedName("boarding_schedule")
            private BoardingScheduleBean boardingSchedule;

            public int getPetId() {
                return petId;
            }

            public void setPetId(int petId) {
                this.petId = petId;
            }

            public int getCartServiceId() {
                return cartServiceId;
            }

            public void setCartServiceId(int cartServiceId) {
                this.cartServiceId = cartServiceId;
            }

            public int getBoardingScheduleId() {
                return boardingScheduleId;
            }

            public void setBoardingScheduleId(int boardingScheduleId) {
                this.boardingScheduleId = boardingScheduleId;
            }

            public String getOrderCode() {
                return orderCode;
            }

            public void setOrderCode(String orderCode) {
                this.orderCode = orderCode;
            }

            public String getOrderDate() {
                return orderDate;
            }

            public void setOrderDate(String orderDate) {
                this.orderDate = orderDate;
            }

            public String getServiceType() {
                return serviceType;
            }

            public void setServiceType(String serviceType) {
                this.serviceType = serviceType;
            }

            public String getServiceName() {
                return serviceName;
            }

            public void setServiceName(String serviceName) {
                this.serviceName = serviceName;
            }

            public int getServiceId() {
                return serviceId;
            }

            public void setServiceId(int serviceId) {
                this.serviceId = serviceId;
            }

            public BoardingScheduleBean getBoardingSchedule() {
                return boardingSchedule;
            }

            public void setBoardingSchedule(BoardingScheduleBean boardingSchedule) {
                this.boardingSchedule = boardingSchedule;
            }

            public static class BoardingScheduleBean {
                /**
                 * cage_name : Kandang Kucing S
                 * cage_price : 150000
                 * cage_schedule_id : 1
                 * cage_id : 1
                 * reservation_id : 1
                 * customer_id : 0
                 * pet_id : 1
                 * schedule_date_from : 2019-06-19
                 * schedule_date_until : 2019-06-21
                 * created_by : 1
                 * updated_by : 0
                 * deleted_at : null
                 * created_at : 2019-06-19 09:43:09
                 * updated_at : 2019-06-19 09:43:09
                 */

                @SerializedName("cage_name")
                private String cageName;
                @SerializedName("cage_price")
                private String cagePrice;
                @SerializedName("cage_schedule_id")
                private int cageScheduleId;
                @SerializedName("cage_id")
                private int cageId;
                @SerializedName("reservation_id")
                private int reservationId;
                @SerializedName("customer_id")
                private int customerId;
                @SerializedName("pet_id")
                private int petId;
                @SerializedName("schedule_date_from")
                private String scheduleDateFrom;
                @SerializedName("schedule_date_until")
                private String scheduleDateUntil;
                @SerializedName("created_by")
                private int createdBy;
                @SerializedName("updated_by")
                private int updatedBy;
                @SerializedName("deleted_at")
                private Object deletedAt;
                @SerializedName("created_at")
                private String createdAt;
                @SerializedName("updated_at")
                private String updatedAt;

                public String getCageName() {
                    return cageName;
                }

                public void setCageName(String cageName) {
                    this.cageName = cageName;
                }

                public String getCagePrice() {
                    return cagePrice;
                }

                public void setCagePrice(String cagePrice) {
                    this.cagePrice = cagePrice;
                }

                public int getCageScheduleId() {
                    return cageScheduleId;
                }

                public void setCageScheduleId(int cageScheduleId) {
                    this.cageScheduleId = cageScheduleId;
                }

                public int getCageId() {
                    return cageId;
                }

                public void setCageId(int cageId) {
                    this.cageId = cageId;
                }

                public int getReservationId() {
                    return reservationId;
                }

                public void setReservationId(int reservationId) {
                    this.reservationId = reservationId;
                }

                public int getCustomerId() {
                    return customerId;
                }

                public void setCustomerId(int customerId) {
                    this.customerId = customerId;
                }

                public int getPetId() {
                    return petId;
                }

                public void setPetId(int petId) {
                    this.petId = petId;
                }

                public String getScheduleDateFrom() {
                    return scheduleDateFrom;
                }

                public void setScheduleDateFrom(String scheduleDateFrom) {
                    this.scheduleDateFrom = scheduleDateFrom;
                }

                public String getScheduleDateUntil() {
                    return scheduleDateUntil;
                }

                public void setScheduleDateUntil(String scheduleDateUntil) {
                    this.scheduleDateUntil = scheduleDateUntil;
                }

                public int getCreatedBy() {
                    return createdBy;
                }

                public void setCreatedBy(int createdBy) {
                    this.createdBy = createdBy;
                }

                public int getUpdatedBy() {
                    return updatedBy;
                }

                public void setUpdatedBy(int updatedBy) {
                    this.updatedBy = updatedBy;
                }

                public Object getDeletedAt() {
                    return deletedAt;
                }

                public void setDeletedAt(Object deletedAt) {
                    this.deletedAt = deletedAt;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }
            }
        }
    }
}
