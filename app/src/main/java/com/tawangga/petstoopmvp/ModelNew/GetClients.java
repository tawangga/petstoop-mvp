package com.tawangga.petstoopmvp.ModelNew;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetClients {
    /**
     * STATUS_CODE : 200
     * MESSAGE : Success
     * DATA : {"page":1,"last_page":1,"total_item":8,"limit":10,"items":[{"user_id":34,"username":"client1","email":"client1@gmail.com","user_group_id":2,"photo":"user-1565342337.png","created_by":1,"updated_by":1,"created_at":"2019-08-09 09:18:57","updated_at":"2019-08-09 09:18:57","user_client_id":1,"name":"Client 1","phone":"081222222","remark":"-","role":{"user_group_id":2,"user_group_name":"Client"}},{"user_id":35,"username":"client2","email":"client2@gmail.com","user_group_id":2,"photo":"user-1565342365.png","created_by":1,"updated_by":0,"created_at":"2019-08-09 09:19:25","updated_at":"2019-08-09 09:19:25","user_client_id":2,"name":"Client 2","phone":"081232131231","remark":"-","role":{"user_group_id":2,"user_group_name":"Client"}},{"user_id":36,"username":"client3","email":"client3@gmail.com","user_group_id":2,"photo":"user-1565342465.png","created_by":1,"updated_by":0,"created_at":"2019-08-09 09:21:05","updated_at":"2019-08-09 09:21:05","user_client_id":3,"name":"Client 3","phone":"0812313131","remark":"adadadaada","role":{"user_group_id":2,"user_group_name":"Client"}},{"user_id":40,"username":"ivanS","email":"ivan@gmail.com","user_group_id":2,"photo":"user-1565854721.png","created_by":1,"updated_by":1,"created_at":"2019-08-15 07:38:41","updated_at":"2019-08-15 07:38:41","user_client_id":4,"name":"Ivan Satriawan","phone":"02199999999","remark":"Doctor","role":{"user_group_id":2,"user_group_name":"Client"}},{"user_id":42,"username":"Klien1","email":"klien1@gmail.com","user_group_id":2,"photo":"user-1567394704.png","created_by":1,"updated_by":1,"created_at":"2019-09-02 03:25:05","updated_at":"2019-09-02 03:25:05","user_client_id":5,"name":"Klien1","phone":"021098974899","remark":"client","role":{"user_group_id":2,"user_group_name":"Client"}},{"user_id":44,"username":"klien2","email":"klien2@gmail.com","user_group_id":2,"photo":"user-1567396008.png","created_by":1,"updated_by":0,"created_at":"2019-09-02 03:46:48","updated_at":"2019-09-02 03:46:48","user_client_id":6,"name":"Klien2","phone":"089777836475","remark":"klien2","role":{"user_group_id":2,"user_group_name":"Client"}},{"user_id":49,"username":"indomie","email":"indomie@seleraku.com","user_group_id":2,"photo":"user-1568256525.png","created_by":1,"updated_by":0,"created_at":"2019-09-12 02:48:46","updated_at":"2019-09-12 02:48:46","user_client_id":7,"name":"Indomie","phone":"09122222","remark":"Indomieeee seleraku","role":{"user_group_id":2,"user_group_name":"Client"}},{"user_id":54,"username":"Bajigur","email":"bajigur@yahoo.co.jp","user_group_id":2,"photo":"user-1568881514.png","created_by":1,"updated_by":0,"created_at":"2019-09-19 08:25:14","updated_at":"2019-09-19 08:25:14","user_client_id":8,"name":"Bajigur","phone":"089998899","remark":"bajigur","role":{"user_group_id":2,"user_group_name":"Client"}}]}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * page : 1
         * last_page : 1
         * total_item : 8
         * limit : 10
         * items : [{"user_id":34,"username":"client1","email":"client1@gmail.com","user_group_id":2,"photo":"user-1565342337.png","created_by":1,"updated_by":1,"created_at":"2019-08-09 09:18:57","updated_at":"2019-08-09 09:18:57","user_client_id":1,"name":"Client 1","phone":"081222222","remark":"-","role":{"user_group_id":2,"user_group_name":"Client"}},{"user_id":35,"username":"client2","email":"client2@gmail.com","user_group_id":2,"photo":"user-1565342365.png","created_by":1,"updated_by":0,"created_at":"2019-08-09 09:19:25","updated_at":"2019-08-09 09:19:25","user_client_id":2,"name":"Client 2","phone":"081232131231","remark":"-","role":{"user_group_id":2,"user_group_name":"Client"}},{"user_id":36,"username":"client3","email":"client3@gmail.com","user_group_id":2,"photo":"user-1565342465.png","created_by":1,"updated_by":0,"created_at":"2019-08-09 09:21:05","updated_at":"2019-08-09 09:21:05","user_client_id":3,"name":"Client 3","phone":"0812313131","remark":"adadadaada","role":{"user_group_id":2,"user_group_name":"Client"}},{"user_id":40,"username":"ivanS","email":"ivan@gmail.com","user_group_id":2,"photo":"user-1565854721.png","created_by":1,"updated_by":1,"created_at":"2019-08-15 07:38:41","updated_at":"2019-08-15 07:38:41","user_client_id":4,"name":"Ivan Satriawan","phone":"02199999999","remark":"Doctor","role":{"user_group_id":2,"user_group_name":"Client"}},{"user_id":42,"username":"Klien1","email":"klien1@gmail.com","user_group_id":2,"photo":"user-1567394704.png","created_by":1,"updated_by":1,"created_at":"2019-09-02 03:25:05","updated_at":"2019-09-02 03:25:05","user_client_id":5,"name":"Klien1","phone":"021098974899","remark":"client","role":{"user_group_id":2,"user_group_name":"Client"}},{"user_id":44,"username":"klien2","email":"klien2@gmail.com","user_group_id":2,"photo":"user-1567396008.png","created_by":1,"updated_by":0,"created_at":"2019-09-02 03:46:48","updated_at":"2019-09-02 03:46:48","user_client_id":6,"name":"Klien2","phone":"089777836475","remark":"klien2","role":{"user_group_id":2,"user_group_name":"Client"}},{"user_id":49,"username":"indomie","email":"indomie@seleraku.com","user_group_id":2,"photo":"user-1568256525.png","created_by":1,"updated_by":0,"created_at":"2019-09-12 02:48:46","updated_at":"2019-09-12 02:48:46","user_client_id":7,"name":"Indomie","phone":"09122222","remark":"Indomieeee seleraku","role":{"user_group_id":2,"user_group_name":"Client"}},{"user_id":54,"username":"Bajigur","email":"bajigur@yahoo.co.jp","user_group_id":2,"photo":"user-1568881514.png","created_by":1,"updated_by":0,"created_at":"2019-09-19 08:25:14","updated_at":"2019-09-19 08:25:14","user_client_id":8,"name":"Bajigur","phone":"089998899","remark":"bajigur","role":{"user_group_id":2,"user_group_name":"Client"}}]
         */

        @SerializedName("page")
        private int page;
        @SerializedName("last_page")
        private int lastPage;
        @SerializedName("total_item")
        private int totalItem;
        @SerializedName("limit")
        private int limit;
        @SerializedName("items")
        private List<ItemsBean> items;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public int getTotalItem() {
            return totalItem;
        }

        public void setTotalItem(int totalItem) {
            this.totalItem = totalItem;
        }

        public int getLimit() {
            return limit;
        }

        public void setLimit(int limit) {
            this.limit = limit;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class ItemsBean {
            /**
             * user_id : 34
             * username : client1
             * email : client1@gmail.com
             * user_group_id : 2
             * photo : user-1565342337.png
             * created_by : 1
             * updated_by : 1
             * created_at : 2019-08-09 09:18:57
             * updated_at : 2019-08-09 09:18:57
             * user_client_id : 1
             * name : Client 1
             * phone : 081222222
             * remark : -
             * role : {"user_group_id":2,"user_group_name":"Client"}
             */

            @SerializedName("user_id")
            private int userId;
            @SerializedName("username")
            private String username;
            @SerializedName("email")
            private String email;
            @SerializedName("user_group_id")
            private int userGroupId;
            @SerializedName("photo")
            private String photo;
            @SerializedName("created_by")
            private int createdBy;
            @SerializedName("updated_by")
            private int updatedBy;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
            @SerializedName("user_client_id")
            private int userClientId;
            @SerializedName("name")
            private String name;
            @SerializedName("phone")
            private String phone;
            @SerializedName("remark")
            private String remark;
            @SerializedName("role")
            private RoleBean role;

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public int getUserGroupId() {
                return userGroupId;
            }

            public void setUserGroupId(int userGroupId) {
                this.userGroupId = userGroupId;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public int getUserClientId() {
                return userClientId;
            }

            public void setUserClientId(int userClientId) {
                this.userClientId = userClientId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public RoleBean getRole() {
                return role;
            }

            public void setRole(RoleBean role) {
                this.role = role;
            }

            public static class RoleBean {
                /**
                 * user_group_id : 2
                 * user_group_name : Client
                 */

                @SerializedName("user_group_id")
                private int userGroupId;
                @SerializedName("user_group_name")
                private String userGroupName;

                public int getUserGroupId() {
                    return userGroupId;
                }

                public void setUserGroupId(int userGroupId) {
                    this.userGroupId = userGroupId;
                }

                public String getUserGroupName() {
                    return userGroupName;
                }

                public void setUserGroupName(String userGroupName) {
                    this.userGroupName = userGroupName;
                }
            }
        }
    }
}
