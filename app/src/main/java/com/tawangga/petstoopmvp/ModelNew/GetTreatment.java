package com.tawangga.petstoopmvp.ModelNew;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetTreatment {
    /**
     * STATUS_CODE : 200
     * MESSAGE : Success
     * DATA : {"page":1,"last_page":1,"total_item":1,"limit":15,"items":[{"cart_service_id":3,"cart_service_type":"2","order_code":"R#1558927127-3","pet_id":3,"doctor_schedule_id":29,"grooming_location_schedule_id":23,"boarding_schedule_id":20,"payment_complete":0,"has_anamnesis":0,"take_action":0,"created_by":3,"updated_by":0,"deleted_at":"","created_at":"2019-05-27 10:18:47","updated_at":"2019-05-27 06:40:06","items":[{"cart_service_detail_id":26,"cart_service_id":3,"service_type":"3","service_id":1,"service_name":"Test","service_price":"20000.00","order_status":"1","is_invoice":0,"created_by":3,"updated_by":0,"deleted_at":"","created_at":"2019-05-27 06:40:06","updated_at":"2019-05-27 06:40:06"}],"pet":{"customer_name":"Lara A. Harding","pet_id":3,"customer_id":4,"pet_name":"Vida","pet_race":"Angora","pet_birthday":"2019-01-02","pet_gender":"2","pet_photo":"http://192.168.1.82/images/customer-1557899398.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":"","created_at":"2019-05-15 05:49:58","updated_at":"2019-05-15 05:49:58"},"doctor_schedule":{"name":"Siraja raja","doctor_schedule_id":29,"staff_id":24,"reservation_id":3,"pet_id":3,"schedule_date":"2019-05-02","schedule_time":"10:00:00","created_by":3,"updated_by":0,"deleted_at":"","created_at":"2019-05-27 06:40:05","updated_at":"2019-05-27 06:40:05"},"grooming_location_schedule":{"location_name":"Ruangan #2","grooming_location_schedule_id":23,"grooming_location_id":2,"reservation_id":3,"customer_id":0,"pet_id":3,"schedule_date":"2019-05-02","schedule_time":"10:00:00","created_by":3,"updated_by":0,"deleted_at":"","created_at":"2019-05-27 06:40:05","updated_at":"2019-05-27 06:40:05"},"boarding_schedule":{"cage_name":"Kandang Kucing S","cage_schedule_id":20,"cage_id":1,"reservation_id":3,"customer_id":0,"pet_id":3,"schedule_date_from":"2019-05-02","schedule_date_until":"2019-05-03","created_by":3,"updated_by":0,"deleted_at":"","created_at":"2019-05-27 06:40:06","updated_at":"2019-05-27 06:40:06"}}]}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * page : 1
         * last_page : 1
         * total_item : 1
         * limit : 15
         * items : [{"cart_service_id":3,"cart_service_type":"2","order_code":"R#1558927127-3","pet_id":3,"doctor_schedule_id":29,"grooming_location_schedule_id":23,"boarding_schedule_id":20,"payment_complete":0,"has_anamnesis":0,"take_action":0,"created_by":3,"updated_by":0,"deleted_at":"","created_at":"2019-05-27 10:18:47","updated_at":"2019-05-27 06:40:06","items":[{"cart_service_detail_id":26,"cart_service_id":3,"service_type":"3","service_id":1,"service_name":"Test","service_price":"20000.00","order_status":"1","is_invoice":0,"created_by":3,"updated_by":0,"deleted_at":"","created_at":"2019-05-27 06:40:06","updated_at":"2019-05-27 06:40:06"}],"pet":{"customer_name":"Lara A. Harding","pet_id":3,"customer_id":4,"pet_name":"Vida","pet_race":"Angora","pet_birthday":"2019-01-02","pet_gender":"2","pet_photo":"http://192.168.1.82/images/customer-1557899398.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":"","created_at":"2019-05-15 05:49:58","updated_at":"2019-05-15 05:49:58"},"doctor_schedule":{"name":"Siraja raja","doctor_schedule_id":29,"staff_id":24,"reservation_id":3,"pet_id":3,"schedule_date":"2019-05-02","schedule_time":"10:00:00","created_by":3,"updated_by":0,"deleted_at":"","created_at":"2019-05-27 06:40:05","updated_at":"2019-05-27 06:40:05"},"grooming_location_schedule":{"location_name":"Ruangan #2","grooming_location_schedule_id":23,"grooming_location_id":2,"reservation_id":3,"customer_id":0,"pet_id":3,"schedule_date":"2019-05-02","schedule_time":"10:00:00","created_by":3,"updated_by":0,"deleted_at":"","created_at":"2019-05-27 06:40:05","updated_at":"2019-05-27 06:40:05"},"boarding_schedule":{"cage_name":"Kandang Kucing S","cage_schedule_id":20,"cage_id":1,"reservation_id":3,"customer_id":0,"pet_id":3,"schedule_date_from":"2019-05-02","schedule_date_until":"2019-05-03","created_by":3,"updated_by":0,"deleted_at":"","created_at":"2019-05-27 06:40:06","updated_at":"2019-05-27 06:40:06"}}]
         */

        @SerializedName("page")
        private int page;
        @SerializedName("last_page")
        private int lastPage;
        @SerializedName("total_item")
        private int totalItem;
        @SerializedName("limit")
        private int limit;
        @SerializedName("items")
        private List<ItemsBeanX> items;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public int getTotalItem() {
            return totalItem;
        }

        public void setTotalItem(int totalItem) {
            this.totalItem = totalItem;
        }

        public int getLimit() {
            return limit;
        }

        public void setLimit(int limit) {
            this.limit = limit;
        }

        public List<ItemsBeanX> getItems() {
            return items;
        }

        public void setItems(List<ItemsBeanX> items) {
            this.items = items;
        }

        public static class ItemsBeanX {
            /**
             * cart_service_id : 3
             * cart_service_type : 2
             * order_code : R#1558927127-3
             * pet_id : 3
             * doctor_schedule_id : 29
             * grooming_location_schedule_id : 23
             * boarding_schedule_id : 20
             * payment_complete : 0
             * has_anamnesis : 0
             * take_action : 0
             * created_by : 3
             * updated_by : 0
             * deleted_at :
             * created_at : 2019-05-27 10:18:47
             * updated_at : 2019-05-27 06:40:06
             * items : [{"cart_service_detail_id":26,"cart_service_id":3,"service_type":"3","service_id":1,"service_name":"Test","service_price":"20000.00","order_status":"1","is_invoice":0,"created_by":3,"updated_by":0,"deleted_at":"","created_at":"2019-05-27 06:40:06","updated_at":"2019-05-27 06:40:06"}]
             * pet : {"customer_name":"Lara A. Harding","pet_id":3,"customer_id":4,"pet_name":"Vida","pet_race":"Angora","pet_birthday":"2019-01-02","pet_gender":"2","pet_photo":"http://192.168.1.82/images/customer-1557899398.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":"","created_at":"2019-05-15 05:49:58","updated_at":"2019-05-15 05:49:58"}
             * doctor_schedule : {"name":"Siraja raja","doctor_schedule_id":29,"staff_id":24,"reservation_id":3,"pet_id":3,"schedule_date":"2019-05-02","schedule_time":"10:00:00","created_by":3,"updated_by":0,"deleted_at":"","created_at":"2019-05-27 06:40:05","updated_at":"2019-05-27 06:40:05"}
             * grooming_location_schedule : {"location_name":"Ruangan #2","grooming_location_schedule_id":23,"grooming_location_id":2,"reservation_id":3,"customer_id":0,"pet_id":3,"schedule_date":"2019-05-02","schedule_time":"10:00:00","created_by":3,"updated_by":0,"deleted_at":"","created_at":"2019-05-27 06:40:05","updated_at":"2019-05-27 06:40:05"}
             * boarding_schedule : {"cage_name":"Kandang Kucing S","cage_schedule_id":20,"cage_id":1,"reservation_id":3,"customer_id":0,"pet_id":3,"schedule_date_from":"2019-05-02","schedule_date_until":"2019-05-03","created_by":3,"updated_by":0,"deleted_at":"","created_at":"2019-05-27 06:40:06","updated_at":"2019-05-27 06:40:06"}
             */

            private int count;
            @SerializedName("cart_service_id")
            private int cartServiceId;
            @SerializedName("cart_service_type")
            private String cartServiceType;
            @SerializedName("order_code")
            private String orderCode;
            @SerializedName("pet_id")
            private int petId;
            @SerializedName("doctor_schedule_id")
            private int doctorScheduleId;
            @SerializedName("grooming_location_schedule_id")
            private int groomingLocationScheduleId;
            @SerializedName("boarding_schedule_id")
            private int boardingScheduleId;
            @SerializedName("payment_complete")
            private int paymentComplete;
            @SerializedName("has_anamnesis")
            private int hasAnamnesis;
            @SerializedName("take_action")
            private int takeAction;
            @SerializedName("is_invoice")
            private int isInvoice;
            @SerializedName("created_by")
            private int createdBy;
            @SerializedName("updated_by")
            private int updatedBy;
            @SerializedName("deleted_at")
            private String deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
            @SerializedName("pet")
            private PetBean pet;
            @SerializedName("doctor_schedule")
            private DoctorScheduleBean doctorSchedule;
            @SerializedName("grooming_location_schedule")
            private GroomingLocationScheduleBean groomingLocationSchedule;
            @SerializedName("boarding_schedule")
            private BoardingScheduleBean boardingSchedule;
            @SerializedName("items")
            private List<ItemsBean> items;
            private boolean isShowingDetail = false;

            public int getCartServiceId() {
                return cartServiceId;
            }

            public void setCartServiceId(int cartServiceId) {
                this.cartServiceId = cartServiceId;
            }

            public String getCartServiceType() {
                return cartServiceType;
            }

            public void setCartServiceType(String cartServiceType) {
                this.cartServiceType = cartServiceType;
            }

            public String getOrderCode() {
                return orderCode;
            }

            public void setOrderCode(String orderCode) {
                this.orderCode = orderCode;
            }

            public int getPetId() {
                return petId;
            }

            public void setPetId(int petId) {
                this.petId = petId;
            }

            public int getDoctorScheduleId() {
                return doctorScheduleId;
            }

            public void setDoctorScheduleId(int doctorScheduleId) {
                this.doctorScheduleId = doctorScheduleId;
            }

            public int getGroomingLocationScheduleId() {
                return groomingLocationScheduleId;
            }

            public void setGroomingLocationScheduleId(int groomingLocationScheduleId) {
                this.groomingLocationScheduleId = groomingLocationScheduleId;
            }

            public int getBoardingScheduleId() {
                return boardingScheduleId;
            }

            public void setBoardingScheduleId(int boardingScheduleId) {
                this.boardingScheduleId = boardingScheduleId;
            }

            public int getPaymentComplete() {
                return paymentComplete;
            }

            public void setPaymentComplete(int paymentComplete) {
                this.paymentComplete = paymentComplete;
            }

            public int getHasAnamnesis() {
                return hasAnamnesis;
            }

            public void setHasAnamnesis(int hasAnamnesis) {
                this.hasAnamnesis = hasAnamnesis;
            }

            public int getTakeAction() {
                return takeAction;
            }

            public void setTakeAction(int takeAction) {
                this.takeAction = takeAction;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(String deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public PetBean getPet() {
                return pet;
            }

            public void setPet(PetBean pet) {
                this.pet = pet;
            }

            public DoctorScheduleBean getDoctorSchedule() {
                return doctorSchedule;
            }

            public void setDoctorSchedule(DoctorScheduleBean doctorSchedule) {
                this.doctorSchedule = doctorSchedule;
            }

            public GroomingLocationScheduleBean getGroomingLocationSchedule() {
                return groomingLocationSchedule;
            }

            public void setGroomingLocationSchedule(GroomingLocationScheduleBean groomingLocationSchedule) {
                this.groomingLocationSchedule = groomingLocationSchedule;
            }

            public BoardingScheduleBean getBoardingSchedule() {
                return boardingSchedule;
            }

            public void setBoardingSchedule(BoardingScheduleBean boardingSchedule) {
                this.boardingSchedule = boardingSchedule;
            }

            public List<ItemsBean> getItems() {
                return items;
            }

            public void setItems(List<ItemsBean> items) {
                this.items = items;
            }

            public boolean isShowingDetail() {
                return isShowingDetail;
            }

            public void setShowingDetail(boolean showingDetail) {
                isShowingDetail = showingDetail;
            }

            public int getCount() {
                return count;
            }

            public void setCount(int count) {
                this.count = count;
            }

            public boolean isInvoice() {
                if (isInvoice == 0)
                    return false;
                return true;
            }

            public void setIsInvoice(int isInvoice) {
                this.isInvoice = isInvoice;
            }

            public static class PetBean {
                /**
                 * customer_name : Lara A. Harding
                 * pet_id : 3
                 * customer_id : 4
                 * pet_name : Vida
                 * pet_race : Angora
                 * pet_birthday : 2019-01-02
                 * pet_gender : 2
                 * pet_photo : http://192.168.1.82/images/customer-1557899398.png
                 * created_on : 1
                 * updated_on : 1
                 * created_by : 4
                 * updated_by : 4
                 * deleted_at :
                 * created_at : 2019-05-15 05:49:58
                 * updated_at : 2019-05-15 05:49:58
                 */

                @SerializedName("customer_name")
                private String customerName;
                @SerializedName("pet_id")
                private int petId;
                @SerializedName("customer_id")
                private int customerId;
                @SerializedName("pet_name")
                private String petName;
                @SerializedName("pet_race")
                private String petRace;
                @SerializedName("pet_birthday")
                private String petBirthday;
                @SerializedName("pet_gender")
                private String petGender;
                @SerializedName("pet_photo")
                private String petPhoto;
                @SerializedName("created_on")
                private String createdOn;
                @SerializedName("updated_on")
                private String updatedOn;
                @SerializedName("created_by")
                private int createdBy;
                @SerializedName("updated_by")
                private int updatedBy;
                @SerializedName("deleted_at")
                private String deletedAt;
                @SerializedName("created_at")
                private String createdAt;
                @SerializedName("updated_at")
                private String updatedAt;

                public String getCustomerName() {
                    return customerName;
                }

                public void setCustomerName(String customerName) {
                    this.customerName = customerName;
                }

                public int getPetId() {
                    return petId;
                }

                public void setPetId(int petId) {
                    this.petId = petId;
                }

                public int getCustomerId() {
                    return customerId;
                }

                public void setCustomerId(int customerId) {
                    this.customerId = customerId;
                }

                public String getPetName() {
                    return petName;
                }

                public void setPetName(String petName) {
                    this.petName = petName;
                }

                public String getPetRace() {
                    return petRace;
                }

                public void setPetRace(String petRace) {
                    this.petRace = petRace;
                }

                public String getPetBirthday() {
                    return petBirthday;
                }

                public void setPetBirthday(String petBirthday) {
                    this.petBirthday = petBirthday;
                }

                public String getPetGender() {
                    return petGender;
                }

                public void setPetGender(String petGender) {
                    this.petGender = petGender;
                }

                public String getPetPhoto() {
                    return petPhoto;
                }

                public void setPetPhoto(String petPhoto) {
                    this.petPhoto = petPhoto;
                }

                public String getCreatedOn() {
                    return createdOn;
                }

                public void setCreatedOn(String createdOn) {
                    this.createdOn = createdOn;
                }

                public String getUpdatedOn() {
                    return updatedOn;
                }

                public void setUpdatedOn(String updatedOn) {
                    this.updatedOn = updatedOn;
                }

                public int getCreatedBy() {
                    return createdBy;
                }

                public void setCreatedBy(int createdBy) {
                    this.createdBy = createdBy;
                }

                public int getUpdatedBy() {
                    return updatedBy;
                }

                public void setUpdatedBy(int updatedBy) {
                    this.updatedBy = updatedBy;
                }

                public String getDeletedAt() {
                    return deletedAt;
                }

                public void setDeletedAt(String deletedAt) {
                    this.deletedAt = deletedAt;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }
            }

            public static class DoctorScheduleBean {
                /**
                 * name : Siraja raja
                 * doctor_schedule_id : 29
                 * staff_id : 24
                 * reservation_id : 3
                 * pet_id : 3
                 * schedule_date : 2019-05-02
                 * schedule_time : 10:00:00
                 * created_by : 3
                 * updated_by : 0
                 * deleted_at :
                 * created_at : 2019-05-27 06:40:05
                 * updated_at : 2019-05-27 06:40:05
                 */

                @SerializedName("name")
                private String name;
                @SerializedName("doctor_schedule_id")
                private int doctorScheduleId;
                @SerializedName("staff_id")
                private int staffId;
                @SerializedName("reservation_id")
                private int reservationId;
                @SerializedName("pet_id")
                private int petId;
                @SerializedName("schedule_date")
                private String scheduleDate;
                @SerializedName("schedule_time")
                private String scheduleTime;
                @SerializedName("created_by")
                private int createdBy;
                @SerializedName("updated_by")
                private int updatedBy;
                @SerializedName("deleted_at")
                private String deletedAt;
                @SerializedName("created_at")
                private String createdAt;
                @SerializedName("updated_at")
                private String updatedAt;

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public int getDoctorScheduleId() {
                    return doctorScheduleId;
                }

                public void setDoctorScheduleId(int doctorScheduleId) {
                    this.doctorScheduleId = doctorScheduleId;
                }

                public int getStaffId() {
                    return staffId;
                }

                public void setStaffId(int staffId) {
                    this.staffId = staffId;
                }

                public int getReservationId() {
                    return reservationId;
                }

                public void setReservationId(int reservationId) {
                    this.reservationId = reservationId;
                }

                public int getPetId() {
                    return petId;
                }

                public void setPetId(int petId) {
                    this.petId = petId;
                }

                public String getScheduleDate() {
                    return scheduleDate;
                }

                public void setScheduleDate(String scheduleDate) {
                    this.scheduleDate = scheduleDate;
                }

                public String getScheduleTime() {
                    return scheduleTime;
                }

                public void setScheduleTime(String scheduleTime) {
                    this.scheduleTime = scheduleTime;
                }

                public int getCreatedBy() {
                    return createdBy;
                }

                public void setCreatedBy(int createdBy) {
                    this.createdBy = createdBy;
                }

                public int getUpdatedBy() {
                    return updatedBy;
                }

                public void setUpdatedBy(int updatedBy) {
                    this.updatedBy = updatedBy;
                }

                public String getDeletedAt() {
                    return deletedAt;
                }

                public void setDeletedAt(String deletedAt) {
                    this.deletedAt = deletedAt;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }
            }

            public static class GroomingLocationScheduleBean {
                /**
                 * location_name : Ruangan #2
                 * grooming_location_schedule_id : 23
                 * grooming_location_id : 2
                 * reservation_id : 3
                 * customer_id : 0
                 * pet_id : 3
                 * schedule_date : 2019-05-02
                 * schedule_time : 10:00:00
                 * created_by : 3
                 * updated_by : 0
                 * deleted_at :
                 * created_at : 2019-05-27 06:40:05
                 * updated_at : 2019-05-27 06:40:05
                 */

                @SerializedName("location_name")
                private String locationName;
                @SerializedName("grooming_location_schedule_id")
                private int groomingLocationScheduleId;
                @SerializedName("grooming_location_id")
                private int groomingLocationId;
                @SerializedName("reservation_id")
                private int reservationId;
                @SerializedName("customer_id")
                private int customerId;
                @SerializedName("pet_id")
                private int petId;
                @SerializedName("schedule_date")
                private String scheduleDate;
                @SerializedName("schedule_time")
                private String scheduleTime;
                @SerializedName("created_by")
                private int createdBy;
                @SerializedName("updated_by")
                private int updatedBy;
                @SerializedName("deleted_at")
                private String deletedAt;
                @SerializedName("created_at")
                private String createdAt;
                @SerializedName("updated_at")
                private String updatedAt;

                public String getLocationName() {
                    return locationName;
                }

                public void setLocationName(String locationName) {
                    this.locationName = locationName;
                }

                public int getGroomingLocationScheduleId() {
                    return groomingLocationScheduleId;
                }

                public void setGroomingLocationScheduleId(int groomingLocationScheduleId) {
                    this.groomingLocationScheduleId = groomingLocationScheduleId;
                }

                public int getGroomingLocationId() {
                    return groomingLocationId;
                }

                public void setGroomingLocationId(int groomingLocationId) {
                    this.groomingLocationId = groomingLocationId;
                }

                public int getReservationId() {
                    return reservationId;
                }

                public void setReservationId(int reservationId) {
                    this.reservationId = reservationId;
                }

                public int getCustomerId() {
                    return customerId;
                }

                public void setCustomerId(int customerId) {
                    this.customerId = customerId;
                }

                public int getPetId() {
                    return petId;
                }

                public void setPetId(int petId) {
                    this.petId = petId;
                }

                public String getScheduleDate() {
                    return scheduleDate;
                }

                public void setScheduleDate(String scheduleDate) {
                    this.scheduleDate = scheduleDate;
                }

                public String getScheduleTime() {
                    return scheduleTime;
                }

                public void setScheduleTime(String scheduleTime) {
                    this.scheduleTime = scheduleTime;
                }

                public int getCreatedBy() {
                    return createdBy;
                }

                public void setCreatedBy(int createdBy) {
                    this.createdBy = createdBy;
                }

                public int getUpdatedBy() {
                    return updatedBy;
                }

                public void setUpdatedBy(int updatedBy) {
                    this.updatedBy = updatedBy;
                }

                public String getDeletedAt() {
                    return deletedAt;
                }

                public void setDeletedAt(String deletedAt) {
                    this.deletedAt = deletedAt;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }
            }

            public static class BoardingScheduleBean {
                /**
                 * cage_name : Kandang Kucing S
                 * cage_schedule_id : 20
                 * cage_id : 1
                 * reservation_id : 3
                 * customer_id : 0
                 * pet_id : 3
                 * schedule_date_from : 2019-05-02
                 * schedule_date_until : 2019-05-03
                 * created_by : 3
                 * updated_by : 0
                 * deleted_at :
                 * created_at : 2019-05-27 06:40:06
                 * updated_at : 2019-05-27 06:40:06
                 */

                @SerializedName("cage_name")
                private String cageName;
                @SerializedName("cage_schedule_id")
                private int cageScheduleId;
                @SerializedName("cage_id")
                private int cageId;
                @SerializedName("reservation_id")
                private int reservationId;
                @SerializedName("customer_id")
                private int customerId;
                @SerializedName("pet_id")
                private int petId;
                @SerializedName("schedule_date_from")
                private String scheduleDateFrom;
                @SerializedName("schedule_date_until")
                private String scheduleDateUntil;
                @SerializedName("created_by")
                private int createdBy;
                @SerializedName("updated_by")
                private int updatedBy;
                @SerializedName("deleted_at")
                private String deletedAt;
                @SerializedName("created_at")
                private String createdAt;
                @SerializedName("updated_at")
                private String updatedAt;

                public String getCageName() {
                    return cageName;
                }

                public void setCageName(String cageName) {
                    this.cageName = cageName;
                }

                public int getCageScheduleId() {
                    return cageScheduleId;
                }

                public void setCageScheduleId(int cageScheduleId) {
                    this.cageScheduleId = cageScheduleId;
                }

                public int getCageId() {
                    return cageId;
                }

                public void setCageId(int cageId) {
                    this.cageId = cageId;
                }

                public int getReservationId() {
                    return reservationId;
                }

                public void setReservationId(int reservationId) {
                    this.reservationId = reservationId;
                }

                public int getCustomerId() {
                    return customerId;
                }

                public void setCustomerId(int customerId) {
                    this.customerId = customerId;
                }

                public int getPetId() {
                    return petId;
                }

                public void setPetId(int petId) {
                    this.petId = petId;
                }

                public String getScheduleDateFrom() {
                    return scheduleDateFrom;
                }

                public void setScheduleDateFrom(String scheduleDateFrom) {
                    this.scheduleDateFrom = scheduleDateFrom;
                }

                public String getScheduleDateUntil() {
                    return scheduleDateUntil;
                }

                public void setScheduleDateUntil(String scheduleDateUntil) {
                    this.scheduleDateUntil = scheduleDateUntil;
                }

                public int getCreatedBy() {
                    return createdBy;
                }

                public void setCreatedBy(int createdBy) {
                    this.createdBy = createdBy;
                }

                public int getUpdatedBy() {
                    return updatedBy;
                }

                public void setUpdatedBy(int updatedBy) {
                    this.updatedBy = updatedBy;
                }

                public String getDeletedAt() {
                    return deletedAt;
                }

                public void setDeletedAt(String deletedAt) {
                    this.deletedAt = deletedAt;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }
            }

            public static class ItemsBean {
                /**
                 * cart_service_detail_id : 26
                 * cart_service_id : 3
                 * service_type : 3
                 * service_id : 1
                 * service_name : Test
                 * service_price : 20000.00
                 * order_status : 1
                 * is_invoice : 0
                 * created_by : 3
                 * updated_by : 0
                 * deleted_at :
                 * created_at : 2019-05-27 06:40:06
                 * updated_at : 2019-05-27 06:40:06
                 */

                @SerializedName("cart_service_detail_id")
                private int cartServiceDetailId;
                @SerializedName("cart_service_id")
                private int cartServiceId;
                @SerializedName("service_type")
                private String serviceType;
                @SerializedName("service_id")
                private int serviceId;
                @SerializedName("service_name")
                private String serviceName;
                @SerializedName("service_price")
                private String servicePrice;
                @SerializedName("order_status")
                private String orderStatus;
                @SerializedName("is_invoice")
                private int isInvoice;
                @SerializedName("created_by")
                private int createdBy;
                @SerializedName("updated_by")
                private int updatedBy;
                @SerializedName("deleted_at")
                private String deletedAt;
                @SerializedName("created_at")
                private String createdAt;
                @SerializedName("updated_at")
                private String updatedAt;

                public int getCartServiceDetailId() {
                    return cartServiceDetailId;
                }

                public void setCartServiceDetailId(int cartServiceDetailId) {
                    this.cartServiceDetailId = cartServiceDetailId;
                }

                public int getCartServiceId() {
                    return cartServiceId;
                }

                public void setCartServiceId(int cartServiceId) {
                    this.cartServiceId = cartServiceId;
                }

                public String getServiceType() {
                    return serviceType;
                }

                public void setServiceType(String serviceType) {
                    this.serviceType = serviceType;
                }

                public int getServiceId() {
                    return serviceId;
                }

                public void setServiceId(int serviceId) {
                    this.serviceId = serviceId;
                }

                public String getServiceName() {
                    return serviceName;
                }

                public void setServiceName(String serviceName) {
                    this.serviceName = serviceName;
                }

                public String getServicePrice() {
                    return servicePrice;
                }

                public void setServicePrice(String servicePrice) {
                    this.servicePrice = servicePrice;
                }

                public String getOrderStatus() {
                    return orderStatus;
                }

                public void setOrderStatus(String orderStatus) {
                    this.orderStatus = orderStatus;
                }

                public boolean isInvoice() {
                    if (isInvoice == 0)
                        return false;
                    return true;
                }

                public void setIsInvoice(int isInvoice) {
                    this.isInvoice = isInvoice;
                }

                public int getCreatedBy() {
                    return createdBy;
                }

                public void setCreatedBy(int createdBy) {
                    this.createdBy = createdBy;
                }

                public int getUpdatedBy() {
                    return updatedBy;
                }

                public void setUpdatedBy(int updatedBy) {
                    this.updatedBy = updatedBy;
                }

                public String getDeletedAt() {
                    return deletedAt;
                }

                public void setDeletedAt(String deletedAt) {
                    this.deletedAt = deletedAt;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }
            }
        }
    }
}
