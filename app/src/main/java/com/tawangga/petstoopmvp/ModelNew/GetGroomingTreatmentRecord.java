package com.tawangga.petstoopmvp.ModelNew;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetGroomingTreatmentRecord {
    /**
     * STATUS_CODE : 200
     * MESSAGE : Success
     * DATA : {"page":1,"last_page":1,"total_item":2,"limit":10,"items":[{"pet_id":1,"cart_service_id":1,"grooming_location_schedule_id":1,"order_code":"R#1560936965-1","order_date":"2019-06-19","service_type":"2","service_text":"Potong kuku anjing, Potong kuku kucing","grooming_location_schedule":{"location_name":"Ruangan #1","grooming_location_schedule_id":1,"grooming_location_id":1,"reservation_id":1,"customer_id":0,"pet_id":1,"schedule_date":"2019-06-19","schedule_time":"09:00:00","created_by":1,"updated_by":0,"deleted_at":null,"created_at":"2019-06-19 09:43:09","updated_at":"2019-06-19 09:43:09"}},{"pet_id":1,"cart_service_id":5,"grooming_location_schedule_id":4,"order_code":"R#1560976994-1","order_date":"2019-05-02","service_type":"2","service_text":"SPA Kucing biasa","grooming_location_schedule":{"location_name":"Ruangan #2","grooming_location_schedule_id":4,"grooming_location_id":2,"reservation_id":5,"customer_id":0,"pet_id":1,"schedule_date":"2019-05-02","schedule_time":"10:00:00","created_by":3,"updated_by":0,"deleted_at":null,"created_at":"2019-06-19 20:45:53","updated_at":"2019-06-19 20:45:53"}}]}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * page : 1
         * last_page : 1
         * total_item : 2
         * limit : 10
         * items : [{"pet_id":1,"cart_service_id":1,"grooming_location_schedule_id":1,"order_code":"R#1560936965-1","order_date":"2019-06-19","service_type":"2","service_text":"Potong kuku anjing, Potong kuku kucing","grooming_location_schedule":{"location_name":"Ruangan #1","grooming_location_schedule_id":1,"grooming_location_id":1,"reservation_id":1,"customer_id":0,"pet_id":1,"schedule_date":"2019-06-19","schedule_time":"09:00:00","created_by":1,"updated_by":0,"deleted_at":null,"created_at":"2019-06-19 09:43:09","updated_at":"2019-06-19 09:43:09"}},{"pet_id":1,"cart_service_id":5,"grooming_location_schedule_id":4,"order_code":"R#1560976994-1","order_date":"2019-05-02","service_type":"2","service_text":"SPA Kucing biasa","grooming_location_schedule":{"location_name":"Ruangan #2","grooming_location_schedule_id":4,"grooming_location_id":2,"reservation_id":5,"customer_id":0,"pet_id":1,"schedule_date":"2019-05-02","schedule_time":"10:00:00","created_by":3,"updated_by":0,"deleted_at":null,"created_at":"2019-06-19 20:45:53","updated_at":"2019-06-19 20:45:53"}}]
         */

        @SerializedName("page")
        private int page;
        @SerializedName("last_page")
        private int lastPage;
        @SerializedName("total_item")
        private int totalItem;
        @SerializedName("limit")
        private int limit;
        @SerializedName("items")
        private List<ItemsBean> items;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public int getTotalItem() {
            return totalItem;
        }

        public void setTotalItem(int totalItem) {
            this.totalItem = totalItem;
        }

        public int getLimit() {
            return limit;
        }

        public void setLimit(int limit) {
            this.limit = limit;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class ItemsBean {
            /**
             * pet_id : 1
             * cart_service_id : 1
             * grooming_location_schedule_id : 1
             * order_code : R#1560936965-1
             * order_date : 2019-06-19
             * service_type : 2
             * service_text : Potong kuku anjing, Potong kuku kucing
             * grooming_location_schedule : {"location_name":"Ruangan #1","grooming_location_schedule_id":1,"grooming_location_id":1,"reservation_id":1,"customer_id":0,"pet_id":1,"schedule_date":"2019-06-19","schedule_time":"09:00:00","created_by":1,"updated_by":0,"deleted_at":null,"created_at":"2019-06-19 09:43:09","updated_at":"2019-06-19 09:43:09"}
             */

            @SerializedName("pet_id")
            private int petId;
            @SerializedName("cart_service_id")
            private int cartServiceId;
            @SerializedName("grooming_location_schedule_id")
            private int groomingLocationScheduleId;
            @SerializedName("order_code")
            private String orderCode;
            @SerializedName("order_date")
            private String orderDate;
            @SerializedName("service_type")
            private String serviceType;
            @SerializedName("service_text")
            private String serviceText;
            @SerializedName("grooming_location_schedule")
            private GroomingLocationScheduleBean groomingLocationSchedule;

            public int getPetId() {
                return petId;
            }

            public void setPetId(int petId) {
                this.petId = petId;
            }

            public int getCartServiceId() {
                return cartServiceId;
            }

            public void setCartServiceId(int cartServiceId) {
                this.cartServiceId = cartServiceId;
            }

            public int getGroomingLocationScheduleId() {
                return groomingLocationScheduleId;
            }

            public void setGroomingLocationScheduleId(int groomingLocationScheduleId) {
                this.groomingLocationScheduleId = groomingLocationScheduleId;
            }

            public String getOrderCode() {
                return orderCode;
            }

            public void setOrderCode(String orderCode) {
                this.orderCode = orderCode;
            }

            public String getOrderDate() {
                return orderDate;
            }

            public void setOrderDate(String orderDate) {
                this.orderDate = orderDate;
            }

            public String getServiceType() {
                return serviceType;
            }

            public void setServiceType(String serviceType) {
                this.serviceType = serviceType;
            }

            public String getServiceText() {
                return serviceText;
            }

            public void setServiceText(String serviceText) {
                this.serviceText = serviceText;
            }

            public GroomingLocationScheduleBean getGroomingLocationSchedule() {
                return groomingLocationSchedule;
            }

            public void setGroomingLocationSchedule(GroomingLocationScheduleBean groomingLocationSchedule) {
                this.groomingLocationSchedule = groomingLocationSchedule;
            }

            public static class GroomingLocationScheduleBean {
                /**
                 * location_name : Ruangan #1
                 * grooming_location_schedule_id : 1
                 * grooming_location_id : 1
                 * reservation_id : 1
                 * customer_id : 0
                 * pet_id : 1
                 * schedule_date : 2019-06-19
                 * schedule_time : 09:00:00
                 * created_by : 1
                 * updated_by : 0
                 * deleted_at : null
                 * created_at : 2019-06-19 09:43:09
                 * updated_at : 2019-06-19 09:43:09
                 */

                @SerializedName("location_name")
                private String locationName;
                @SerializedName("grooming_location_schedule_id")
                private int groomingLocationScheduleId;
                @SerializedName("grooming_location_id")
                private int groomingLocationId;
                @SerializedName("reservation_id")
                private int reservationId;
                @SerializedName("customer_id")
                private int customerId;
                @SerializedName("pet_id")
                private int petId;
                @SerializedName("schedule_date")
                private String scheduleDate;
                @SerializedName("schedule_time")
                private String scheduleTime;
                @SerializedName("created_by")
                private int createdBy;
                @SerializedName("updated_by")
                private int updatedBy;
                @SerializedName("deleted_at")
                private Object deletedAt;
                @SerializedName("created_at")
                private String createdAt;
                @SerializedName("updated_at")
                private String updatedAt;

                public String getLocationName() {
                    return locationName;
                }

                public void setLocationName(String locationName) {
                    this.locationName = locationName;
                }

                public int getGroomingLocationScheduleId() {
                    return groomingLocationScheduleId;
                }

                public void setGroomingLocationScheduleId(int groomingLocationScheduleId) {
                    this.groomingLocationScheduleId = groomingLocationScheduleId;
                }

                public int getGroomingLocationId() {
                    return groomingLocationId;
                }

                public void setGroomingLocationId(int groomingLocationId) {
                    this.groomingLocationId = groomingLocationId;
                }

                public int getReservationId() {
                    return reservationId;
                }

                public void setReservationId(int reservationId) {
                    this.reservationId = reservationId;
                }

                public int getCustomerId() {
                    return customerId;
                }

                public void setCustomerId(int customerId) {
                    this.customerId = customerId;
                }

                public int getPetId() {
                    return petId;
                }

                public void setPetId(int petId) {
                    this.petId = petId;
                }

                public String getScheduleDate() {
                    return scheduleDate;
                }

                public void setScheduleDate(String scheduleDate) {
                    this.scheduleDate = scheduleDate;
                }

                public String getScheduleTime() {
                    return scheduleTime;
                }

                public void setScheduleTime(String scheduleTime) {
                    this.scheduleTime = scheduleTime;
                }

                public int getCreatedBy() {
                    return createdBy;
                }

                public void setCreatedBy(int createdBy) {
                    this.createdBy = createdBy;
                }

                public int getUpdatedBy() {
                    return updatedBy;
                }

                public void setUpdatedBy(int updatedBy) {
                    this.updatedBy = updatedBy;
                }

                public Object getDeletedAt() {
                    return deletedAt;
                }

                public void setDeletedAt(Object deletedAt) {
                    this.deletedAt = deletedAt;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }
            }
        }
    }
}
