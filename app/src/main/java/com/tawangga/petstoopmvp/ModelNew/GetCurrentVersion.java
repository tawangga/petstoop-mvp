package com.tawangga.petstoopmvp.ModelNew;

import com.google.gson.annotations.SerializedName;

public class GetCurrentVersion {


    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * version_id : 3
         * version_name : 1.2
         * change_log : - Fix previous bugs
         - Change some  logical
         * created_by : 4
         * updated_by : 0
         * deleted_at : null
         * created_at : 2019-06-20 15:20:45
         * updated_at : 2019-06-20 15:20:45
         */

        @SerializedName("version_id")
        private int versionId;
        @SerializedName("version_name")
        private String versionName;
        @SerializedName("change_log")
        private String changeLog;
        @SerializedName("created_by")
        private int createdBy;
        @SerializedName("updated_by")
        private int updatedBy;
        @SerializedName("deleted_at")
        private Object deletedAt;
        @SerializedName("created_at")
        private String createdAt;
        @SerializedName("updated_at")
        private String updatedAt;

        public int getVersionId() {
            return versionId;
        }

        public void setVersionId(int versionId) {
            this.versionId = versionId;
        }

        public String getVersionName() {
            return versionName;
        }

        public void setVersionName(String versionName) {
            this.versionName = versionName;
        }

        public String getChangeLog() {
            return changeLog;
        }

        public void setChangeLog(String changeLog) {
            this.changeLog = changeLog;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public int getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            this.updatedBy = updatedBy;
        }

        public Object getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(Object deletedAt) {
            this.deletedAt = deletedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }
    }
}
