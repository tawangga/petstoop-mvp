package com.tawangga.petstoopmvp.Param;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddOrderToInvoice {

    /**
     * CREATED_BY : 1
     * INVOICE :
     * DATA : [{"order_id":9,"order_type":2,"order_items":[96,98]},{"order_id":13,"order_type":1,"order_items":[25,27]}]
     */

    @SerializedName("CREATED_BY")
    private int CREATEDBY;
    @SerializedName("INVOICE")
    private String INVOICE;
    @SerializedName("DATA")
    private List<DATAorder_to_invoice> DATA;

    public int getCREATEDBY() {
        return CREATEDBY;
    }

    public void setCREATEDBY(int CREATEDBY) {
        this.CREATEDBY = CREATEDBY;
    }

    public String getINVOICE() {
        return INVOICE;
    }

    public void setINVOICE(String INVOICE) {
        this.INVOICE = INVOICE;
    }

    public List<DATAorder_to_invoice> getDATA() {
        return DATA;
    }

    public void setDATA(List<DATAorder_to_invoice> DATA) {
        this.DATA = DATA;
    }

    public static class DATAorder_to_invoice {

        public DATAorder_to_invoice(int orderId, int orderType, List<Integer> orderItems) {
            this.orderId = orderId;
            this.orderType = orderType;
            this.orderItems = orderItems;
        }

        /**
         * order_id : 9
         * order_type : 2
         * order_items : [96,98]
         */


        @SerializedName("order_id")
        private int orderId;
        @SerializedName("order_type")
        private int orderType;
        @SerializedName("order_items")
        private List<Integer> orderItems;

        public int getOrderId() {
            return orderId;
        }

        public int getOrderType() {
            return orderType;
        }

        public List<Integer> getOrderItems() {
            return orderItems;
        }

        public void setOrderItems(List<Integer> orderItems) {
            this.orderItems = orderItems;
        }
    }
}



