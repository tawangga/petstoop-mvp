package com.tawangga.petstoopmvp.Param;

import com.google.gson.annotations.SerializedName;
import com.tawangga.petstoopmvp.Model.GetOrders;

import java.util.List;


public class AddCartToInvoice {
    /**
     * transaction_id : 0
     * items : [{"order_id":2,"order_type":1,"order_type_name":"Shop","order_code":"S#1558950055-1","customer_id":1,"customer_name":"Edinofri Karizal Caniago","pet_name":"","pet_id":0,"items":[{"item_order_id":10,"item_id":1,"item_name":"Obat Kutu","item_type":"","item_price":25000,"item_qty":1,"item_note":"","item_disc_id":0},{"item_order_id":11,"item_id":3,"item_name":"Obat Flu Kucing","item_type":"","item_price":35000,"item_qty":21,"item_note":"","item_disc_id":0}]}]
     */

    @SerializedName("transaction_id")
    private int transactionId;
    @SerializedName("items")
    private List<GetOrders.DATABean> items;

    public int getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }

    public List<GetOrders.DATABean> getItems() {
        return items;
    }

    public void setItems(List<GetOrders.DATABean> items) {
        this.items = items;
    }

}
