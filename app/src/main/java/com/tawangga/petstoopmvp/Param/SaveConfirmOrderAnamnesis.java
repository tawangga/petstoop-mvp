package com.tawangga.petstoopmvp.Param;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SaveConfirmOrderAnamnesis {

    /**
     * anamnesis : .
     * diagnosis : .
     * return_schedule_date : 2019-05-27
     * return_schedule_desc : .
     * is_take_action : false
     * clinical_checkup : [{"clinical_checkup_id":1,"clinical_checkup_name":"Berat Badan","value":"39","is_exam":0},{"clinical_checkup_id":2,"clinical_checkup_name":"Temperatur","value":"35","is_exam":0}]
     */

    @SerializedName("anamnesis")
    private String anamnesis;
    @SerializedName("diagnosis")
    private String diagnosis;
    @SerializedName("return_schedule_date")
    private String returnScheduleDate;
    @SerializedName("return_schedule_desc")
    private String returnScheduleDesc;
    @SerializedName("is_take_action")
    private boolean isTakeAction;
    @SerializedName("clinical_checkup")
    private List<ClinicalCheckupBean> clinicalCheckup;

    public String getAnamnesis() {
        return anamnesis;
    }

    public void setAnamnesis(String anamnesis) {
        this.anamnesis = anamnesis;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getReturnScheduleDate() {
        return returnScheduleDate;
    }

    public void setReturnScheduleDate(String returnScheduleDate) {
        this.returnScheduleDate = returnScheduleDate;
    }

    public String getReturnScheduleDesc() {
        return returnScheduleDesc;
    }

    public void setReturnScheduleDesc(String returnScheduleDesc) {
        this.returnScheduleDesc = returnScheduleDesc;
    }

    public boolean isIsTakeAction() {
        return isTakeAction;
    }

    public void setIsTakeAction(boolean isTakeAction) {
        this.isTakeAction = isTakeAction;
    }

    public List<ClinicalCheckupBean> getClinicalCheckup() {
        return clinicalCheckup;
    }

    public void setClinicalCheckup(List<ClinicalCheckupBean> clinicalCheckup) {
        this.clinicalCheckup = clinicalCheckup;
    }

    public static class ClinicalCheckupBean {
        /**
         * clinical_checkup_id : 1
         * clinical_checkup_name : Berat Badan
         * value : 39
         * is_exam : 0
         */

        @SerializedName("clinical_checkup_id")
        private int clinicalCheckupId;
        @SerializedName("clinical_checkup_name")
        private String clinicalCheckupName;
        @SerializedName("value")
        private String value;
        @SerializedName("is_exam")
        private int isExam;
        @SerializedName("clinical_checkup_unit")
        private String clinicalCheckupUnit;

        public int getClinicalCheckupId() {
            return clinicalCheckupId;
        }

        public void setClinicalCheckupId(int clinicalCheckupId) {
            this.clinicalCheckupId = clinicalCheckupId;
        }

        public String getClinicalCheckupName() {
            return clinicalCheckupName;
        }

        public void setClinicalCheckupName(String clinicalCheckupName) {
            this.clinicalCheckupName = clinicalCheckupName;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public int getIsExam() {
            return isExam;
        }

        public void setIsExam(int isExam) {
            this.isExam = isExam;
        }

        public String getClinicalCheckupUnit() {
            return clinicalCheckupUnit;
        }

        public void setClinicalCheckupUnit(String clinicalCheckupUnit) {
            this.clinicalCheckupUnit = clinicalCheckupUnit;
        }
    }
}
