package com.tawangga.petstoopmvp.Param;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddMedicalService {
    @SerializedName("SERVICE")
    private List<SERVICEBean> SERVICE;

    public List<SERVICEBean> getSERVICE() {
        return SERVICE;
    }

    public void setSERVICE(List<SERVICEBean> SERVICE) {
        this.SERVICE = SERVICE;
    }

    public static class SERVICEBean {
        /**
         * CART_SERVICE_ID : 1
         * SERVICE_ID : 3
         * SERVICE_NAME : test
         * SERVICE_PRICE : 10000
         * SERVICE_TYPE : 1
         */

        @SerializedName("CART_SERVICE_ID")
        private int CARTSERVICEID;
        @SerializedName("SERVICE_ID")
        private int SERVICEID;
        @SerializedName("SERVICE_NAME")
        private String SERVICENAME;
        @SerializedName("SERVICE_PRICE")
        private int SERVICEPRICE;
        @SerializedName("SERVICE_TYPE")
        private String SERVICETYPE;

        public int getCARTSERVICEID() {
            return CARTSERVICEID;
        }

        public void setCARTSERVICEID(int CARTSERVICEID) {
            this.CARTSERVICEID = CARTSERVICEID;
        }

        public int getSERVICEID() {
            return SERVICEID;
        }

        public void setSERVICEID(int SERVICEID) {
            this.SERVICEID = SERVICEID;
        }

        public String getSERVICENAME() {
            return SERVICENAME;
        }

        public void setSERVICENAME(String SERVICENAME) {
            this.SERVICENAME = SERVICENAME;
        }

        public int getSERVICEPRICE() {
            return SERVICEPRICE;
        }

        public void setSERVICEPRICE(int SERVICEPRICE) {
            this.SERVICEPRICE = SERVICEPRICE;
        }

        public String getSERVICETYPE() {
            return SERVICETYPE;
        }

        public void setSERVICETYPE(String SERVICETYPE) {
            this.SERVICETYPE = SERVICETYPE;
        }
    }
}
