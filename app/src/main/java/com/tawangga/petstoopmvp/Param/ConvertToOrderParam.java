package com.tawangga.petstoopmvp.Param;

import java.util.List;

public class ConvertToOrderParam {
    private int cart_service_id;
    private List<Integer> deleted_services;

    public ConvertToOrderParam(int cart_service_id, List<Integer> deleted_services) {
        this.cart_service_id = cart_service_id;
        this.deleted_services = deleted_services;
    }

    public int getCart_service_id() {
        return cart_service_id;
    }

    public void setCart_service_id(int cart_service_id) {
        this.cart_service_id = cart_service_id;
    }

    public List<Integer> getDeleted_services() {
        return deleted_services;
    }

    public void setDeleted_services(List<Integer> deleted_services) {
        this.deleted_services = deleted_services;
    }
}
