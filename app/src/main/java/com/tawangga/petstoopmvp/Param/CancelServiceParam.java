package com.tawangga.petstoopmvp.Param;

import com.google.gson.annotations.SerializedName;
import com.tawangga.petstoopmvp.ModelNew.CreateCart;

import java.util.List;


public class CancelServiceParam {
    /**
     * cart_service_id : 26
     * medical_service_old : [{"cart_service_detail_id":530,"cart_service_id":344,"site_id":37,"service_type":"1","service_id":1,"service_name":"Konsultasi Dokter","service_price":"165000.00","service_qty":1,"order_status":"1","is_invoice":0,"created_by":2,"updated_by":0,"deleted_at":null,"created_at":"2019-10-08 09:43:48","updated_at":"2019-10-08 09:43:48"},{"cart_service_detail_id":531,"cart_service_id":344,"site_id":37,"service_type":"1","service_id":7,"service_name":"Suntik Vaksin","service_price":"200000.00","service_qty":1,"order_status":"1","is_invoice":0,"created_by":2,"updated_by":0,"deleted_at":null,"created_at":"2019-10-08 09:43:50","updated_at":"2019-10-08 09:43:50"}]
     * grooming_service_old : [{"cart_service_detail_id":538,"cart_service_id":344,"site_id":37,"service_type":"2","service_id":1,"service_name":"Keramas","service_price":"100000.00","service_qty":1,"order_status":"1","is_invoice":0,"created_by":2,"updated_by":0,"deleted_at":null,"created_at":"2019-10-08 10:23:40","updated_at":"2019-10-08 10:23:40"},{"cart_service_detail_id":539,"cart_service_id":344,"site_id":37,"service_type":"2","service_id":2,"service_name":"Bersihkan Kuku","service_price":"250000.00","service_qty":1,"order_status":"1","is_invoice":0,"created_by":2,"updated_by":0,"deleted_at":null,"created_at":"2019-10-08 10:23:44","updated_at":"2019-10-08 10:23:44"}]
     */

    @SerializedName("cart_service_id")
    private int cartServiceId;
    @SerializedName("medical_service_old")
    private List<CreateCart.DATABean.ServiceBean> medicalServiceOld;
    @SerializedName("grooming_service_old")
    private List<CreateCart.DATABean.ServiceBean> groomingServiceOld;

    public int getCartServiceId() {
        return cartServiceId;
    }

    public void setCartServiceId(int cartServiceId) {
        this.cartServiceId = cartServiceId;
    }

    public List<CreateCart.DATABean.ServiceBean> getMedicalServiceOld() {
        return medicalServiceOld;
    }

    public void setMedicalServiceOld(List<CreateCart.DATABean.ServiceBean> medicalServiceOld) {
        this.medicalServiceOld = medicalServiceOld;
    }

    public List<CreateCart.DATABean.ServiceBean> getGroomingServiceOld() {
        return groomingServiceOld;
    }

    public void setGroomingServiceOld(List<CreateCart.DATABean.ServiceBean> groomingServiceOld) {
        this.groomingServiceOld = groomingServiceOld;
    }

}
