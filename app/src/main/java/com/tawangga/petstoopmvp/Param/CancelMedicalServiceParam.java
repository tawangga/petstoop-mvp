package com.tawangga.petstoopmvp.Param;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CancelMedicalServiceParam {
    /**
     * cart_service_id : 3
     * medical_item_old : [{"cart_service_id":3,"product_id":1,"product_name":"Equilibrio Persian Cat Makanan Kucing Persia 7 kg","product_price":"460000.00","product_qty":1,"product_note":"","discount_id":0,"discount_name":"","doctor_id":1,"doctor_name":"Dokter","is_invoice":0,"created_by":1,"updated_by":0,"deleted_at":null,"created_at":"2019-10-09 13:43:13","updated_at":"2019-10-09 13:43:13"}]
     */

    @SerializedName("cart_service_id")
    private int cartServiceId;
    @SerializedName("medical_item_old")
    private List<MedicalItemOldBean> medicalItemOld;

    public int getCartServiceId() {
        return cartServiceId;
    }

    public void setCartServiceId(int cartServiceId) {
        this.cartServiceId = cartServiceId;
    }

    public List<MedicalItemOldBean> getMedicalItemOld() {
        return medicalItemOld;
    }

    public void setMedicalItemOld(List<MedicalItemOldBean> medicalItemOld) {
        this.medicalItemOld = medicalItemOld;
    }

    public static class MedicalItemOldBean {
        /**
         * cart_service_id : 3
         * product_id : 1
         * product_name : Equilibrio Persian Cat Makanan Kucing Persia 7 kg
         * product_price : 460000.00
         * product_qty : 1
         * product_note :
         * discount_id : 0
         * discount_name :
         * doctor_id : 1
         * doctor_name : Dokter
         * is_invoice : 0
         * created_by : 1
         * updated_by : 0
         * deleted_at : null
         * created_at : 2019-10-09 13:43:13
         * updated_at : 2019-10-09 13:43:13
         */

        @SerializedName("cart_service_id")
        private int cartServiceId;
        @SerializedName("product_id")
        private int productId;
        @SerializedName("product_name")
        private String productName;
        @SerializedName("product_price")
        private String productPrice;
        @SerializedName("product_qty")
        private int productQty;
        @SerializedName("product_note")
        private String productNote;
        @SerializedName("discount_id")
        private int discountId;
        @SerializedName("discount_amount")
        private double discountAmount;
        @SerializedName("discount_name")

        private String discountName;
        @SerializedName("doctor_id")
        private int doctorId;
        @SerializedName("doctor_name")
        private String doctorName;
        @SerializedName("is_invoice")
        private int isInvoice;
        @SerializedName("created_by")
        private int createdBy;
        @SerializedName("updated_by")
        private int updatedBy;
        @SerializedName("deleted_at")
        private String deletedAt;
        @SerializedName("created_at")
        private String createdAt;
        @SerializedName("category_id")
        private int categoryId;
        @SerializedName("category_name")
        private String categoryName;
        @SerializedName("product_code")
        private String productCode;

        public double getDiscountAmount() {
            return discountAmount;
        }

        public void setDiscountAmount(double discountAmount) {
            this.discountAmount = discountAmount;
        }

        @SerializedName("updated_at")
        private String updatedAt;

        public int getCartServiceId() {
            return cartServiceId;
        }

        public void setCartServiceId(int cartServiceId) {
            this.cartServiceId = cartServiceId;
        }

        public int getProductId() {
            return productId;
        }

        public void setProductId(int productId) {
            this.productId = productId;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getProductPrice() {
            return productPrice;
        }

        public void setProductPrice(String productPrice) {
            this.productPrice = productPrice;
        }

        public int getProductQty() {
            return productQty;
        }

        public void setProductQty(int productQty) {
            this.productQty = productQty;
        }

        public String getProductNote() {
            return productNote;
        }

        public void setProductNote(String productNote) {
            this.productNote = productNote;
        }

        public int getDiscountId() {
            return discountId;
        }

        public void setDiscountId(int discountId) {
            this.discountId = discountId;
        }

        public String getDiscountName() {
            return discountName;
        }

        public void setDiscountName(String discountName) {
            this.discountName = discountName;
        }

        public int getDoctorId() {
            return doctorId;
        }

        public void setDoctorId(int doctorId) {
            this.doctorId = doctorId;
        }

        public String getDoctorName() {
            return doctorName;
        }

        public void setDoctorName(String doctorName) {
            this.doctorName = doctorName;
        }

        public int getIsInvoice() {
            return isInvoice;
        }

        public void setIsInvoice(int isInvoice) {
            this.isInvoice = isInvoice;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public int getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(String deletedAt) {
            this.deletedAt = deletedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public int getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(int categoryId) {
            this.categoryId = categoryId;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }
    }
}
