package com.tawangga.petstoopmvp.Model;

import com.google.gson.annotations.SerializedName;

public class SignInModel {

    /**
     * STATUS_CODE : 200
     * MESSAGE : Success
     * DATA : {"staff_id":2,"name":"kasir1","site_id":37,"username":"kasir1","email":"kasir1@gmail.com","phone":"081231312312","photo":"http://178.128.62.50/petv2/api/public/images/staff-1565345585.png","staff_category_id":2,"category_name":"Operator","photo_old":"staff-1565345585.png","token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOnsic3RhZmZfaWQiOjIsInN0YWZmX2NhdGVnb3J5Ijoib3BlcmF0b3IiLCJzaXRlX2lkIjozN30sImlhdCI6MTU2NzczODk4MiwiZXhwIjoxNTY3NzgyMTgyfQ.VfwQd4mDqqyKIK_5Kxs0r-rI5n2NsNmKbmIx-XVnwt8","staff_code":"S000002","site":{"user_id":37,"username":"client1site1","email":"client1site1@gmail.com","user_group_id":3,"photo":"user-1565343561.png","created_by":34,"updated_by":34,"created_at":"2019-08-09 09:39:21","updated_at":"2019-08-15 07:05:36","site_name":"Site 1","site_city":"Aceh","site_manager":"Lorem","remark":"-","site_address":"-"}}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * staff_id : 2
         * name : kasir1
         * site_id : 37
         * username : kasir1
         * email : kasir1@gmail.com
         * phone : 081231312312
         * photo : http://178.128.62.50/petv2/api/public/images/staff-1565345585.png
         * staff_category_id : 2
         * category_name : Operator
         * photo_old : staff-1565345585.png
         * token : eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOnsic3RhZmZfaWQiOjIsInN0YWZmX2NhdGVnb3J5Ijoib3BlcmF0b3IiLCJzaXRlX2lkIjozN30sImlhdCI6MTU2NzczODk4MiwiZXhwIjoxNTY3NzgyMTgyfQ.VfwQd4mDqqyKIK_5Kxs0r-rI5n2NsNmKbmIx-XVnwt8
         * staff_code : S000002
         * site : {"user_id":37,"username":"client1site1","email":"client1site1@gmail.com","user_group_id":3,"photo":"user-1565343561.png","created_by":34,"updated_by":34,"created_at":"2019-08-09 09:39:21","updated_at":"2019-08-15 07:05:36","site_name":"Site 1","site_city":"Aceh","site_manager":"Lorem","remark":"-","site_address":"-"}
         */

        @SerializedName("staff_id")
        private int staffId;
        @SerializedName("name")
        private String name;
        @SerializedName("site_id")
        private int siteId;
        @SerializedName("username")
        private String username;
        @SerializedName("email")
        private String email;
        @SerializedName("phone")
        private String phone;
        @SerializedName("photo")
        private String photo;
        @SerializedName("staff_category_id")
        private int staffCategoryId;
        @SerializedName("category_name")
        private String categoryName;
        @SerializedName("photo_old")
        private String photoOld;
        @SerializedName("token")
        private String token;
        @SerializedName("staff_code")
        private String staffCode;
        @SerializedName("site")
        private SiteBean site;

        public int getStaffId() {
            return staffId;
        }

        public void setStaffId(int staffId) {
            this.staffId = staffId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getSiteId() {
            return siteId;
        }

        public void setSiteId(int siteId) {
            this.siteId = siteId;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public int getStaffCategoryId() {
            return staffCategoryId;
        }

        public void setStaffCategoryId(int staffCategoryId) {
            this.staffCategoryId = staffCategoryId;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public String getPhotoOld() {
            return photoOld;
        }

        public void setPhotoOld(String photoOld) {
            this.photoOld = photoOld;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getStaffCode() {
            return staffCode;
        }

        public void setStaffCode(String staffCode) {
            this.staffCode = staffCode;
        }

        public SiteBean getSite() {
            return site;
        }

        public void setSite(SiteBean site) {
            this.site = site;
        }

        public static class SiteBean {
            /**
             * user_id : 37
             * username : client1site1
             * email : client1site1@gmail.com
             * user_group_id : 3
             * photo : user-1565343561.png
             * created_by : 34
             * updated_by : 34
             * created_at : 2019-08-09 09:39:21
             * updated_at : 2019-08-15 07:05:36
             * site_name : Site 1
             * site_city : Aceh
             * site_manager : Lorem
             * remark : -
             * site_address : -
             */

            @SerializedName("user_id")
            private int userId;
            @SerializedName("username")
            private String username;
            @SerializedName("email")
            private String email;
            @SerializedName("user_group_id")
            private int userGroupId;
            @SerializedName("photo")
            private String photo;
            @SerializedName("created_by")
            private int createdBy;
            @SerializedName("updated_by")
            private int updatedBy;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
            @SerializedName("site_name")
            private String siteName;
            @SerializedName("site_city")
            private String siteCity;
            @SerializedName("site_manager")
            private String siteManager;
            @SerializedName("remark")
            private String remark;
            @SerializedName("site_address")
            private String siteAddress;

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public int getUserGroupId() {
                return userGroupId;
            }

            public void setUserGroupId(int userGroupId) {
                this.userGroupId = userGroupId;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public String getSiteName() {
                return siteName;
            }

            public void setSiteName(String siteName) {
                this.siteName = siteName;
            }

            public String getSiteCity() {
                return siteCity;
            }

            public void setSiteCity(String siteCity) {
                this.siteCity = siteCity;
            }

            public String getSiteManager() {
                return siteManager;
            }

            public void setSiteManager(String siteManager) {
                this.siteManager = siteManager;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public String getSiteAddress() {
                return siteAddress;
            }

            public void setSiteAddress(String siteAddress) {
                this.siteAddress = siteAddress;
            }
        }
    }
}
