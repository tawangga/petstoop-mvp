package com.tawangga.petstoopmvp.Model;

import com.google.gson.annotations.SerializedName;

public class GetInvoice {

    /**
     * STATUS_CODE : 200
     * MESSAGE : SUCCESS ADD TO INVOICE
     * DATA : {"transaction_id":84,"transaction_date":"2019-04-08","transaction_invoice_code":"#X20190408-5","customer_id":5,"customer_name":"Lara A. Harding","transaction_dp":"0.00","total_transaction":"4657.00","payment_status":"1","created_by":1,"update_by":null,"created_at":"2019-04-08 07:06:47","updated_at":"2019-04-08 07:06:47","status":"1","orders":[{"transaction_item_id":80,"transaction_id":84,"transaction_item_code":"R#1553668613-27","customer_id":5,"customer_name":"Lara A. Harding","pet_id":32,"pet_name":"Kssk","created_by":1,"updated_by":0,"created_at":"2019-04-08 07:06:47","updated_at":"2019-04-08 07:06:47","status":"1","items":[{"transaction_item_detail_id":165,"transaction_item_id":80,"transaction_item_type":"2","item_id":96,"item_name":"Service Medical B","item_type":"1","item_price":"0.00","item_qty":0,"item_note":"","created_at":"2019-04-08 07:06:47","updated_at":"2019-04-08 07:06:47","status":"1"},{"transaction_item_detail_id":166,"transaction_item_id":80,"transaction_item_type":"2","item_id":98,"item_name":"Service B","item_type":"2","item_price":"0.00","item_qty":0,"item_note":"","created_at":"2019-04-08 07:06:47","updated_at":"2019-04-08 07:06:47","status":"1"}]},{"transaction_item_id":81,"transaction_id":84,"transaction_item_code":"K#13","customer_id":44,"customer_name":"Rudi","pet_id":0,"pet_name":"","created_by":1,"updated_by":0,"created_at":"2019-04-08 07:06:47","updated_at":"2019-04-08 07:06:47","status":"1","items":[{"transaction_item_detail_id":167,"transaction_item_id":81,"transaction_item_type":"1","item_id":25,"item_name":"asd","item_type":"","item_price":"213.00","item_qty":1,"item_note":"","created_at":"2019-04-08 07:06:47","updated_at":"2019-04-08 07:06:47","status":"1"},{"transaction_item_detail_id":168,"transaction_item_id":81,"transaction_item_type":"1","item_id":27,"item_name":"kitkat","item_type":"","item_price":"2222.00","item_qty":2,"item_note":"Huuuu","created_at":"2019-04-08 07:06:47","updated_at":"2019-04-08 07:06:47","status":"1"}]}]}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATAget_invoice DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATAget_invoice getDATA() {
        return DATA;
    }

    public void setDATA(DATAget_invoice DATA) {
        this.DATA = DATA;
    }


}
