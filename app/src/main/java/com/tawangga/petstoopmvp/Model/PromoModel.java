package com.tawangga.petstoopmvp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PromoModel {

    /**
     * STATUS_CODE : 200
     * MESSAGE : Success
     * DATA : {"page":1,"last_page":1,"total_item":3,"limit":15,"items":[{"promo_id":4,"promo_name":"Happy Hours 5%","promo_desc":"-","promo_type":"1","promo_amount":"5.00","promo_disc_max":"15000.00","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-20 07:30:27","updated_at":"2019-05-20 07:45:25"},{"promo_id":5,"promo_name":"Weekend Deals 10%","promo_desc":"-","promo_type":"1","promo_amount":"10.00","promo_disc_max":"25000.00","created_by":4,"updated_by":0,"deleted_at":null,"created_at":"2019-05-20 07:31:47","updated_at":"2019-05-20 07:31:47"},{"promo_id":6,"promo_name":"Friyeay","promo_desc":"-","promo_type":"2","promo_amount":"15000.00","promo_disc_max":"0.00","created_by":4,"updated_by":0,"deleted_at":null,"created_at":"2019-05-22 20:48:12","updated_at":"2019-05-22 20:48:12"}]}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * page : 1
         * last_page : 1
         * total_item : 3
         * limit : 15
         * items : [{"promo_id":4,"promo_name":"Happy Hours 5%","promo_desc":"-","promo_type":"1","promo_amount":"5.00","promo_disc_max":"15000.00","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-20 07:30:27","updated_at":"2019-05-20 07:45:25"},{"promo_id":5,"promo_name":"Weekend Deals 10%","promo_desc":"-","promo_type":"1","promo_amount":"10.00","promo_disc_max":"25000.00","created_by":4,"updated_by":0,"deleted_at":null,"created_at":"2019-05-20 07:31:47","updated_at":"2019-05-20 07:31:47"},{"promo_id":6,"promo_name":"Friyeay","promo_desc":"-","promo_type":"2","promo_amount":"15000.00","promo_disc_max":"0.00","created_by":4,"updated_by":0,"deleted_at":null,"created_at":"2019-05-22 20:48:12","updated_at":"2019-05-22 20:48:12"}]
         */

        @SerializedName("page")
        private int page;
        @SerializedName("last_page")
        private int lastPage;
        @SerializedName("total_item")
        private int totalItem;
        @SerializedName("limit")
        private int limit;
        @SerializedName("items")
        private List<ItemsBean> items;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public int getTotalItem() {
            return totalItem;
        }

        public void setTotalItem(int totalItem) {
            this.totalItem = totalItem;
        }

        public int getLimit() {
            return limit;
        }

        public void setLimit(int limit) {
            this.limit = limit;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class ItemsBean {
            /**
             * promo_id : 4
             * promo_name : Happy Hours 5%
             * promo_desc : -
             * promo_type : 1
             * promo_amount : 5.00
             * promo_disc_max : 15000.00
             * created_by : 4
             * updated_by : 4
             * deleted_at : null
             * created_at : 2019-05-20 07:30:27
             * updated_at : 2019-05-20 07:45:25
             */

            @SerializedName("promo_id")
            private int promoId;
            @SerializedName("promo_name")
            private String promoName;
            @SerializedName("promo_desc")
            private String promoDesc;
            @SerializedName("promo_type")
            private String promoType;
            @SerializedName("promo_amount")
            private String promoAmount;
            @SerializedName("promo_disc_max")
            private String promoDiscMax;
            @SerializedName("created_by")
            private int createdBy;
            @SerializedName("updated_by")
            private int updatedBy;
            @SerializedName("deleted_at")
            private String deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;

            public int getPromoId() {
                return promoId;
            }

            public void setPromoId(int promoId) {
                this.promoId = promoId;
            }

            public String getPromoName() {
                return promoName;
            }

            public void setPromoName(String promoName) {
                this.promoName = promoName;
            }

            public String getPromoDesc() {
                return promoDesc;
            }

            public void setPromoDesc(String promoDesc) {
                this.promoDesc = promoDesc;
            }

            public String getPromoType() {
                return promoType;
            }

            public void setPromoType(String promoType) {
                this.promoType = promoType;
            }

            public String getPromoAmount() {
                return promoAmount;
            }

            public void setPromoAmount(String promoAmount) {
                this.promoAmount = promoAmount;
            }

            public String getPromoDiscMax() {
                return promoDiscMax;
            }

            public void setPromoDiscMax(String promoDiscMax) {
                this.promoDiscMax = promoDiscMax;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(String deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }
        }
    }
}
