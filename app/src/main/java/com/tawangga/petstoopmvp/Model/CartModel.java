package com.tawangga.petstoopmvp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CartModel {


    /**
     * STATUS_CODE : 200
     * MESSAGE : SUCCESS, Exist data
     * DATA : {"cart_product_id":1,"order_code":"S#1558941606-2","customer_id":2,"customer_name":"Ujicoba","is_invoice":0,"created_by":3,"updated_by":0,"deleted_at":null,"created_at":"2019-05-27 14:20:06","updated_at":"2019-05-27 14:20:06","items":[{"cart_product_detail_id":5,"cart_product_id":1,"product_id":1,"product_name":"Obat Kutu","product_price":"20000.00","product_qty":10,"product_note":"Jangan lupa buble wraps","discount_id":4,"discount_name":"Happy Hours 5%","doctor_id":0,"doctor_name":"","is_invoice":0,"created_by":3,"updated_by":3,"deleted_at":null,"created_at":"2019-05-27 08:02:17","updated_at":"2019-05-27 08:35:58","product_image":"http://192.168.1.82/images/product-1558509359.png","product_desc":"-","total":200000,"discount_amount":10000,"discount":{"promo_id":4,"promo_name":"Happy Hours 5%","promo_desc":"-","promo_type":"1","promo_amount":"5.00","promo_disc_max":"15000.00","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-20 07:30:27","updated_at":"2019-05-20 07:45:25"},"grand_total":190000},{"cart_product_detail_id":6,"cart_product_id":1,"product_id":2,"product_name":"Obat Kutu","product_price":"10000.00","product_qty":3,"product_note":"Jangan lupa buble wraps","discount_id":4,"discount_name":"Happy Hours 5%","doctor_id":0,"doctor_name":"","is_invoice":0,"created_by":3,"updated_by":0,"deleted_at":null,"created_at":"2019-05-27 08:58:19","updated_at":"2019-05-27 08:58:19","product_image":"http://192.168.1.82/images/user-1558328645.png","product_desc":"-","total":30000,"discount_amount":1500,"discount":{"promo_id":4,"promo_name":"Happy Hours 5%","promo_desc":"-","promo_type":"1","promo_amount":"5.00","promo_disc_max":"15000.00","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-20 07:30:27","updated_at":"2019-05-20 07:45:25"},"grand_total":28500}],"grand_total":218500}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * cart_product_id : 1
         * order_code : S#1558941606-2
         * customer_id : 2
         * customer_name : Ujicoba
         * is_invoice : 0
         * created_by : 3
         * updated_by : 0
         * deleted_at : null
         * created_at : 2019-05-27 14:20:06
         * updated_at : 2019-05-27 14:20:06
         * items : [{"cart_product_detail_id":5,"cart_product_id":1,"product_id":1,"product_name":"Obat Kutu","product_price":"20000.00","product_qty":10,"product_note":"Jangan lupa buble wraps","discount_id":4,"discount_name":"Happy Hours 5%","doctor_id":0,"doctor_name":"","is_invoice":0,"created_by":3,"updated_by":3,"deleted_at":null,"created_at":"2019-05-27 08:02:17","updated_at":"2019-05-27 08:35:58","product_image":"http://192.168.1.82/images/product-1558509359.png","product_desc":"-","total":200000,"discount_amount":10000,"discount":{"promo_id":4,"promo_name":"Happy Hours 5%","promo_desc":"-","promo_type":"1","promo_amount":"5.00","promo_disc_max":"15000.00","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-20 07:30:27","updated_at":"2019-05-20 07:45:25"},"grand_total":190000},{"cart_product_detail_id":6,"cart_product_id":1,"product_id":2,"product_name":"Obat Kutu","product_price":"10000.00","product_qty":3,"product_note":"Jangan lupa buble wraps","discount_id":4,"discount_name":"Happy Hours 5%","doctor_id":0,"doctor_name":"","is_invoice":0,"created_by":3,"updated_by":0,"deleted_at":null,"created_at":"2019-05-27 08:58:19","updated_at":"2019-05-27 08:58:19","product_image":"http://192.168.1.82/images/user-1558328645.png","product_desc":"-","total":30000,"discount_amount":1500,"discount":{"promo_id":4,"promo_name":"Happy Hours 5%","promo_desc":"-","promo_type":"1","promo_amount":"5.00","promo_disc_max":"15000.00","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-20 07:30:27","updated_at":"2019-05-20 07:45:25"},"grand_total":28500}]
         * grand_total : 218500
         */

        @SerializedName("cart_product_id")
        private int cartProductId;
        @SerializedName("order_code")
        private String orderCode;
        @SerializedName("customer_id")
        private int customerId;
        @SerializedName("cart_service_id")
        private int cartServiceId;
        @SerializedName("customer_name")
        private String customerName;
        @SerializedName("is_invoice")
        private int isInvoice;
        @SerializedName("created_by")
        private int createdBy;
        @SerializedName("updated_by")
        private int updatedBy;
        @SerializedName("deleted_at")
        private String deletedAt;
        @SerializedName("created_at")
        private String createdAt;
        @SerializedName("updated_at")
        private String updatedAt;
        @SerializedName("grand_total")
        private int grandTotal;
        @SerializedName("items")
//        @SerializedName("medical_items")
        private List<DATAget_detailCart> items;

        public int getCartProductId() {
            return cartProductId;
        }

        public void setCartProductId(int cartProductId) {
            this.cartProductId = cartProductId;
        }

        public String getOrderCode() {
            return orderCode;
        }

        public void setOrderCode(String orderCode) {
            this.orderCode = orderCode;
        }

        public int getCustomerId() {
            return customerId;
        }

        public void setCustomerId(int customerId) {
            this.customerId = customerId;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public int getIsInvoice() {
            return isInvoice;
        }

        public void setIsInvoice(int isInvoice) {
            this.isInvoice = isInvoice;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public int getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(String deletedAt) {
            this.deletedAt = deletedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public int getGrandTotal() {
            return grandTotal;
        }

        public void setGrandTotal(int grandTotal) {
            this.grandTotal = grandTotal;
        }

        public List<DATAget_detailCart> getItems() {
            return items;
        }

        public void setItems(List<DATAget_detailCart> items) {
            this.items = items;
        }

        public int getCartServiceId() {
            return cartServiceId;
        }

        public void setCartServiceId(int cartServiceId) {
            this.cartServiceId = cartServiceId;
        }
    }
}



