package com.tawangga.petstoopmvp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetExamModel {

    /**
     * STATUS_CODE : 200
     * MESSAGE : Success
     * DATA : {"page":1,"last_page":1,"total_item":2,"limit":"10","items":[{"exam_id":1,"exam_name":"Exam 1","exam_code":"exam1","exam_price":"35000.00","exam_desc":"-","exam_unit":"Unit","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-24 10:07:53","updated_at":"2019-05-24 10:09:39"},{"exam_id":2,"exam_name":"Exam 2","exam_code":"exam2","exam_price":"45000.00","exam_desc":"-","exam_unit":"Unit","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-24 10:08:05","updated_at":"2019-05-24 10:09:45"}]}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * page : 1
         * last_page : 1
         * total_item : 2
         * limit : 10
         * items : [{"exam_id":1,"exam_name":"Exam 1","exam_code":"exam1","exam_price":"35000.00","exam_desc":"-","exam_unit":"Unit","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-24 10:07:53","updated_at":"2019-05-24 10:09:39"},{"exam_id":2,"exam_name":"Exam 2","exam_code":"exam2","exam_price":"45000.00","exam_desc":"-","exam_unit":"Unit","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-24 10:08:05","updated_at":"2019-05-24 10:09:45"}]
         */

        @SerializedName("page")
        private int page;
        @SerializedName("last_page")
        private int lastPage;
        @SerializedName("total_item")
        private int totalItem;
        @SerializedName("limit")
        private String limit;
        @SerializedName("items")
        private List<ItemsBean> items;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public int getTotalItem() {
            return totalItem;
        }

        public void setTotalItem(int totalItem) {
            this.totalItem = totalItem;
        }

        public String getLimit() {
            return limit;
        }

        public void setLimit(String limit) {
            this.limit = limit;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }



        public static class ItemsBean {
            /**
             * exam_id : 1
             * exam_name : Exam 1
             * exam_code : exam1
             * exam_price : 35000.00
             * exam_desc : -
             * exam_unit : Unit
             * created_by : 4
             * updated_by : 4
             * deleted_at : null
             * created_at : 2019-05-24 10:07:53
             * updated_at : 2019-05-24 10:09:39
             */

            @SerializedName("exam_id")
            private int examId;
            @SerializedName("exam_name")
            private String examName;
            @SerializedName("exam_code")
            private String examCode;
            @SerializedName("exam_price")
            private String examPrice;
            @SerializedName("exam_desc")
            private String examDesc;
            @SerializedName("exam_unit")
            private String examUnit;
            @SerializedName("created_by")
            private int createdBy;
            @SerializedName("updated_by")
            private int updatedBy;
            @SerializedName("deleted_at")
            private Object deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
            private boolean isAdded = false;

            public int getExamId() {
                return examId;
            }

            public void setExamId(int examId) {
                this.examId = examId;
            }

            public String getExamName() {
                return examName;
            }

            public void setExamName(String examName) {
                this.examName = examName;
            }

            public String getExamCode() {
                return examCode;
            }

            public void setExamCode(String examCode) {
                this.examCode = examCode;
            }

            public String getExamPrice() {
                return examPrice;
            }

            public void setExamPrice(String examPrice) {
                this.examPrice = examPrice;
            }

            public String getExamDesc() {
                return examDesc;
            }

            public void setExamDesc(String examDesc) {
                this.examDesc = examDesc;
            }

            public String getExamUnit() {
                return examUnit;
            }

            public void setExamUnit(String examUnit) {
                this.examUnit = examUnit;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public Object getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(Object deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public boolean isAdded() {
                return isAdded;
            }

            public void setAdded(boolean added) {
                isAdded = added;
            }
        }
    }
}
