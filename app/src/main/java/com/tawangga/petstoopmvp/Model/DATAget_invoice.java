package com.tawangga.petstoopmvp.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DATAget_invoice implements Parcelable {
    /**
     * transaction_id : 84
     * transaction_date : 2019-04-08
     * transaction_invoice_code : #X20190408-5
     * customer_id : 5
     * customer_name : Lara A. Harding
     * transaction_dp : 0.00
     * total_transaction : 4657.00
     * payment_status : 1
     * created_by : 1
     * update_by : null
     * created_at : 2019-04-08 07:06:47
     * updated_at : 2019-04-08 07:06:47
     * status : 1
     * orders : [{"transaction_item_id":80,"transaction_id":84,"transaction_item_code":"R#1553668613-27","customer_id":5,"customer_name":"Lara A. Harding","pet_id":32,"pet_name":"Kssk","created_by":1,"updated_by":0,"created_at":"2019-04-08 07:06:47","updated_at":"2019-04-08 07:06:47","status":"1","items":[{"transaction_item_detail_id":165,"transaction_item_id":80,"transaction_item_type":"2","item_id":96,"item_name":"Service Medical B","item_type":"1","item_price":"0.00","item_qty":0,"item_note":"","created_at":"2019-04-08 07:06:47","updated_at":"2019-04-08 07:06:47","status":"1"},{"transaction_item_detail_id":166,"transaction_item_id":80,"transaction_item_type":"2","item_id":98,"item_name":"Service B","item_type":"2","item_price":"0.00","item_qty":0,"item_note":"","created_at":"2019-04-08 07:06:47","updated_at":"2019-04-08 07:06:47","status":"1"}]},{"transaction_item_id":81,"transaction_id":84,"transaction_item_code":"K#13","customer_id":44,"customer_name":"Rudi","pet_id":0,"pet_name":"","created_by":1,"updated_by":0,"created_at":"2019-04-08 07:06:47","updated_at":"2019-04-08 07:06:47","status":"1","items":[{"transaction_item_detail_id":167,"transaction_item_id":81,"transaction_item_type":"1","item_id":25,"item_name":"asd","item_type":"","item_price":"213.00","item_qty":1,"item_note":"","created_at":"2019-04-08 07:06:47","updated_at":"2019-04-08 07:06:47","status":"1"},{"transaction_item_detail_id":168,"transaction_item_id":81,"transaction_item_type":"1","item_id":27,"item_name":"kitkat","item_type":"","item_price":"2222.00","item_qty":2,"item_note":"Huuuu","created_at":"2019-04-08 07:06:47","updated_at":"2019-04-08 07:06:47","status":"1"}]}]
     */

    @SerializedName("transaction_id")
    private int transactionId;
    @SerializedName("transaction_date")
    private String transactionDate;
    @SerializedName("transaction_invoice_code")
    private String transactionInvoiceCode;
    @SerializedName("customer_id")
    private int customerId;
    @SerializedName("customer_name")
    private String customerName;
    @SerializedName("transaction_dp")
    private String transactionDp;
    @SerializedName("total_transaction")
    private String totalTransaction;
    @SerializedName("payment_status")
    private String paymentStatus;
    @SerializedName("created_by")
    private int createdBy;
    @SerializedName("update_by")
    private Object updateBy;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("status")
    private String status;

    public String getCount_product() {
        return count_product;
    }

    public void setCount_product(String count_product) {
        this.count_product = count_product;
    }

    public String getCount_treatment() {
        return count_treatment;
    }

    public void setCount_treatment(String count_treatment) {
        this.count_treatment = count_treatment;
    }

    @SerializedName("count_product")
    private String count_product;
    @SerializedName("count_treatment")
    private String count_treatment;
    @SerializedName("orders")
    private List<Ordersget_invoice> orders;

    protected DATAget_invoice(Parcel in) {
        transactionId = in.readInt();
        transactionDate = in.readString();
        transactionInvoiceCode = in.readString();
        customerId = in.readInt();
        customerName = in.readString();
        transactionDp = in.readString();
        totalTransaction = in.readString();
        paymentStatus = in.readString();
        createdBy = in.readInt();
        createdAt = in.readString();
        updatedAt = in.readString();
        status = in.readString();
        count_product = in.readString();
        count_treatment = in.readString();
    }

    public static final Creator<DATAget_invoice> CREATOR = new Creator<DATAget_invoice>() {
        @Override
        public DATAget_invoice createFromParcel(Parcel in) {
            return new DATAget_invoice(in);
        }

        @Override
        public DATAget_invoice[] newArray(int size) {
            return new DATAget_invoice[size];
        }
    };

    public int getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionInvoiceCode() {
        return transactionInvoiceCode;
    }

    public void setTransactionInvoiceCode(String transactionInvoiceCode) {
        this.transactionInvoiceCode = transactionInvoiceCode;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getTransactionDp() {
        return transactionDp;
    }

    public void setTransactionDp(String transactionDp) {
        this.transactionDp = transactionDp;
    }

    public String getTotalTransaction() {
        return totalTransaction;
    }

    public void setTotalTransaction(String totalTransaction) {
        this.totalTransaction = totalTransaction;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public Object getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Object updateBy) {
        this.updateBy = updateBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Ordersget_invoice> getOrders() {
        return orders;
    }

    public void setOrders(List<Ordersget_invoice> orders) {
        this.orders = orders;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(transactionId);
        parcel.writeString(transactionDate);
        parcel.writeString(transactionInvoiceCode);
        parcel.writeInt(customerId);
        parcel.writeString(customerName);
        parcel.writeString(transactionDp);
        parcel.writeString(totalTransaction);
        parcel.writeString(paymentStatus);
        parcel.writeInt(createdBy);
        parcel.writeString(createdAt);
        parcel.writeString(updatedAt);
        parcel.writeString(status);
        parcel.writeString(count_product);
        parcel.writeString(count_treatment);
    }

    public static class Ordersget_invoice {
        /**
         * transaction_item_id : 80
         * transaction_id : 84
         * transaction_item_code : R#1553668613-27
         * customer_id : 5
         * customer_name : Lara A. Harding
         * pet_id : 32
         * pet_name : Kssk
         * created_by : 1
         * updated_by : 0
         * created_at : 2019-04-08 07:06:47
         * updated_at : 2019-04-08 07:06:47
         * status : 1
         * items : [{"transaction_item_detail_id":165,"transaction_item_id":80,"transaction_item_type":"2","item_id":96,"item_name":"Service Medical B","item_type":"1","item_price":"0.00","item_qty":0,"item_note":"","created_at":"2019-04-08 07:06:47","updated_at":"2019-04-08 07:06:47","status":"1"},{"transaction_item_detail_id":166,"transaction_item_id":80,"transaction_item_type":"2","item_id":98,"item_name":"Service B","item_type":"2","item_price":"0.00","item_qty":0,"item_note":"","created_at":"2019-04-08 07:06:47","updated_at":"2019-04-08 07:06:47","status":"1"}]
         */

        @SerializedName("transaction_item_id")
        private int transactionItemId;
        @SerializedName("transaction_id")
        private int transactionId;
        @SerializedName("transaction_item_code")
        private String transactionItemCode;
        @SerializedName("customer_id")
        private int customerId;
        @SerializedName("customer_name")
        private String customerName;
        @SerializedName("pet_id")
        private int petId;
        @SerializedName("pet_name")
        private String petName;
        @SerializedName("created_by")
        private int createdBy;
        @SerializedName("updated_by")
        private int updatedBy;
        @SerializedName("created_at")
        private String createdAt;
        @SerializedName("updated_at")
        private String updatedAt;
        @SerializedName("status")
        private String status;
        @SerializedName("items")
        private List<Itemsget_invoice> items;

        public int getTransactionItemId() {
            return transactionItemId;
        }

        public void setTransactionItemId(int transactionItemId) {
            this.transactionItemId = transactionItemId;
        }

        public int getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(int transactionId) {
            this.transactionId = transactionId;
        }

        public String getTransactionItemCode() {
            return transactionItemCode;
        }

        public void setTransactionItemCode(String transactionItemCode) {
            this.transactionItemCode = transactionItemCode;
        }

        public int getCustomerId() {
            return customerId;
        }

        public void setCustomerId(int customerId) {
            this.customerId = customerId;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public int getPetId() {
            return petId;
        }

        public void setPetId(int petId) {
            this.petId = petId;
        }

        public String getPetName() {
            return petName;
        }

        public void setPetName(String petName) {
            this.petName = petName;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public int getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<Itemsget_invoice> getItems() {
            return items;
        }

        public void setItems(List<Itemsget_invoice> items) {
            this.items = items;
        }

        public static class Itemsget_invoice {
            /**
             * transaction_item_detail_id : 165
             * transaction_item_id : 80
             * transaction_item_type : 2
             * item_id : 96
             * item_name : Service Medical B
             * item_type : 1
             * item_price : 0.00
             * item_qty : 0
             * item_note :
             * created_at : 2019-04-08 07:06:47
             * updated_at : 2019-04-08 07:06:47
             * status : 1
             */

            @SerializedName("transaction_item_detail_id")
            private int transactionItemDetailId;
            @SerializedName("transaction_item_id")
            private int transactionItemId;
            @SerializedName("transaction_item_type")
            private String transactionItemType;
            @SerializedName("item_id")
            private int itemId;
            @SerializedName("item_name")
            private String itemName;
            @SerializedName("item_type")
            private String itemType;
            @SerializedName("item_price")
            private String itemPrice;
            @SerializedName("item_qty")
            private String itemQty;
            @SerializedName("item_note")
            private String itemNote;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
            @SerializedName("status")
            private String status;

            public int getTransactionItemDetailId() {
                return transactionItemDetailId;
            }

            public void setTransactionItemDetailId(int transactionItemDetailId) {
                this.transactionItemDetailId = transactionItemDetailId;
            }

            public int getTransactionItemId() {
                return transactionItemId;
            }

            public void setTransactionItemId(int transactionItemId) {
                this.transactionItemId = transactionItemId;
            }

            public String getTransactionItemType() {
                return transactionItemType;
            }

            public void setTransactionItemType(String transactionItemType) {
                this.transactionItemType = transactionItemType;
            }

            public int getItemId() {
                return itemId;
            }

            public void setItemId(int itemId) {
                this.itemId = itemId;
            }

            public String getItemName() {
                return itemName;
            }

            public void setItemName(String itemName) {
                this.itemName = itemName;
            }

            public String getItemType() {
                return itemType;
            }

            public void setItemType(String itemType) {
                this.itemType = itemType;
            }

            public String getItemPrice() {
                return itemPrice;
            }

            public void setItemPrice(String itemPrice) {
                this.itemPrice = itemPrice;
            }

            public String getItemQty() {
                return itemQty;
            }

            public void setItemQty(String itemQty) {
                this.itemQty = itemQty;
            }

            public String getItemNote() {
                return itemNote;
            }

            public void setItemNote(String itemNote) {
                this.itemNote = itemNote;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
        }
    }
}
