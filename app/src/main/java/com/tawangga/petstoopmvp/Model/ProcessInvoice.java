package com.tawangga.petstoopmvp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProcessInvoice {

    /**
     * STATUS_CODE : 200
     * MESSAGE : CUSTOMER ID
     * DATA : {"customers":[{"customer_id":1,"customer_name":"Edinofri Karizal Caniago","customer_photo":"customer-1557892983.png","deposit":false}],"invoice":{"transaction_id":6,"transaction_invoice_code":"#INV-1559196822-1","customer_id":1,"customer_name":"Edinofri Karizal Caniago","transaction_dp":"0.00","total_transaction":"735000.00","payment_status":"1","created_by":1,"updated_by":0,"deleted_at":null,"created_at":"2019-05-30 06:13:42","updated_at":"2019-05-30 06:13:42","orders":[{"transaction_item_id":9,"transaction_item_code":"S#1558950055-1","transaction_id":6,"customer_id":1,"customer_name":"Edinofri Karizal Caniago","pet_id":0,"pet_name":"","created_by":1,"updated_by":0,"deleted_at":null,"created_at":"2019-05-30 06:13:42","updated_at":"2019-05-30 06:13:42","items":[{"transaction_item_detail_id":11,"transaction_item_id":9,"transaction_item_type":"1","item_order_id":11,"item_id":3,"item_name":"Obat Flu Kucing","item_type":"","item_price":"35000.00","item_qty":21,"item_note":"","item_disc_id":0,"created_by":0,"updated_by":0,"deleted_at":null,"created_at":null,"updated_at":null}]}]}}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * customers : [{"customer_id":1,"customer_name":"Edinofri Karizal Caniago","customer_photo":"customer-1557892983.png","deposit":false}]
         * invoice : {"transaction_id":6,"transaction_invoice_code":"#INV-1559196822-1","customer_id":1,"customer_name":"Edinofri Karizal Caniago","transaction_dp":"0.00","total_transaction":"735000.00","payment_status":"1","created_by":1,"updated_by":0,"deleted_at":null,"created_at":"2019-05-30 06:13:42","updated_at":"2019-05-30 06:13:42","orders":[{"transaction_item_id":9,"transaction_item_code":"S#1558950055-1","transaction_id":6,"customer_id":1,"customer_name":"Edinofri Karizal Caniago","pet_id":0,"pet_name":"","created_by":1,"updated_by":0,"deleted_at":null,"created_at":"2019-05-30 06:13:42","updated_at":"2019-05-30 06:13:42","items":[{"transaction_item_detail_id":11,"transaction_item_id":9,"transaction_item_type":"1","item_order_id":11,"item_id":3,"item_name":"Obat Flu Kucing","item_type":"","item_price":"35000.00","item_qty":21,"item_note":"","item_disc_id":0,"created_by":0,"updated_by":0,"deleted_at":null,"created_at":null,"updated_at":null}]}]}
         */

        @SerializedName("invoice")
        private DATAget_invoice invoice;
        @SerializedName("customers")
        private List<CustomersBean> customers;

        public DATAget_invoice getInvoice() {
            return invoice;
        }

        public void setInvoice(DATAget_invoice invoice) {
            this.invoice = invoice;
        }

        public List<CustomersBean> getCustomers() {
            return customers;
        }

        public void setCustomers(List<CustomersBean> customers) {
            this.customers = customers;
        }


        public static class CustomersBean {
            /**
             * customer_id : 1
             * customer_name : Edinofri Karizal Caniago
             * customer_photo : customer-1557892983.png
             * deposit : false
             */

            @SerializedName("customer_id")
            private int customerId;
            @SerializedName("customer_name")
            private String customerName;
            @SerializedName("customer_photo")
            private String customerPhoto;
            @SerializedName("deposit")
            private boolean deposit;

            public int getCustomerId() {
                return customerId;
            }

            public void setCustomerId(int customerId) {
                this.customerId = customerId;
            }

            public String getCustomerName() {
                return customerName;
            }

            public void setCustomerName(String customerName) {
                this.customerName = customerName;
            }

            public String getCustomerPhoto() {
                return customerPhoto;
            }

            public void setCustomerPhoto(String customerPhoto) {
                this.customerPhoto = customerPhoto;
            }

            public boolean isDeposit() {
                return deposit;
            }

            public void setDeposit(boolean deposit) {
                this.deposit = deposit;
            }
        }
    }
}
