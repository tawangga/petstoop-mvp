package com.tawangga.petstoopmvp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GroomingLocationModel {

    /**
     * STATUS_CODE : 200
     * MESSAGE : SUCCESS
     * DATA : [{"location_id":2,"location_name":"Location B","location_desc":"location","location_duration":"08:30:00","location_available":"1","created_date":"2019-03-13 15:54:01","created_by":1,"update_date":"2019-03-20 18:02:42","update_by":12,"status":"1"},{"location_id":3,"location_name":"Location C","location_desc":"location","location_duration":"09:00:00","location_available":"1","created_date":"2019-03-13 15:54:13","created_by":1,"update_date":"2019-03-20 18:04:34","update_by":12,"status":"1"}]
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private List<DATABean> DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public List<DATABean> getDATA() {
        return DATA;
    }

    public void setDATA(List<DATABean> DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * location_id : 2
         * location_name : Location B
         * location_desc : location
         * location_duration : 08:30:00
         * location_available : 1
         * created_date : 2019-03-13 15:54:01
         * created_by : 1
         * update_date : 2019-03-20 18:02:42
         * update_by : 12
         * status : 1
         */

        @SerializedName("location_id")
        private int locationId;
        @SerializedName("location_name")
        private String locationName;
        @SerializedName("location_desc")
        private String locationDesc;
        @SerializedName("location_duration")
        private String locationDuration;
        @SerializedName("location_available")
        private String locationAvailable;
        @SerializedName("created_date")
        private String createdDate;
        @SerializedName("created_by")
        private int createdBy;
        @SerializedName("update_date")
        private String updateDate;
        @SerializedName("update_by")
        private int updateBy;
        @SerializedName("status")
        private String status;

        public int getLocationId() {
            return locationId;
        }

        public void setLocationId(int locationId) {
            this.locationId = locationId;
        }

        public String getLocationName() {
            return locationName;
        }

        public void setLocationName(String locationName) {
            this.locationName = locationName;
        }

        public String getLocationDesc() {
            return locationDesc;
        }

        public void setLocationDesc(String locationDesc) {
            this.locationDesc = locationDesc;
        }

        public String getLocationDuration() {
            return locationDuration;
        }

        public void setLocationDuration(String locationDuration) {
            this.locationDuration = locationDuration;
        }

        public String getLocationAvailable() {
            return locationAvailable;
        }

        public void setLocationAvailable(String locationAvailable) {
            this.locationAvailable = locationAvailable;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public String getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(String updateDate) {
            this.updateDate = updateDate;
        }

        public int getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(int updateBy) {
            this.updateBy = updateBy;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
