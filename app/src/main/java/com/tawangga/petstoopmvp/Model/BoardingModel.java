package com.tawangga.petstoopmvp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BoardingModel {
    /**
     * STATUS_CODE : 200
     * MESSAGE : SUCCESS
     * DATA : [{"cage_id":1,"cage_name":"Cage A","cage_size":"1","cage_desc":"cage","cage_available":"1","created_by":1,"created_date":"2019-03-14 00:04:45","update_by":1,"update_date":"2019-03-14 00:10:05","status":"1"},{"cage_id":2,"cage_name":"Cage B","cage_size":"2","cage_desc":"cage","cage_available":"1","created_by":1,"created_date":"2019-03-14 00:10:18","update_by":0,"update_date":"2019-03-14 00:10:18","status":"1"},{"cage_id":3,"cage_name":"Cage C","cage_size":"1","cage_desc":"cage","cage_available":"1","created_by":1,"created_date":"2019-03-14 00:10:30","update_by":0,"update_date":"2019-03-14 00:10:30","status":"1"},{"cage_id":4,"cage_name":"Cage D","cage_size":"3","cage_desc":"cage","cage_available":"1","created_by":1,"created_date":"2019-03-14 00:10:40","update_by":0,"update_date":"2019-03-14 00:10:40","status":"1"},{"cage_id":5,"cage_name":"cage E","cage_size":"3","cage_desc":"cage","cage_available":"1","created_by":1,"created_date":"2019-03-14 00:10:50","update_by":1,"update_date":"2019-03-14 00:10:58","status":"1"}]
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private List<DATABean> DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public List<DATABean> getDATA() {
        return DATA;
    }

    public void setDATA(List<DATABean> DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * cage_id : 1
         * cage_name : Cage A
         * cage_size : 1
         * cage_desc : cage
         * cage_available : 1
         * created_by : 1
         * created_date : 2019-03-14 00:04:45
         * update_by : 1
         * update_date : 2019-03-14 00:10:05
         * status : 1
         */

        @SerializedName("cage_id")
        private int cageId;
        @SerializedName("cage_name")
        private String cageName;
        @SerializedName("cage_size")
        private String cageSize;
        @SerializedName("cage_desc")
        private String cageDesc;
        @SerializedName("cage_available")
        private String cageAvailable;
        @SerializedName("created_by")
        private int createdBy;
        @SerializedName("created_date")
        private String createdDate;
        @SerializedName("update_by")
        private int updateBy;
        @SerializedName("update_date")
        private String updateDate;
        @SerializedName("status")
        private String status;

        public int getCageId() {
            return cageId;
        }

        public void setCageId(int cageId) {
            this.cageId = cageId;
        }

        public String getCageName() {
            return cageName;
        }

        public void setCageName(String cageName) {
            this.cageName = cageName;
        }

        public String getCageSize() {
            return cageSize;
        }

        public void setCageSize(String cageSize) {
            this.cageSize = cageSize;
        }

        public String getCageDesc() {
            return cageDesc;
        }

        public void setCageDesc(String cageDesc) {
            this.cageDesc = cageDesc;
        }

        public String getCageAvailable() {
            return cageAvailable;
        }

        public void setCageAvailable(String cageAvailable) {
            this.cageAvailable = cageAvailable;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public int getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(int updateBy) {
            this.updateBy = updateBy;
        }

        public String getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(String updateDate) {
            this.updateDate = updateDate;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
