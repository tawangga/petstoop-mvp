package com.tawangga.petstoopmvp.Model;

public class GetErrorMessage {

    @com.google.gson.annotations.SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @com.google.gson.annotations.SerializedName("MESSAGE")
    private String MESSAGE;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }
}
