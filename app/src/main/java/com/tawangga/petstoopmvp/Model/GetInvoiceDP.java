package com.tawangga.petstoopmvp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetInvoiceDP {
    /**
     * STATUS_CODE : 200
     * MESSAGE : SUCCESS
     * DATA : [{"transaction_id":220,"transaction_date":"2019-04-15","transaction_invoice_code":"#X20190415-50","customer_id":50,"customer_name":"Yaha","transaction_dp":"50000.00","total_transaction":"80002.00","payment_status":"2","created_by":7,"update_by":null,"created_at":"2019-04-15 04:12:22","updated_at":"2019-04-15 04:13:03","status":"1","orders":[{"transaction_item_id":254,"transaction_id":220,"transaction_item_code":"K#39","customer_id":50,"customer_name":"Yaha","pet_id":0,"pet_name":"","created_by":7,"updated_by":0,"created_at":"2019-04-15 04:12:22","updated_at":"2019-04-15 04:12:22","status":"1","items":[{"transaction_item_detail_id":480,"transaction_item_id":254,"transaction_item_type":"1","item_id":65,"item_name":"Product H","item_type":"","item_price":"35000.00","item_qty":1,"item_note":"","created_at":"2019-04-15 04:12:22","updated_at":"2019-04-15 04:12:22","status":"1"},{"transaction_item_detail_id":481,"transaction_item_id":254,"transaction_item_type":"1","item_id":66,"item_name":"Product I","item_type":"","item_price":"45000.00","item_qty":1,"item_note":"Gshs","created_at":"2019-04-15 04:12:22","updated_at":"2019-04-15 04:12:22","status":"1"}]},{"transaction_item_id":255,"transaction_id":220,"transaction_item_code":"K#1554835916-10","customer_id":1,"customer_name":"Jhon Smith","pet_id":0,"pet_name":"","created_by":7,"updated_by":0,"created_at":"2019-04-15 04:12:25","updated_at":"2019-04-15 04:12:25","status":"1","items":[{"transaction_item_detail_id":482,"transaction_item_id":255,"transaction_item_type":"1","item_id":135,"item_name":"asdas","item_type":"","item_price":"2.00","item_qty":1,"item_note":"","created_at":"2019-04-15 04:12:25","updated_at":"2019-04-15 04:12:25","status":"1"}]}]},{"transaction_id":225,"transaction_date":"2019-04-15","transaction_invoice_code":"#X20190415-1","customer_id":1,"customer_name":"Jhon Smith","transaction_dp":"40000.00","total_transaction":"52000.00","payment_status":"2","created_by":7,"update_by":null,"created_at":"2019-04-15 04:25:34","updated_at":"2019-04-15 04:29:19","status":"1","orders":[{"transaction_item_id":279,"transaction_id":225,"transaction_item_code":"K#1554835916-10","customer_id":1,"customer_name":"Jhon Smith","pet_id":0,"pet_name":"","created_by":7,"updated_by":0,"created_at":"2019-04-15 04:25:34","updated_at":"2019-04-15 04:25:34","status":"1","items":[{"transaction_item_detail_id":504,"transaction_item_id":279,"transaction_item_type":"1","item_id":136,"item_name":"Product A","item_type":"","item_price":"16000.00","item_qty":1,"item_note":"Coba","created_at":"2019-04-15 04:25:34","updated_at":"2019-04-15 04:25:34","status":"1"}]},{"transaction_item_id":280,"transaction_id":225,"transaction_item_code":"K#1554874216-10","customer_id":8,"customer_name":"Linda T. Murphy","pet_id":0,"pet_name":"","created_by":7,"updated_by":0,"created_at":"2019-04-15 04:25:40","updated_at":"2019-04-15 04:25:40","status":"1","items":[{"transaction_item_detail_id":505,"transaction_item_id":280,"transaction_item_type":"1","item_id":141,"item_name":"Product A","item_type":"","item_price":"20000.00","item_qty":1,"item_note":"","created_at":"2019-04-15 04:25:40","updated_at":"2019-04-15 04:25:40","status":"1"}]},{"transaction_item_id":281,"transaction_id":225,"transaction_item_code":"K#1554835916-10","customer_id":1,"customer_name":"Jhon Smith","pet_id":0,"pet_name":"","created_by":7,"updated_by":0,"created_at":"2019-04-15 04:26:01","updated_at":"2019-04-15 04:26:01","status":"1","items":[{"transaction_item_detail_id":506,"transaction_item_id":281,"transaction_item_type":"1","item_id":136,"item_name":"Product A","item_type":"","item_price":"16000.00","item_qty":1,"item_note":"Coba","created_at":"2019-04-15 04:26:01","updated_at":"2019-04-15 04:26:01","status":"1"}]}]},{"transaction_id":261,"transaction_date":"2019-04-15","transaction_invoice_code":"#X20190415-47","customer_id":47,"customer_name":"Darmawan","transaction_dp":"100000.00","total_transaction":"105000.00","payment_status":"2","created_by":8,"update_by":null,"created_at":"2019-04-15 09:06:02","updated_at":"2019-04-15 09:06:32","status":"1","orders":[{"transaction_item_id":334,"transaction_id":261,"transaction_item_code":"K#42","customer_id":47,"customer_name":"Darmawan","pet_id":0,"pet_name":"","created_by":8,"updated_by":0,"created_at":"2019-04-15 09:06:02","updated_at":"2019-04-15 09:06:02","status":"1","items":[{"transaction_item_detail_id":608,"transaction_item_id":334,"transaction_item_type":"1","item_id":153,"item_name":"Product A","item_type":"","item_price":"20000.00","item_qty":1,"item_note":"","created_at":"2019-04-15 09:06:02","updated_at":"2019-04-15 09:06:02","status":"1"},{"transaction_item_detail_id":609,"transaction_item_id":334,"transaction_item_type":"1","item_id":154,"item_name":"Product E","item_type":"","item_price":"50000.00","item_qty":1,"item_note":"","created_at":"2019-04-15 09:06:02","updated_at":"2019-04-15 09:06:02","status":"1"}]},{"transaction_item_id":335,"transaction_id":261,"transaction_item_code":"K#1554790525-09","customer_id":5,"customer_name":"Lara A. Harding","pet_id":0,"pet_name":"","created_by":8,"updated_by":0,"created_at":"2019-04-15 09:06:02","updated_at":"2019-04-15 09:06:02","status":"1","items":[{"transaction_item_detail_id":610,"transaction_item_id":335,"transaction_item_type":"1","item_id":138,"item_name":"Product H","item_type":"","item_price":"35000.00","item_qty":1,"item_note":"","created_at":"2019-04-15 09:06:02","updated_at":"2019-04-15 09:06:02","status":"1"}]}]}]
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private List<DATAget_invoice> DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public List<DATAget_invoice> getDATA() {
        return DATA;
    }

    public void setDATA(List<DATAget_invoice> DATA) {
        this.DATA = DATA;
    }

}
