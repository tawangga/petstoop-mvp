package com.tawangga.petstoopmvp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomerModel {


    /**
     * STATUS_CODE : 200
     * MESSAGE : Success
     * DATA : {"page":1,"last_page":1,"total_item":10,"limit":15,"items":[{"customer_id":1,"customer_name":"Edinofri Karizal Caniago","customer_email":"tukangbasic@gmail.com","customer_phone":"082368338355","customer_address":"Jln Medan- Banda Aceh","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557892983.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 10:15:35","updated_at":"2019-05-15 11:03:04"},{"customer_id":2,"customer_name":"Jhon Smith","customer_email":"jhon.smith@mail.com","customer_phone":"0812345868","customer_address":"Jln Mawar","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557892489.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 10:54:49","updated_at":"2019-05-15 10:54:49"},{"customer_id":3,"customer_name":"Henry S. Browne","customer_email":"henry@gmail.com","customer_phone":"9205958973","customer_address":"","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557892538.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 10:55:38","updated_at":"2019-05-15 10:55:38"},{"customer_id":4,"customer_name":"Lara A. Harding","customer_email":"harding@gmail.com","customer_phone":"662912710","customer_address":"test","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557892589.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 10:56:29","updated_at":"2019-05-16 01:42:17"},{"customer_id":5,"customer_name":"Willie M. Reed","customer_email":"william@gmail.com","customer_phone":"650-921-9539","customer_address":"","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557892626.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 10:57:06","updated_at":"2019-05-15 10:57:06"},{"customer_id":6,"customer_name":"Debbi L. Cheeks","customer_email":"debi@gmail.com","customer_phone":"972-514-1155","customer_address":"","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557892694.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 10:58:14","updated_at":"2019-05-15 10:58:14"},{"customer_id":7,"customer_name":"Linda T. Murphy","customer_email":"lindamur@gmail.com","customer_phone":"270-908-1246","customer_address":"","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557892845.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 11:00:45","updated_at":"2019-05-15 11:00:45"},{"customer_id":8,"customer_name":"Randy H. Ford","customer_email":"ran@gmail.com","customer_phone":"805-576-3881","customer_address":"","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557892941.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 11:02:21","updated_at":"2019-05-15 11:02:21"},{"customer_id":9,"customer_name":"Paula P. Walczak","customer_email":"walczak@gmail.com","customer_phone":"252-441-0108","customer_address":"","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557893179.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 11:06:19","updated_at":"2019-05-15 11:06:19"},{"customer_id":12,"customer_name":"Saikoli","customer_email":"saikoli@mail.com","customer_phone":"23988888","customer_address":"Lorem banana","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557945770.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-16 01:42:50","updated_at":"2019-05-16 01:42:50"}]}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * page : 1
         * last_page : 1
         * total_item : 10
         * limit : 15
         * items : [{"customer_id":1,"customer_name":"Edinofri Karizal Caniago","customer_email":"tukangbasic@gmail.com","customer_phone":"082368338355","customer_address":"Jln Medan- Banda Aceh","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557892983.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 10:15:35","updated_at":"2019-05-15 11:03:04"},{"customer_id":2,"customer_name":"Jhon Smith","customer_email":"jhon.smith@mail.com","customer_phone":"0812345868","customer_address":"Jln Mawar","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557892489.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 10:54:49","updated_at":"2019-05-15 10:54:49"},{"customer_id":3,"customer_name":"Henry S. Browne","customer_email":"henry@gmail.com","customer_phone":"9205958973","customer_address":"","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557892538.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 10:55:38","updated_at":"2019-05-15 10:55:38"},{"customer_id":4,"customer_name":"Lara A. Harding","customer_email":"harding@gmail.com","customer_phone":"662912710","customer_address":"test","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557892589.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 10:56:29","updated_at":"2019-05-16 01:42:17"},{"customer_id":5,"customer_name":"Willie M. Reed","customer_email":"william@gmail.com","customer_phone":"650-921-9539","customer_address":"","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557892626.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 10:57:06","updated_at":"2019-05-15 10:57:06"},{"customer_id":6,"customer_name":"Debbi L. Cheeks","customer_email":"debi@gmail.com","customer_phone":"972-514-1155","customer_address":"","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557892694.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 10:58:14","updated_at":"2019-05-15 10:58:14"},{"customer_id":7,"customer_name":"Linda T. Murphy","customer_email":"lindamur@gmail.com","customer_phone":"270-908-1246","customer_address":"","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557892845.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 11:00:45","updated_at":"2019-05-15 11:00:45"},{"customer_id":8,"customer_name":"Randy H. Ford","customer_email":"ran@gmail.com","customer_phone":"805-576-3881","customer_address":"","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557892941.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 11:02:21","updated_at":"2019-05-15 11:02:21"},{"customer_id":9,"customer_name":"Paula P. Walczak","customer_email":"walczak@gmail.com","customer_phone":"252-441-0108","customer_address":"","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557893179.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 11:06:19","updated_at":"2019-05-15 11:06:19"},{"customer_id":12,"customer_name":"Saikoli","customer_email":"saikoli@mail.com","customer_phone":"23988888","customer_address":"Lorem banana","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557945770.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-16 01:42:50","updated_at":"2019-05-16 01:42:50"}]
         */

        @SerializedName("page")
        private String page;
        @SerializedName("last_page")
        private String lastPage;
        @SerializedName("total_item")
        private String totalItem;
        @SerializedName("limit")
        private String limit;
        @SerializedName("items")
        private List<ItemsBean> items;

        public String getPage() {
            return page;
        }

        public void setPage(String page) {
            this.page = page;
        }

        public String getLastPage() {
            return lastPage;
        }

        public void setLastPage(String lastPage) {
            this.lastPage = lastPage;
        }

        public String getTotalItem() {
            return totalItem;
        }

        public void setTotalItem(String totalItem) {
            this.totalItem = totalItem;
        }

        public String getLimit() {
            return limit;
        }

        public void setLimit(String limit) {
            this.limit = limit;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class ItemsBean {
            /**
             * customer_id : 1
             * customer_name : Edinofri Karizal Caniago
             * customer_email : tukangbasic@gmail.com
             * customer_phone : 082368338355
             * customer_address : Jln Medan- Banda Aceh
             * customer_photo : http://178.128.62.50/petv2/api/public/images/customer-1557892983.png
             * created_on : 1
             * updated_on : 1
             * created_by : 4
             * updated_by : 4
             * deleted_at : null
             * created_at : 2019-05-15 10:15:35
             * updated_at : 2019-05-15 11:03:04
             */

            @SerializedName("customer_id")
            private String customerId;
            @SerializedName("customer_name")
            private String customerName;
            @SerializedName("customer_email")
            private String customerEmail;
            @SerializedName("customer_phone")
            private String customerPhone;
            @SerializedName("customer_address")
            private String customerAddress;
            @SerializedName("customer_photo")
            private String customerPhoto;
            @SerializedName("created_on")
            private String createdOn;
            @SerializedName("updated_on")
            private String updatedOn;
            @SerializedName("created_by")
            private String createdBy;
            @SerializedName("updated_by")
            private String updatedBy;
            @SerializedName("deleted_at")
            private String deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
            @SerializedName("customer_code")
            private String customerCode;

            public String getCustomerId() {
                return customerId;
            }

            public void setCustomerId(String customerId) {
                this.customerId = customerId;
            }

            public String getCustomerName() {
                return customerName;
            }

            public void setCustomerName(String customerName) {
                this.customerName = customerName;
            }

            public String getCustomerEmail() {
                return customerEmail;
            }

            public void setCustomerEmail(String customerEmail) {
                this.customerEmail = customerEmail;
            }

            public String getCustomerPhone() {
                return customerPhone;
            }

            public void setCustomerPhone(String customerPhone) {
                this.customerPhone = customerPhone;
            }

            public String getCustomerAddress() {
                return customerAddress;
            }

            public void setCustomerAddress(String customerAddress) {
                this.customerAddress = customerAddress;
            }

            public String getCustomerPhoto() {
                return customerPhoto;
            }

            public void setCustomerPhoto(String customerPhoto) {
                this.customerPhoto = customerPhoto;
            }

            public String getCreatedOn() {
                return createdOn;
            }

            public void setCreatedOn(String createdOn) {
                this.createdOn = createdOn;
            }

            public String getUpdatedOn() {
                return updatedOn;
            }

            public void setUpdatedOn(String updatedOn) {
                this.updatedOn = updatedOn;
            }

            public String getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(String createdBy) {
                this.createdBy = createdBy;
            }

            public String getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(String updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(String deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public String getCustomerCode() {
                return customerCode;
            }

            public void setCustomerCode(String customerCode) {
                this.customerCode = customerCode;
            }
        }
    }
}
