package com.tawangga.petstoopmvp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetOrderConfirmAnamnesis {

    /**
     * STATUS_CODE : 200
     * MESSAGE : SUCCESS
     * DATA : {"cart_service_anamnesis_id":4,"cart_service_id":1,"anamnesis":"","diagnosis":"","return_schedule_desc":"","return_schedule_date":"","created_by":0,"updated_by":0,"deleted_at":"","created_at":"2019-05-28 06:13:23","updated_at":"2019-05-28 06:13:23","treatments":[{"cart_service_detail_id":1,"cart_service_id":1,"service_type":"1","service_id":1,"service_name":"Konsultasi Dokter","service_price":"165000.00","order_status":"1","is_invoice":0,"created_by":1,"updated_by":0,"deleted_at":"","created_at":"2019-05-28 04:55:21","updated_at":"2019-05-28 04:55:21"},{"cart_service_detail_id":2,"cart_service_id":1,"service_type":"2","service_id":1,"service_name":"SPA Kucing biasa","service_price":"100000.00","order_status":"1","is_invoice":0,"created_by":1,"updated_by":0,"deleted_at":"","created_at":"2019-05-28 04:55:31","updated_at":"2019-05-28 04:55:31"},{"cart_service_detail_id":5,"cart_service_id":1,"service_type":"3","service_id":1,"service_name":"Kandang Kucing S","service_price":"0.00","order_status":"1","is_invoice":0,"created_by":1,"updated_by":0,"deleted_at":"","created_at":"2019-05-28 05:22:12","updated_at":"2019-05-28 05:22:12"}],"clinical_checkups":[{"clinical_checkup_id":1,"clinical_checkup_name":"Berat Badan","clinical_checkup_unit":"Kg","clinical_checkup_desc":"-","clinical_checkup_is_priority":1,"created_by":4,"updated_by":0,"deleted_at":"","created_at":"2019-05-24 09:53:22","updated_at":"2019-05-24 09:53:22","value":0},{"clinical_checkup_id":2,"clinical_checkup_name":"Temperatur","clinical_checkup_unit":"C","clinical_checkup_desc":"-","clinical_checkup_is_priority":1,"created_by":4,"updated_by":0,"deleted_at":"","created_at":"2019-05-24 09:53:43","updated_at":"2019-05-24 09:53:43","value":0},{"clinical_checkup_id":3,"clinical_checkup_name":"Status Gizi","clinical_checkup_unit":"AKG","clinical_checkup_desc":"-","clinical_checkup_is_priority":0,"created_by":4,"updated_by":0,"deleted_at":"","created_at":"2019-05-24 09:54:55","updated_at":"2019-05-24 09:54:55","value":0},{"clinical_checkup_id":4,"clinical_checkup_name":"Kulit dan Telinga","clinical_checkup_unit":"Unit","clinical_checkup_desc":"-","clinical_checkup_is_priority":0,"created_by":4,"updated_by":0,"deleted_at":"","created_at":"2019-05-24 09:55:19","updated_at":"2019-05-24 09:55:19","value":0},{"clinical_checkup_id":5,"clinical_checkup_name":"Gigi dan Gusi","clinical_checkup_unit":"Unit","clinical_checkup_desc":"-","clinical_checkup_is_priority":0,"created_by":4,"updated_by":0,"deleted_at":"","created_at":"2019-05-24 09:55:53","updated_at":"2019-05-24 09:55:53","value":0},{"clinical_checkup_id":6,"clinical_checkup_name":"Selaput lendir, mata, mulut","clinical_checkup_unit":"Unit","clinical_checkup_desc":"-","clinical_checkup_is_priority":0,"created_by":4,"updated_by":4,"deleted_at":"","created_at":"2019-05-24 09:56:30","updated_at":"2019-05-24 10:00:08","value":0},{"clinical_checkup_id":7,"clinical_checkup_name":"Auskultasi Paru-paru","clinical_checkup_unit":"Unit","clinical_checkup_desc":"-","clinical_checkup_is_priority":0,"created_by":4,"updated_by":0,"deleted_at":"","created_at":"2019-05-24 09:57:06","updated_at":"2019-05-24 09:57:06","value":0},{"clinical_checkup_id":8,"clinical_checkup_name":"Auskultasi Jantung","clinical_checkup_unit":"Unit","clinical_checkup_desc":"-","clinical_checkup_is_priority":0,"created_by":4,"updated_by":0,"deleted_at":"","created_at":"2019-05-24 09:57:22","updated_at":"2019-05-24 09:57:22","value":0},{"clinical_checkup_id":9,"clinical_checkup_name":"Suara Pencernaan","clinical_checkup_unit":"Unit","clinical_checkup_desc":"-","clinical_checkup_is_priority":0,"created_by":4,"updated_by":0,"deleted_at":"","created_at":"2019-05-24 09:57:50","updated_at":"2019-05-24 09:57:50","value":0},{"clinical_checkup_id":10,"clinical_checkup_name":"Frekuensi Nafas","clinical_checkup_unit":"Unit","clinical_checkup_desc":"-","clinical_checkup_is_priority":0,"created_by":4,"updated_by":4,"deleted_at":"","created_at":"2019-05-24 09:58:08","updated_at":"2019-05-24 10:00:21","value":0},{"clinical_checkup_id":11,"clinical_checkup_name":"Frekuensi Detak Jantung","clinical_checkup_unit":"Unit","clinical_checkup_desc":"-","clinical_checkup_is_priority":0,"created_by":4,"updated_by":0,"deleted_at":"","created_at":"2019-05-24 09:58:28","updated_at":"2019-05-24 09:58:28","value":0}],"medical_items":[]}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * cart_service_anamnesis_id : 4
         * cart_service_id : 1
         * anamnesis :
         * diagnosis :
         * return_schedule_desc :
         * return_schedule_date :
         * created_by : 0
         * updated_by : 0
         * deleted_at :
         * created_at : 2019-05-28 06:13:23
         * updated_at : 2019-05-28 06:13:23
         * treatments : [{"cart_service_detail_id":1,"cart_service_id":1,"service_type":"1","service_id":1,"service_name":"Konsultasi Dokter","service_price":"165000.00","order_status":"1","is_invoice":0,"created_by":1,"updated_by":0,"deleted_at":"","created_at":"2019-05-28 04:55:21","updated_at":"2019-05-28 04:55:21"},{"cart_service_detail_id":2,"cart_service_id":1,"service_type":"2","service_id":1,"service_name":"SPA Kucing biasa","service_price":"100000.00","order_status":"1","is_invoice":0,"created_by":1,"updated_by":0,"deleted_at":"","created_at":"2019-05-28 04:55:31","updated_at":"2019-05-28 04:55:31"},{"cart_service_detail_id":5,"cart_service_id":1,"service_type":"3","service_id":1,"service_name":"Kandang Kucing S","service_price":"0.00","order_status":"1","is_invoice":0,"created_by":1,"updated_by":0,"deleted_at":"","created_at":"2019-05-28 05:22:12","updated_at":"2019-05-28 05:22:12"}]
         * clinical_checkups : [{"clinical_checkup_id":1,"clinical_checkup_name":"Berat Badan","clinical_checkup_unit":"Kg","clinical_checkup_desc":"-","clinical_checkup_is_priority":1,"created_by":4,"updated_by":0,"deleted_at":"","created_at":"2019-05-24 09:53:22","updated_at":"2019-05-24 09:53:22","value":0},{"clinical_checkup_id":2,"clinical_checkup_name":"Temperatur","clinical_checkup_unit":"C","clinical_checkup_desc":"-","clinical_checkup_is_priority":1,"created_by":4,"updated_by":0,"deleted_at":"","created_at":"2019-05-24 09:53:43","updated_at":"2019-05-24 09:53:43","value":0},{"clinical_checkup_id":3,"clinical_checkup_name":"Status Gizi","clinical_checkup_unit":"AKG","clinical_checkup_desc":"-","clinical_checkup_is_priority":0,"created_by":4,"updated_by":0,"deleted_at":"","created_at":"2019-05-24 09:54:55","updated_at":"2019-05-24 09:54:55","value":0},{"clinical_checkup_id":4,"clinical_checkup_name":"Kulit dan Telinga","clinical_checkup_unit":"Unit","clinical_checkup_desc":"-","clinical_checkup_is_priority":0,"created_by":4,"updated_by":0,"deleted_at":"","created_at":"2019-05-24 09:55:19","updated_at":"2019-05-24 09:55:19","value":0},{"clinical_checkup_id":5,"clinical_checkup_name":"Gigi dan Gusi","clinical_checkup_unit":"Unit","clinical_checkup_desc":"-","clinical_checkup_is_priority":0,"created_by":4,"updated_by":0,"deleted_at":"","created_at":"2019-05-24 09:55:53","updated_at":"2019-05-24 09:55:53","value":0},{"clinical_checkup_id":6,"clinical_checkup_name":"Selaput lendir, mata, mulut","clinical_checkup_unit":"Unit","clinical_checkup_desc":"-","clinical_checkup_is_priority":0,"created_by":4,"updated_by":4,"deleted_at":"","created_at":"2019-05-24 09:56:30","updated_at":"2019-05-24 10:00:08","value":0},{"clinical_checkup_id":7,"clinical_checkup_name":"Auskultasi Paru-paru","clinical_checkup_unit":"Unit","clinical_checkup_desc":"-","clinical_checkup_is_priority":0,"created_by":4,"updated_by":0,"deleted_at":"","created_at":"2019-05-24 09:57:06","updated_at":"2019-05-24 09:57:06","value":0},{"clinical_checkup_id":8,"clinical_checkup_name":"Auskultasi Jantung","clinical_checkup_unit":"Unit","clinical_checkup_desc":"-","clinical_checkup_is_priority":0,"created_by":4,"updated_by":0,"deleted_at":"","created_at":"2019-05-24 09:57:22","updated_at":"2019-05-24 09:57:22","value":0},{"clinical_checkup_id":9,"clinical_checkup_name":"Suara Pencernaan","clinical_checkup_unit":"Unit","clinical_checkup_desc":"-","clinical_checkup_is_priority":0,"created_by":4,"updated_by":0,"deleted_at":"","created_at":"2019-05-24 09:57:50","updated_at":"2019-05-24 09:57:50","value":0},{"clinical_checkup_id":10,"clinical_checkup_name":"Frekuensi Nafas","clinical_checkup_unit":"Unit","clinical_checkup_desc":"-","clinical_checkup_is_priority":0,"created_by":4,"updated_by":4,"deleted_at":"","created_at":"2019-05-24 09:58:08","updated_at":"2019-05-24 10:00:21","value":0},{"clinical_checkup_id":11,"clinical_checkup_name":"Frekuensi Detak Jantung","clinical_checkup_unit":"Unit","clinical_checkup_desc":"-","clinical_checkup_is_priority":0,"created_by":4,"updated_by":0,"deleted_at":"","created_at":"2019-05-24 09:58:28","updated_at":"2019-05-24 09:58:28","value":0}]
         * medical_items : []
         */

        @SerializedName("cart_service_anamnesis_id")
        private int cartServiceAnamnesisId;
        @SerializedName("cart_service_id")
        private int cartServiceId;
        @SerializedName("anamnesis")
        private String anamnesis;
        @SerializedName("diagnosis")
        private String diagnosis;
        @SerializedName("return_schedule_desc")
        private String returnScheduleDesc;
        @SerializedName("return_schedule_date")
        private String returnScheduleDate;
        @SerializedName("created_by")
        private int createdBy;
        @SerializedName("updated_by")
        private int updatedBy;
        @SerializedName("deleted_at")
        private String deletedAt;
        @SerializedName("created_at")
        private String createdAt;
        @SerializedName("updated_at")
        private String updatedAt;
        @SerializedName("treatments")
        private List<TreatmentsBean> treatments;
        @SerializedName("clinical_checkups")
        private List<ClinicalCheckupsBean> clinicalCheckups;
        @SerializedName("medical_items")
        private List<MedicalItemsBean> medicalItems;

        public int getCartServiceAnamnesisId() {
            return cartServiceAnamnesisId;
        }

        public void setCartServiceAnamnesisId(int cartServiceAnamnesisId) {
            this.cartServiceAnamnesisId = cartServiceAnamnesisId;
        }

        public int getCartServiceId() {
            return cartServiceId;
        }

        public void setCartServiceId(int cartServiceId) {
            this.cartServiceId = cartServiceId;
        }

        public String getAnamnesis() {
            return anamnesis;
        }

        public void setAnamnesis(String anamnesis) {
            this.anamnesis = anamnesis;
        }

        public String getDiagnosis() {
            return diagnosis;
        }

        public void setDiagnosis(String diagnosis) {
            this.diagnosis = diagnosis;
        }

        public String getReturnScheduleDesc() {
            return returnScheduleDesc;
        }

        public void setReturnScheduleDesc(String returnScheduleDesc) {
            this.returnScheduleDesc = returnScheduleDesc;
        }

        public String getReturnScheduleDate() {
            return returnScheduleDate;
        }

        public void setReturnScheduleDate(String returnScheduleDate) {
            this.returnScheduleDate = returnScheduleDate;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public int getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(String deletedAt) {
            this.deletedAt = deletedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public List<TreatmentsBean> getTreatments() {
            return treatments;
        }

        public void setTreatments(List<TreatmentsBean> treatments) {
            this.treatments = treatments;
        }

        public List<ClinicalCheckupsBean> getClinicalCheckups() {
            return clinicalCheckups;
        }

        public void setClinicalCheckups(List<ClinicalCheckupsBean> clinicalCheckups) {
            this.clinicalCheckups = clinicalCheckups;
        }

        public List<MedicalItemsBean> getMedicalItems() {
            return medicalItems;
        }

        public void setMedicalItems(List<MedicalItemsBean> medicalItems) {
            this.medicalItems = medicalItems;
        }


        public static class TreatmentsBean {
            /**
             * cart_service_detail_id : 1
             * cart_service_id : 1
             * service_type : 1
             * service_id : 1
             * service_name : Konsultasi Dokter
             * service_price : 165000.00
             * order_status : 1
             * is_invoice : 0
             * created_by : 1
             * updated_by : 0
             * deleted_at :
             * created_at : 2019-05-28 04:55:21
             * updated_at : 2019-05-28 04:55:21
             */

            @SerializedName("cart_service_detail_id")
            private int cartServiceDetailId;
            @SerializedName("cart_service_id")
            private int cartServiceId;
            @SerializedName("service_type")
            private String serviceType;
            @SerializedName("service_id")
            private int serviceId;
            @SerializedName("service_name")
            private String serviceName;
            @SerializedName("service_price")
            private String servicePrice;
            @SerializedName("order_status")
            private String orderStatus;
            @SerializedName("is_invoice")
            private int isInvoice;
            @SerializedName("created_by")
            private int createdBy;
            @SerializedName("updated_by")
            private int updatedBy;
            @SerializedName("deleted_at")
            private String deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;

            public int getCartServiceDetailId() {
                return cartServiceDetailId;
            }

            public void setCartServiceDetailId(int cartServiceDetailId) {
                this.cartServiceDetailId = cartServiceDetailId;
            }

            public int getCartServiceId() {
                return cartServiceId;
            }

            public void setCartServiceId(int cartServiceId) {
                this.cartServiceId = cartServiceId;
            }

            public String getServiceType() {
                return serviceType;
            }

            public void setServiceType(String serviceType) {
                this.serviceType = serviceType;
            }

            public int getServiceId() {
                return serviceId;
            }

            public void setServiceId(int serviceId) {
                this.serviceId = serviceId;
            }

            public String getServiceName() {
                return serviceName;
            }

            public void setServiceName(String serviceName) {
                this.serviceName = serviceName;
            }

            public String getServicePrice() {
                return servicePrice;
            }

            public void setServicePrice(String servicePrice) {
                this.servicePrice = servicePrice;
            }

            public String getOrderStatus() {
                return orderStatus;
            }

            public void setOrderStatus(String orderStatus) {
                this.orderStatus = orderStatus;
            }

            public int getIsInvoice() {
                return isInvoice;
            }

            public void setIsInvoice(int isInvoice) {
                this.isInvoice = isInvoice;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(String deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }
        }

        public static class ClinicalCheckupsBean {
            /**
             * clinical_checkup_id : 1
             * clinical_checkup_name : Berat Badan
             * clinical_checkup_unit : Kg
             * clinical_checkup_desc : -
             * clinical_checkup_is_priority : 1
             * created_by : 4
             * updated_by : 0
             * deleted_at :
             * created_at : 2019-05-24 09:53:22
             * updated_at : 2019-05-24 09:53:22
             * value : 0
             */

            @SerializedName("clinical_checkup_id")
            private int clinicalCheckupId;
            @SerializedName("clinical_checkup_name")
            private String clinicalCheckupName;
            @SerializedName("clinical_checkup_unit")
            private String clinicalCheckupUnit;
            @SerializedName("clinical_checkup_desc")
            private String clinicalCheckupDesc;
            @SerializedName("clinical_checkup_type")
            private String clinicalCheckupType;
            @SerializedName("clinical_checkup_is_priority")
            private int clinicalCheckupIsPriority;
            @SerializedName("created_by")
            private int createdBy;
            @SerializedName("updated_by")
            private int updatedBy;
            @SerializedName("deleted_at")
            private String deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
            @SerializedName("value")
            private String value;
            private boolean isExam = false;

            public int getClinicalCheckupId() {
                return clinicalCheckupId;
            }

            public void setClinicalCheckupId(int clinicalCheckupId) {
                this.clinicalCheckupId = clinicalCheckupId;
            }

            public String getClinicalCheckupName() {
                return clinicalCheckupName;
            }

            public void setClinicalCheckupName(String clinicalCheckupName) {
                this.clinicalCheckupName = clinicalCheckupName;
            }

            public String getClinicalCheckupUnit() {
                return clinicalCheckupUnit;
            }

            public void setClinicalCheckupUnit(String clinicalCheckupUnit) {
                this.clinicalCheckupUnit = clinicalCheckupUnit;
            }

            public String getClinicalCheckupDesc() {
                return clinicalCheckupDesc;
            }

            public void setClinicalCheckupDesc(String clinicalCheckupDesc) {
                this.clinicalCheckupDesc = clinicalCheckupDesc;
            }

            public int getClinicalCheckupIsPriority() {
                return clinicalCheckupIsPriority;
            }

            public void setClinicalCheckupIsPriority(int clinicalCheckupIsPriority) {
                this.clinicalCheckupIsPriority = clinicalCheckupIsPriority;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(String deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {

                this.value = value;
            }

            public boolean isExam() {
                return isExam;
            }

            public void setExam(boolean exam) {
                isExam = exam;
            }

            public String getClinicalCheckupType() {
                return clinicalCheckupType;
            }

            public void setClinicalCheckupType(String clinicalCheckupType) {
                this.clinicalCheckupType = clinicalCheckupType;
            }
        }

        public static class MedicalItemsBean {
            /**
             * anamnesis_medical_item_id : 4
             * cart_service_id : 3
             * product_id : 1
             * product_name : Obat Kutu
             * product_price : 23000.00
             * product_qty : 5
             * product_note : Jangan lupa buble wraps
             * discount_id : 4
             * discount_name : Happy Hours 5%
             * doctor_id : 0
             * doctor_name :
             * is_invoice : 0
             * created_by : 4
             * updated_by : 0
             * deleted_at : null
             * created_at : 2019-05-29 09:13:11
             * updated_at : 2019-05-29 09:13:11
             */

            @SerializedName("anamnesis_medical_item_id")
            private int anamnesisMedicalItemId;
            @SerializedName("cart_service_id")
            private int cartServiceId;
            @SerializedName("product_id")
            private int productId;
            @SerializedName("product_name")
            private String productName;
            @SerializedName("product_price")
            private String productPrice;
            @SerializedName("product_qty")
            private int productQty;
            @SerializedName("product_note")
            private String productNote;
            @SerializedName("discount_id")
            private int discountId;
            @SerializedName("discount_name")
            private String discountName;
            @SerializedName("doctor_id")
            private int doctorId;
            @SerializedName("doctor_name")
            private String doctorName;
            @SerializedName("is_invoice")
            private int isInvoice;
            @SerializedName("created_by")
            private int createdBy;
            @SerializedName("updated_by")
            private int updatedBy;
            @SerializedName("deleted_at")
            private Object deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;

            public int getAnamnesisMedicalItemId() {
                return anamnesisMedicalItemId;
            }

            public void setAnamnesisMedicalItemId(int anamnesisMedicalItemId) {
                this.anamnesisMedicalItemId = anamnesisMedicalItemId;
            }

            public int getCartServiceId() {
                return cartServiceId;
            }

            public void setCartServiceId(int cartServiceId) {
                this.cartServiceId = cartServiceId;
            }

            public int getProductId() {
                return productId;
            }

            public void setProductId(int productId) {
                this.productId = productId;
            }

            public String getProductName() {
                return productName;
            }

            public void setProductName(String productName) {
                this.productName = productName;
            }

            public String getProductPrice() {
                return productPrice;
            }

            public void setProductPrice(String productPrice) {
                this.productPrice = productPrice;
            }

            public int getProductQty() {
                return productQty;
            }

            public void setProductQty(int productQty) {
                this.productQty = productQty;
            }

            public String getProductNote() {
                return productNote;
            }

            public void setProductNote(String productNote) {
                this.productNote = productNote;
            }

            public int getDiscountId() {
                return discountId;
            }

            public void setDiscountId(int discountId) {
                this.discountId = discountId;
            }

            public String getDiscountName() {
                return discountName;
            }

            public void setDiscountName(String discountName) {
                this.discountName = discountName;
            }

            public int getDoctorId() {
                return doctorId;
            }

            public void setDoctorId(int doctorId) {
                this.doctorId = doctorId;
            }

            public String getDoctorName() {
                return doctorName;
            }

            public void setDoctorName(String doctorName) {
                this.doctorName = doctorName;
            }

            public int getIsInvoice() {
                return isInvoice;
            }

            public void setIsInvoice(int isInvoice) {
                this.isInvoice = isInvoice;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public Object getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(Object deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }


        }


    }

}
