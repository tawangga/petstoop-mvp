package com.tawangga.petstoopmvp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MedicalServiceModel {

    /**
     * STATUS_CODE : 200
     * MESSAGE : SUCCESS
     * DATA : [{"medical_id":1,"medical_name":"Service Medical A","medical_price":"0.00","medical_desc":"medical service","created_date":2019,"created_by":12,"update_date":0,"update_by":0,"status":"1"},{"medical_id":2,"medical_name":"Service Medical B","medical_price":"0.00","medical_desc":"medical service","created_date":2019,"created_by":12,"update_date":0,"update_by":0,"status":"1"},{"medical_id":3,"medical_name":"Service Medical C","medical_price":"0.00","medical_desc":"medical service","created_date":2019,"created_by":12,"update_date":0,"update_by":0,"status":"1"},{"medical_id":4,"medical_name":"Service Medical D","medical_price":"0.00","medical_desc":"medical service","created_date":2019,"created_by":12,"update_date":2019,"update_by":12,"status":"1"}]
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private List<DATABean> DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public List<DATABean> getDATA() {
        return DATA;
    }

    public void setDATA(List<DATABean> DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * medical_id : 1
         * medical_name : Service Medical A
         * medical_price : 0.00
         * medical_desc : medical service
         * created_date : 2019
         * created_by : 12
         * update_date : 0
         * update_by : 0
         * status : 1
         */

        @SerializedName("medical_id")
        private int medicalId;
        @SerializedName("medical_name")
        private String medicalName;
        @SerializedName("medical_price")
        private String medicalPrice;
        @SerializedName("medical_desc")
        private String medicalDesc;
        @SerializedName("created_date")
        private int createdDate;
        @SerializedName("created_by")
        private int createdBy;
        @SerializedName("update_date")
        private int updateDate;
        @SerializedName("update_by")
        private int updateBy;
        @SerializedName("status")
        private String status;
        private boolean isAdded;

        public int getMedicalId() {
            return medicalId;
        }

        public void setMedicalId(int medicalId) {
            this.medicalId = medicalId;
        }

        public String getMedicalName() {
            return medicalName;
        }

        public void setMedicalName(String medicalName) {
            this.medicalName = medicalName;
        }

        public String getMedicalPrice() {
            return medicalPrice;
        }

        public void setMedicalPrice(String medicalPrice) {
            this.medicalPrice = medicalPrice;
        }

        public String getMedicalDesc() {
            return medicalDesc;
        }

        public void setMedicalDesc(String medicalDesc) {
            this.medicalDesc = medicalDesc;
        }

        public int getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(int createdDate) {
            this.createdDate = createdDate;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public int getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(int updateDate) {
            this.updateDate = updateDate;
        }

        public int getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(int updateBy) {
            this.updateBy = updateBy;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public boolean isAdded() {
            return isAdded;
        }

        public void setAdded(boolean added) {
            isAdded = added;
        }
    }
}
