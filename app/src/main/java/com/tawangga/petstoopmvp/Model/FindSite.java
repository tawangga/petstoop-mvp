package com.tawangga.petstoopmvp.Model;

import com.google.gson.annotations.SerializedName;

public class FindSite {
    /**
     * STATUS_CODE : 200
     * MESSAGE : Site found
     * DATA : {"site_id":37,"site_code":"S000037","client_code":"C000034"}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * site_id : 37
         * site_code : S000037
         * client_code : C000034
         */

        @SerializedName("site_id")
        private int siteId;
        @SerializedName("site_code")
        private String siteCode;
        @SerializedName("client_code")
        private String clientCode;

        public int getSiteId() {
            return siteId;
        }

        public void setSiteId(int siteId) {
            this.siteId = siteId;
        }

        public String getSiteCode() {
            return siteCode;
        }

        public void setSiteCode(String siteCode) {
            this.siteCode = siteCode;
        }

        public String getClientCode() {
            return clientCode;
        }

        public void setClientCode(String clientCode) {
            this.clientCode = clientCode;
        }
    }
}
