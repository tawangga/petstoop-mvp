package com.tawangga.petstoopmvp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PetListModel {

    /**
     * STATUS_CODE : 200
     * MESSAGE : Success
     * DATA : {"page":1,"last_page":1,"total_item":3,"limit":"10","items":[{"pet_id":1,"customer_id":2,"pet_name":"Moore","pet_race":"Halder","pet_birthday":"2019-02-03","pet_gender":"1","pet_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557897678.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 10:54:49","updated_at":"2019-05-15 10:54:49","customer_name":"Jhon Smith","customer_email":"jhon.smith@mail.com","customer_phone":"0812345868","customer_address":"Jln Mawar","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557892489.png"},{"pet_id":2,"customer_id":3,"pet_name":"Fitch","pet_race":"Angora","pet_birthday":"2019-01-02","pet_gender":"1","pet_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557898290.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 10:55:38","updated_at":"2019-05-15 10:55:38","customer_name":"Henry S. Browne","customer_email":"henry@gmail.com","customer_phone":"9205958973","customer_address":"","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557892538.png"},{"pet_id":3,"customer_id":4,"pet_name":"Vida","pet_race":"Angora","pet_birthday":"2019-01-02","pet_gender":"2","pet_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557899398.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 10:56:29","updated_at":"2019-05-16 01:42:17","customer_name":"Lara A. Harding","customer_email":"harding@gmail.com","customer_phone":"662912710","customer_address":"test","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557892589.png"}]}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * page : 1
         * last_page : 1
         * total_item : 3
         * limit : 10
         * items : [{"pet_id":1,"customer_id":2,"pet_name":"Moore","pet_race":"Halder","pet_birthday":"2019-02-03","pet_gender":"1","pet_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557897678.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 10:54:49","updated_at":"2019-05-15 10:54:49","customer_name":"Jhon Smith","customer_email":"jhon.smith@mail.com","customer_phone":"0812345868","customer_address":"Jln Mawar","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557892489.png"},{"pet_id":2,"customer_id":3,"pet_name":"Fitch","pet_race":"Angora","pet_birthday":"2019-01-02","pet_gender":"1","pet_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557898290.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 10:55:38","updated_at":"2019-05-15 10:55:38","customer_name":"Henry S. Browne","customer_email":"henry@gmail.com","customer_phone":"9205958973","customer_address":"","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557892538.png"},{"pet_id":3,"customer_id":4,"pet_name":"Vida","pet_race":"Angora","pet_birthday":"2019-01-02","pet_gender":"2","pet_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557899398.png","created_on":"1","updated_on":"1","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-15 10:56:29","updated_at":"2019-05-16 01:42:17","customer_name":"Lara A. Harding","customer_email":"harding@gmail.com","customer_phone":"662912710","customer_address":"test","customer_photo":"http://178.128.62.50/petv2/api/public/images/customer-1557892589.png"}]
         */

        @SerializedName("page")
        private int page;
        @SerializedName("last_page")
        private int lastPage;
        @SerializedName("total_item")
        private int totalItem;
        @SerializedName("limit")
        private String limit;
        @SerializedName("items")
        private List<ItemsBean> items;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public int getTotalItem() {
            return totalItem;
        }

        public void setTotalItem(int totalItem) {
            this.totalItem = totalItem;
        }

        public String getLimit() {
            return limit;
        }

        public void setLimit(String limit) {
            this.limit = limit;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class ItemsBean {
            /**
             * pet_id : 1
             * customer_id : 2
             * pet_name : Moore
             * pet_race : Halder
             * pet_birthday : 2019-02-03
             * pet_gender : 1
             * pet_photo : http://178.128.62.50/petv2/api/public/images/customer-1557897678.png
             * created_on : 1
             * updated_on : 1
             * created_by : 4
             * updated_by : 4
             * deleted_at : null
             * created_at : 2019-05-15 10:54:49
             * updated_at : 2019-05-15 10:54:49
             * customer_name : Jhon Smith
             * customer_email : jhon.smith@mail.com
             * customer_phone : 0812345868
             * customer_address : Jln Mawar
             * customer_photo : http://178.128.62.50/petv2/api/public/images/customer-1557892489.png
             */

            @SerializedName("pet_id")
            private int petId;
            @SerializedName("customer_id")
            private int customerId;
            @SerializedName("pet_name")
            private String petName;
            @SerializedName("pet_race")
            private String petRace;
            @SerializedName("pet_birthday")
            private String petBirthday;
            @SerializedName("pet_gender")
            private String petGender;
            @SerializedName("pet_photo")
            private String petPhoto;
            @SerializedName("created_on")
            private String createdOn;
            @SerializedName("updated_on")
            private String updatedOn;
            @SerializedName("created_by")
            private int createdBy;
            @SerializedName("updated_by")
            private int updatedBy;
            @SerializedName("deleted_at")
            private String deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
            @SerializedName("customer_name")
            private String customerName;
            @SerializedName("customer_email")
            private String customerEmail;
            @SerializedName("customer_phone")
            private String customerPhone;
            @SerializedName("customer_address")
            private String customerAddress;
            @SerializedName("customer_photo")
            private String customerPhoto;
            @SerializedName("pet_code")
            private String petCode;
            @SerializedName("customer_code")
            private String customerCode;

            public int getPetId() {
                return petId;
            }

            public void setPetId(int petId) {
                this.petId = petId;
            }

            public int getCustomerId() {
                return customerId;
            }

            public void setCustomerId(int customerId) {
                this.customerId = customerId;
            }

            public String getPetName() {
                return petName;
            }

            public void setPetName(String petName) {
                this.petName = petName;
            }

            public String getPetRace() {
                return petRace;
            }

            public void setPetRace(String petRace) {
                this.petRace = petRace;
            }

            public String getPetBirthday() {
                return petBirthday;
            }

            public void setPetBirthday(String petBirthday) {
                this.petBirthday = petBirthday;
            }

            public String getPetGender() {
                return petGender;
            }

            public void setPetGender(String petGender) {
                this.petGender = petGender;
            }

            public String getPetPhoto() {
                return petPhoto;
            }

            public void setPetPhoto(String petPhoto) {
                this.petPhoto = petPhoto;
            }

            public String getCreatedOn() {
                return createdOn;
            }

            public void setCreatedOn(String createdOn) {
                this.createdOn = createdOn;
            }

            public String getUpdatedOn() {
                return updatedOn;
            }

            public void setUpdatedOn(String updatedOn) {
                this.updatedOn = updatedOn;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(String deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public String getCustomerName() {
                return customerName;
            }

            public void setCustomerName(String customerName) {
                this.customerName = customerName;
            }

            public String getCustomerEmail() {
                return customerEmail;
            }

            public void setCustomerEmail(String customerEmail) {
                this.customerEmail = customerEmail;
            }

            public String getCustomerPhone() {
                return customerPhone;
            }

            public void setCustomerPhone(String customerPhone) {
                this.customerPhone = customerPhone;
            }

            public String getCustomerAddress() {
                return customerAddress;
            }

            public void setCustomerAddress(String customerAddress) {
                this.customerAddress = customerAddress;
            }

            public String getCustomerPhoto() {
                return customerPhoto;
            }

            public void setCustomerPhoto(String customerPhoto) {
                this.customerPhoto = customerPhoto;
            }

            public String getPetCode() {
                return petCode;
            }

            public void setPetCode(String petCode) {
                this.petCode = petCode;
            }

            public String getCustomerCode() {
                return customerCode;
            }

            public void setCustomerCode(String customerCode) {
                this.customerCode = customerCode;
            }
        }
    }
}
