package com.tawangga.petstoopmvp.Model;

import com.google.gson.annotations.SerializedName;

public class UpdateStaffModel {

    /**
     * STATUS_CODE : 200
     * MESSAGE : Success
     * DATA : {"staff_id":1,"staff_category_id":2,"name":"Rudi Tawangga","username":"tawangga","email":"rudi@gmail.com","phone":"083823163915","address":"-\n-","desc":"-0","photo":"http://192.168.1.82/images/staff-1557911315.png","token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOnsic3RhZmZfaWQiOjEsInN0YWZmX2NhdGVnb3J5IjoicmVzZXJ2YXRpb24ifSwiaWF0IjoxNTYyNjU5NDM1LCJleHAiOjE1NjI3MDI2MzV9.AvZOPWeVFknd4k2JbujvhRwuagZ-q9Pq4PAy2UvKswI","created_by":4,"updated_by":1,"deleted_at":null,"created_at":"2019-05-15 09:08:35","updated_at":"2019-07-09 08:05:10","staff_code":"S000001"}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * staff_id : 1
         * staff_category_id : 2
         * name : Rudi Tawangga
         * username : tawangga
         * email : rudi@gmail.com
         * phone : 083823163915
         * address : -
         -
         * desc : -0
         * photo : http://192.168.1.82/images/staff-1557911315.png
         * token : eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOnsic3RhZmZfaWQiOjEsInN0YWZmX2NhdGVnb3J5IjoicmVzZXJ2YXRpb24ifSwiaWF0IjoxNTYyNjU5NDM1LCJleHAiOjE1NjI3MDI2MzV9.AvZOPWeVFknd4k2JbujvhRwuagZ-q9Pq4PAy2UvKswI
         * created_by : 4
         * updated_by : 1
         * deleted_at : null
         * created_at : 2019-05-15 09:08:35
         * updated_at : 2019-07-09 08:05:10
         * staff_code : S000001
         */

        @SerializedName("staff_id")
        private int staffId;
        @SerializedName("staff_category_id")
        private int staffCategoryId;
        @SerializedName("name")
        private String name;
        @SerializedName("username")
        private String username;
        @SerializedName("email")
        private String email;
        @SerializedName("phone")
        private String phone;
        @SerializedName("address")
        private String address;
        @SerializedName("desc")
        private String desc;
        @SerializedName("photo")
        private String photo;
        @SerializedName("photo_old")
        private String photoOld;
        @SerializedName("token")
        private String token;
        @SerializedName("created_by")
        private int createdBy;
        @SerializedName("updated_by")
        private int updatedBy;
        @SerializedName("deleted_at")
        private Object deletedAt;
        @SerializedName("created_at")
        private String createdAt;
        @SerializedName("updated_at")
        private String updatedAt;
        @SerializedName("staff_code")
        private String staffCode;

        public int getStaffId() {
            return staffId;
        }

        public void setStaffId(int staffId) {
            this.staffId = staffId;
        }

        public int getStaffCategoryId() {
            return staffCategoryId;
        }

        public void setStaffCategoryId(int staffCategoryId) {
            this.staffCategoryId = staffCategoryId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public int getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            this.updatedBy = updatedBy;
        }

        public Object getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(Object deletedAt) {
            this.deletedAt = deletedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getStaffCode() {
            return staffCode;
        }

        public void setStaffCode(String staffCode) {
            this.staffCode = staffCode;
        }

        public String getPhotoOld() {
            return photoOld;
        }

        public void setPhotoOld(String photoOld) {
            this.photoOld = photoOld;
        }
    }
}