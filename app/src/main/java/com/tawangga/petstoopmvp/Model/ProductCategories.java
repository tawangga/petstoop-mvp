package com.tawangga.petstoopmvp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductCategories {
    /**
     * STATUS_CODE : 200
     * MESSAGE : Success
     * DATA : {"page":1,"last_page":1,"total_item":3,"limit":"10","items":[{"category_id":1,"category_name":"Medical Item","category_desc":"Obat-obatan","created_by":4,"updated_by":0,"deleted_at":null,"created_at":"2019-05-19 01:52:40","updated_at":"2019-05-19 01:52:40"},{"category_id":2,"category_name":"Food Cat","category_desc":"Makanan Kucing","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-19 01:52:15","updated_at":"2019-05-19 04:20:58"},{"category_id":3,"category_name":"Food Dog","category_desc":"Makanan Anjing","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-19 01:52:26","updated_at":"2019-05-19 04:21:08"}]}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * page : 1
         * last_page : 1
         * total_item : 3
         * limit : 10
         * items : [{"category_id":1,"category_name":"Medical Item","category_desc":"Obat-obatan","created_by":4,"updated_by":0,"deleted_at":null,"created_at":"2019-05-19 01:52:40","updated_at":"2019-05-19 01:52:40"},{"category_id":2,"category_name":"Food Cat","category_desc":"Makanan Kucing","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-19 01:52:15","updated_at":"2019-05-19 04:20:58"},{"category_id":3,"category_name":"Food Dog","category_desc":"Makanan Anjing","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-19 01:52:26","updated_at":"2019-05-19 04:21:08"}]
         */

        @SerializedName("page")
        private int page;
        @SerializedName("last_page")
        private int lastPage;
        @SerializedName("total_item")
        private int totalItem;
        @SerializedName("limit")
        private String limit;
        @SerializedName("items")
        private List<ItemsBean> items;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public int getTotalItem() {
            return totalItem;
        }

        public void setTotalItem(int totalItem) {
            this.totalItem = totalItem;
        }

        public String getLimit() {
            return limit;
        }

        public void setLimit(String limit) {
            this.limit = limit;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class ItemsBean {
            /**
             * category_id : 1
             * category_name : Medical Item
             * category_desc : Obat-obatan
             * created_by : 4
             * updated_by : 0
             * deleted_at : null
             * created_at : 2019-05-19 01:52:40
             * updated_at : 2019-05-19 01:52:40
             */

            @SerializedName("category_id")
            private int categoryId;
            @SerializedName("category_name")
            private String categoryName;
            @SerializedName("category_desc")
            private String categoryDesc;
            @SerializedName("created_by")
            private int createdBy;
            @SerializedName("updated_by")
            private int updatedBy;
            @SerializedName("deleted_at")
            private String deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;

            public int getCategoryId() {
                return categoryId;
            }

            public void setCategoryId(int categoryId) {
                this.categoryId = categoryId;
            }

            public String getCategoryName() {
                return categoryName;
            }

            public void setCategoryName(String categoryName) {
                this.categoryName = categoryName;
            }

            public String getCategoryDesc() {
                return categoryDesc;
            }

            public void setCategoryDesc(String categoryDesc) {
                this.categoryDesc = categoryDesc;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(String deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }
        }
    }
}
