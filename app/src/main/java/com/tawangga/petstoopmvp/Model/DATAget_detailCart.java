package com.tawangga.petstoopmvp.Model;

import com.google.gson.annotations.SerializedName;

public class DATAget_detailCart {

    /**
     * cart_product_detail_id : 5
     * cart_product_id : 1
     * product_id : 1
     * product_name : Obat Kutu
     * product_price : 20000.00
     * product_qty : 10
     * product_note : Jangan lupa buble wraps
     * discount_id : 4
     * discount_name : Happy Hours 5%
     * doctor_id : 0
     * doctor_name :
     * is_invoice : 0
     * created_by : 3
     * updated_by : 3
     * deleted_at : null
     * created_at : 2019-05-27 08:02:17
     * updated_at : 2019-05-27 08:35:58
     * product_image : http://192.168.1.82/images/product-1558509359.png
     * product_desc : -
     * total : 200000
     * discount_amount : 10000
     * discount : {"promo_id":4,"promo_name":"Happy Hours 5%","promo_desc":"-","promo_type":"1","promo_amount":"5.00","promo_disc_max":"15000.00","created_by":4,"updated_by":4,"deleted_at":null,"created_at":"2019-05-20 07:30:27","updated_at":"2019-05-20 07:45:25"}
     * grand_total : 190000
     */

    @SerializedName("cart_product_detail_id")
    private int cartProductDetailId;
    @SerializedName("cart_product_id")
    private int cartProductId;
    @SerializedName("cart_service_id")
    private int cartServiceId;
    @SerializedName("product_id")
    private int productId;
    @SerializedName("product_code")
    private String productCode;
    @SerializedName("product_name")
    private String productName;
    @SerializedName("product_price")
    private String productPrice;
    @SerializedName("product_qty")
    private int productQty;
    @SerializedName("product_available_qty")
    private int productAvailableQty;
    @SerializedName("product_note")
    private String productNote;
    @SerializedName("discount_id")
    private int discountId;
    @SerializedName("discount_name")
    private String discountName;
    @SerializedName("doctor_id")
    private int doctorId;
    @SerializedName("doctor_name")
    private String doctorName;
    @SerializedName("is_invoice")
    private int isInvoice;
    @SerializedName("created_by")
    private int createdBy;
    @SerializedName("updated_by")
    private int updatedBy;
    @SerializedName("deleted_at")
    private String deletedAt;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("product_image")
    private String productImage;
    @SerializedName("product_desc")
    private String productDesc;
    @SerializedName("total")
    private int total;
    @SerializedName("discount_amount")
    private double discountAmount;
    @SerializedName("discount")
    private DiscountBean discount;
    @SerializedName("grand_total")
    private int grandTotal;

    @SerializedName("category_id")
    private int categoryId;
    @SerializedName("category_name")
    private String categoryName;


    public int getCartProductDetailId() {
        return cartProductDetailId;
    }

    public void setCartProductDetailId(int cartProductDetailId) {
        this.cartProductDetailId = cartProductDetailId;
    }

    public int getCartProductId() {
        return cartProductId;
    }

    public void setCartProductId(int cartProductId) {
        this.cartProductId = cartProductId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public int getProductQty() {
        return productQty;
    }

    public void setProductQty(int productQty) {
        this.productQty = productQty;
    }

    public String getProductNote() {
        return productNote;
    }

    public void setProductNote(String productNote) {
        this.productNote = productNote;
    }

    public int getDiscountId() {
        return discountId;
    }

    public void setDiscountId(int discountId) {
        this.discountId = discountId;
    }

    public String getDiscountName() {
        return discountName;
    }

    public void setDiscountName(String discountName) {
        this.discountName = discountName;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public int getIsInvoice() {
        return isInvoice;
    }

    public void setIsInvoice(int isInvoice) {
        this.isInvoice = isInvoice;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public int getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(int updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public DiscountBean getDiscount() {
        return discount;
    }

    public void setDiscount(DiscountBean discount) {
        this.discount = discount;
    }

    public int getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(int grandTotal) {
        this.grandTotal = grandTotal;
    }

    public int getCartServiceId() {
        return cartServiceId;
    }

    public void setCartServiceId(int cartServiceId) {
        this.cartServiceId = cartServiceId;
    }

    public int getProductAvailableQty() {
        return productAvailableQty;
    }

    public void setProductAvailableQty(int productAvailableQty) {
        this.productAvailableQty = productAvailableQty;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public static class DiscountBean {
        /**
         * promo_id : 4
         * promo_name : Happy Hours 5%
         * promo_desc : -
         * promo_type : 1
         * promo_amount : 5.00
         * promo_disc_max : 15000.00
         * created_by : 4
         * updated_by : 4
         * deleted_at : null
         * created_at : 2019-05-20 07:30:27
         * updated_at : 2019-05-20 07:45:25
         */

        @SerializedName("promo_id")
        private int promoId;
        @SerializedName("promo_name")
        private String promoName;
        @SerializedName("promo_desc")
        private String promoDesc;
        @SerializedName("promo_type")
        private String promoType;
        @SerializedName("promo_amount")
        private String promoAmount;
        @SerializedName("promo_disc_max")
        private String promoDiscMax;
        @SerializedName("created_by")
        private int createdBy;
        @SerializedName("updated_by")
        private int updatedBy;
        @SerializedName("deleted_at")
        private String deletedAt;
        @SerializedName("created_at")
        private String createdAt;
        @SerializedName("updated_at")
        private String updatedAt;

        public int getPromoId() {
            return promoId;
        }

        public void setPromoId(int promoId) {
            this.promoId = promoId;
        }

        public String getPromoName() {
            return promoName;
        }

        public void setPromoName(String promoName) {
            this.promoName = promoName;
        }

        public String getPromoDesc() {
            return promoDesc;
        }

        public void setPromoDesc(String promoDesc) {
            this.promoDesc = promoDesc;
        }

        public String getPromoType() {
            return promoType;
        }

        public void setPromoType(String promoType) {
            this.promoType = promoType;
        }

        public String getPromoAmount() {
            return promoAmount;
        }

        public void setPromoAmount(String promoAmount) {
            this.promoAmount = promoAmount;
        }

        public String getPromoDiscMax() {
            return promoDiscMax;
        }

        public void setPromoDiscMax(String promoDiscMax) {
            this.promoDiscMax = promoDiscMax;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public int getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(String deletedAt) {
            this.deletedAt = deletedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }


    }


}


