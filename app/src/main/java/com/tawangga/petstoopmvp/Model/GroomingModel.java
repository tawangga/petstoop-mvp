package com.tawangga.petstoopmvp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GroomingModel {

    /**
     * STATUS_CODE : 200
     * MESSAGE : SUCCESS
     * DATA : [{"grooming_id":1,"grooming_name":"Service A","grooming_price":"0.00","grooming_desc":"service","created_date":"2019-03-13 16:12:22","created_by":1,"update_date":"2019-03-13 16:12:22","update_by":0,"status":"1"},{"grooming_id":2,"grooming_name":"Service B","grooming_price":"0.00","grooming_desc":"service","created_date":"2019-03-13 16:13:12","created_by":1,"update_date":"2019-03-13 16:15:06","update_by":1,"status":"1"},{"grooming_id":3,"grooming_name":"Service C","grooming_price":"0.00","grooming_desc":"service","created_date":"2019-03-13 16:13:24","created_by":1,"update_date":"2019-03-13 16:13:24","update_by":0,"status":"1"}]
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private List<DATABean> DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public List<DATABean> getDATA() {
        return DATA;
    }

    public void setDATA(List<DATABean> DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * grooming_id : 1
         * grooming_name : Service A
         * grooming_price : 0.00
         * grooming_desc : service
         * created_date : 2019-03-13 16:12:22
         * created_by : 1
         * update_date : 2019-03-13 16:12:22
         * update_by : 0
         * status : 1
         */

        @SerializedName("grooming_id")
        private int groomingId;
        @SerializedName("grooming_name")
        private String groomingName;
        @SerializedName("grooming_price")
        private String groomingPrice;
        @SerializedName("grooming_desc")
        private String groomingDesc;
        @SerializedName("created_date")
        private String createdDate;
        @SerializedName("created_by")
        private int createdBy;
        @SerializedName("update_date")
        private String updateDate;
        @SerializedName("update_by")
        private int updateBy;
        @SerializedName("status")
        private String status;
        private boolean isAdded;

        public int getGroomingId() {
            return groomingId;
        }

        public void setGroomingId(int groomingId) {
            this.groomingId = groomingId;
        }

        public String getGroomingName() {
            return groomingName;
        }

        public void setGroomingName(String groomingName) {
            this.groomingName = groomingName;
        }

        public String getGroomingPrice() {
            return groomingPrice;
        }

        public void setGroomingPrice(String groomingPrice) {
            this.groomingPrice = groomingPrice;
        }

        public String getGroomingDesc() {
            return groomingDesc;
        }

        public void setGroomingDesc(String groomingDesc) {
            this.groomingDesc = groomingDesc;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public String getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(String updateDate) {
            this.updateDate = updateDate;
        }

        public int getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(int updateBy) {
            this.updateBy = updateBy;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public boolean isAdded() {
            return isAdded;
        }

        public void setAdded(boolean added) {
            isAdded = added;
        }
    }
}
