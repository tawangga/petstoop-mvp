package com.tawangga.petstoopmvp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductModel {


    /**
     * STATUS_CODE : 200
     * MESSAGE : Success
     * DATA : {"page":1,"last_page":1,"total_item":1,"limit":"10","items":[{"product_id":1,"category_id":1,"product_code":"OKC1","product_name":"Obat Kutu","product_price":25000,"product_expired":"2020-03-19","product_image":"http://178.128.62.50/petv2/api/public/images/user-1558328548.png","product_qty":25,"product_desc":"-","created_by":4,"updated_by":0,"deleted_at":null,"created_at":"2019-05-20 12:02:28","updated_at":"2019-05-20 12:02:28","category":{"category_id":1,"category_name":"Medical Item"}}]}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * page : 1
         * last_page : 1
         * total_item : 1
         * limit : 10
         * items : [{"product_id":1,"category_id":1,"product_code":"OKC1","product_name":"Obat Kutu","product_price":25000,"product_expired":"2020-03-19","product_image":"http://178.128.62.50/petv2/api/public/images/user-1558328548.png","product_qty":25,"product_desc":"-","created_by":4,"updated_by":0,"deleted_at":null,"created_at":"2019-05-20 12:02:28","updated_at":"2019-05-20 12:02:28","category":{"category_id":1,"category_name":"Medical Item"}}]
         */

        @SerializedName("page")
        private int page;
        @SerializedName("last_page")
        private int lastPage;
        @SerializedName("total_item")
        private int totalItem;
        @SerializedName("limit")
        private String limit;
        @SerializedName("items")
        private List<ItemsBean> items;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public int getTotalItem() {
            return totalItem;
        }

        public void setTotalItem(int totalItem) {
            this.totalItem = totalItem;
        }

        public String getLimit() {
            return limit;
        }

        public void setLimit(String limit) {
            this.limit = limit;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class ItemsBean {
            /**
             * product_id : 1
             * category_id : 1
             * product_code : OKC1
             * product_name : Obat Kutu
             * product_price : 25000
             * product_expired : 2020-03-19
             * product_image : http://178.128.62.50/petv2/api/public/images/user-1558328548.png
             * product_qty : 25
             * product_desc : -
             * created_by : 4
             * updated_by : 0
             * deleted_at : null
             * created_at : 2019-05-20 12:02:28
             * updated_at : 2019-05-20 12:02:28
             * category : {"category_id":1,"category_name":"Medical Item"}
             */

            @SerializedName("product_id")
            private int productId;
            @SerializedName("category_id")
            private int categoryId;
            @SerializedName("product_code")
            private String productCode;
            @SerializedName("product_name")
            private String productName;
            @SerializedName("product_price")
            private int productPrice;
            @SerializedName("product_expired")
            private String productExpired;
            @SerializedName("product_image")
            private String productImage;
            @SerializedName("product_qty")
            private int productQty;
            @SerializedName("product_desc")
            private String productDesc;
            @SerializedName("created_by")
            private int createdBy;
            @SerializedName("updated_by")
            private int updatedBy;
            @SerializedName("deleted_at")
            private String deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
            @SerializedName("category")
            private CategoryBean category;

            public int getProductId() {
                return productId;
            }

            public void setProductId(int productId) {
                this.productId = productId;
            }

            public int getCategoryId() {
                return categoryId;
            }

            public void setCategoryId(int categoryId) {
                this.categoryId = categoryId;
            }

            public String getProductCode() {
                return productCode;
            }

            public void setProductCode(String productCode) {
                this.productCode = productCode;
            }

            public String getProductName() {
                return productName;
            }

            public void setProductName(String productName) {
                this.productName = productName;
            }

            public int getProductPrice() {
                return productPrice;
            }

            public void setProductPrice(int productPrice) {
                this.productPrice = productPrice;
            }

            public String getProductExpired() {
                return productExpired;
            }

            public void setProductExpired(String productExpired) {
                this.productExpired = productExpired;
            }

            public String getProductImage() {
                return productImage;
            }

            public void setProductImage(String productImage) {
                this.productImage = productImage;
            }

            public int getProductQty() {
                return productQty;
            }

            public void setProductQty(int productQty) {
                this.productQty = productQty;
            }

            public String getProductDesc() {
                return productDesc;
            }

            public void setProductDesc(String productDesc) {
                this.productDesc = productDesc;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(String deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public CategoryBean getCategory() {
                return category;
            }

            public void setCategory(CategoryBean category) {
                this.category = category;
            }

            public static class CategoryBean {
                /**
                 * category_id : 1
                 * category_name : Medical Item
                 */

                @SerializedName("category_id")
                private int categoryId;
                @SerializedName("category_name")
                private String categoryName;

                public int getCategoryId() {
                    return categoryId;
                }

                public void setCategoryId(int categoryId) {
                    this.categoryId = categoryId;
                }

                public String getCategoryName() {
                    return categoryName;
                }

                public void setCategoryName(String categoryName) {
                    this.categoryName = categoryName;
                }
            }
        }
    }
}
