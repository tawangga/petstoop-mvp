package com.tawangga.petstoopmvp.Model;

import com.google.gson.annotations.SerializedName;

public class AddCustomerModel {
    /**
     * STATUS_CODE : 200
     * MESSAGE : SUCCESS! DATA SAVED!
     * DATA : {"customer_name":"AKU ANAK ANAK","customer_email":"gembal@mail.com","customer_phone":"0210000221","customer_address":"Jl. jalan","customer_photo":"http://178.128.62.50/pet/api/public/image/app/images_default.png","created_by":"4","created_date":"2019-03-06 04:04:05","created_on":"2"}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * customer_name : AKU ANAK ANAK
         * customer_email : gembal@mail.com
         * customer_phone : 0210000221
         * customer_address : Jl. jalan
         * customer_photo : http://178.128.62.50/pet/api/public/image/app/images_default.png
         * created_by : 4
         * created_date : 2019-03-06 04:04:05
         * created_on : 2
         */

        @SerializedName("customer_name")
        private String customerName;
        @SerializedName("customer_email")
        private String customerEmail;
        @SerializedName("customer_phone")
        private String customerPhone;
        @SerializedName("customer_address")
        private String customerAddress;
        @SerializedName("customer_photo")
        private String customerPhoto;
        @SerializedName("created_by")
        private String createdBy;
        @SerializedName("created_date")
        private String createdDate;
        @SerializedName("created_on")
        private String createdOn;

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getCustomerEmail() {
            return customerEmail;
        }

        public void setCustomerEmail(String customerEmail) {
            this.customerEmail = customerEmail;
        }

        public String getCustomerPhone() {
            return customerPhone;
        }

        public void setCustomerPhone(String customerPhone) {
            this.customerPhone = customerPhone;
        }

        public String getCustomerAddress() {
            return customerAddress;
        }

        public void setCustomerAddress(String customerAddress) {
            this.customerAddress = customerAddress;
        }

        public String getCustomerPhoto() {
            return customerPhoto;
        }

        public void setCustomerPhoto(String customerPhoto) {
            this.customerPhoto = customerPhoto;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }
    }
}
