package com.tawangga.petstoopmvp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddToReservationModel {
    /**
     * STATUS_CODE : 200
     * MESSAGE : SUCCESS SAVED RESERVATION
     * DATA : {"service_id":1,"cart_service_type":"1","service_code":"R#1552878373-18","service_time":"20:00:00","service_date":"2019-02-01","service_end":"0000-00-00","pet_id":2,"pet_name":"baban","customer_id":3,"customer_name":"AGUN","doctor_id":3,"doctor_name":"lope","grooming_id":1,"grooming_name":"TEST","boarding_id":0,"boarding_name":"","payment_status":"0","created_date":"2019-03-18 10:06:13","created_by":1,"update_date":"2019-03-20 13:35:20","update_by":1,"status":"1","medical_service":[{"cart_detail_service_id":1,"cart_service_id":1,"service_type":"1","service_id":3,"service_name":"test","service_price":"10000.00","status":"1"},{"cart_detail_service_id":2,"cart_service_id":1,"service_type":"1","service_id":4,"service_name":"test2","service_price":"20000.00","status":"1"},{"cart_detail_service_id":12,"cart_service_id":1,"service_type":"1","service_id":1,"service_name":"Service Medical A","service_price":"0.00","status":"1"}],"grooming_service":[{"cart_detail_service_id":3,"cart_service_id":1,"service_type":"2","service_id":3,"service_name":"test","service_price":"0.00","status":"1"},{"cart_detail_service_id":15,"cart_service_id":1,"service_type":"2","service_id":2,"service_name":"Service B","service_price":"0.00","status":"1"}]}
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private DATABean DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public DATABean getDATA() {
        return DATA;
    }

    public void setDATA(DATABean DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * service_id : 1
         * cart_service_type : 1
         * service_code : R#1552878373-18
         * service_time : 20:00:00
         * service_date : 2019-02-01
         * service_end : 0000-00-00
         * pet_id : 2
         * pet_name : baban
         * customer_id : 3
         * customer_name : AGUN
         * doctor_id : 3
         * doctor_name : lope
         * grooming_id : 1
         * grooming_name : TEST
         * boarding_id : 0
         * boarding_name :
         * payment_status : 0
         * created_date : 2019-03-18 10:06:13
         * created_by : 1
         * update_date : 2019-03-20 13:35:20
         * update_by : 1
         * status : 1
         * medical_service : [{"cart_detail_service_id":1,"cart_service_id":1,"service_type":"1","service_id":3,"service_name":"test","service_price":"10000.00","status":"1"},{"cart_detail_service_id":2,"cart_service_id":1,"service_type":"1","service_id":4,"service_name":"test2","service_price":"20000.00","status":"1"},{"cart_detail_service_id":12,"cart_service_id":1,"service_type":"1","service_id":1,"service_name":"Service Medical A","service_price":"0.00","status":"1"}]
         * grooming_service : [{"cart_detail_service_id":3,"cart_service_id":1,"service_type":"2","service_id":3,"service_name":"test","service_price":"0.00","status":"1"},{"cart_detail_service_id":15,"cart_service_id":1,"service_type":"2","service_id":2,"service_name":"Service B","service_price":"0.00","status":"1"}]
         */

        @SerializedName("service_id")
        private int serviceId;
        @SerializedName("cart_service_type")
        private String cartServiceType;
        @SerializedName("service_code")
        private String serviceCode;
        @SerializedName("service_time")
        private String serviceTime;
        @SerializedName("service_date")
        private String serviceDate;
        @SerializedName("service_end")
        private String serviceEnd;
        @SerializedName("pet_id")
        private int petId;
        @SerializedName("pet_name")
        private String petName;
        @SerializedName("customer_id")
        private int customerId;
        @SerializedName("customer_name")
        private String customerName;
        @SerializedName("doctor_id")
        private int doctorId;
        @SerializedName("doctor_name")
        private String doctorName;
        @SerializedName("grooming_id")
        private int groomingId;
        @SerializedName("grooming_name")
        private String groomingName;
        @SerializedName("boarding_id")
        private int boardingId;
        @SerializedName("boarding_name")
        private String boardingName;
        @SerializedName("payment_status")
        private String paymentStatus;
        @SerializedName("created_date")
        private String createdDate;
        @SerializedName("created_by")
        private int createdBy;
        @SerializedName("update_date")
        private String updateDate;
        @SerializedName("update_by")
        private int updateBy;
        @SerializedName("status")
        private String status;
        @SerializedName("medical_service")
        private List<MedicalServiceBean> medicalService;
        @SerializedName("grooming_service")
        private List<GroomingServiceBean> groomingService;

        public int getServiceId() {
            return serviceId;
        }

        public void setServiceId(int serviceId) {
            this.serviceId = serviceId;
        }

        public String getCartServiceType() {
            return cartServiceType;
        }

        public void setCartServiceType(String cartServiceType) {
            this.cartServiceType = cartServiceType;
        }

        public String getServiceCode() {
            return serviceCode;
        }

        public void setServiceCode(String serviceCode) {
            this.serviceCode = serviceCode;
        }

        public String getServiceTime() {
            return serviceTime;
        }

        public void setServiceTime(String serviceTime) {
            this.serviceTime = serviceTime;
        }

        public String getServiceDate() {
            return serviceDate;
        }

        public void setServiceDate(String serviceDate) {
            this.serviceDate = serviceDate;
        }

        public String getServiceEnd() {
            return serviceEnd;
        }

        public void setServiceEnd(String serviceEnd) {
            this.serviceEnd = serviceEnd;
        }

        public int getPetId() {
            return petId;
        }

        public void setPetId(int petId) {
            this.petId = petId;
        }

        public String getPetName() {
            return petName;
        }

        public void setPetName(String petName) {
            this.petName = petName;
        }

        public int getCustomerId() {
            return customerId;
        }

        public void setCustomerId(int customerId) {
            this.customerId = customerId;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public int getDoctorId() {
            return doctorId;
        }

        public void setDoctorId(int doctorId) {
            this.doctorId = doctorId;
        }

        public String getDoctorName() {
            return doctorName;
        }

        public void setDoctorName(String doctorName) {
            this.doctorName = doctorName;
        }

        public int getGroomingId() {
            return groomingId;
        }

        public void setGroomingId(int groomingId) {
            this.groomingId = groomingId;
        }

        public String getGroomingName() {
            return groomingName;
        }

        public void setGroomingName(String groomingName) {
            this.groomingName = groomingName;
        }

        public int getBoardingId() {
            return boardingId;
        }

        public void setBoardingId(int boardingId) {
            this.boardingId = boardingId;
        }

        public String getBoardingName() {
            return boardingName;
        }

        public void setBoardingName(String boardingName) {
            this.boardingName = boardingName;
        }

        public String getPaymentStatus() {
            return paymentStatus;
        }

        public void setPaymentStatus(String paymentStatus) {
            this.paymentStatus = paymentStatus;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public String getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(String updateDate) {
            this.updateDate = updateDate;
        }

        public int getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(int updateBy) {
            this.updateBy = updateBy;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<MedicalServiceBean> getMedicalService() {
            return medicalService;
        }

        public void setMedicalService(List<MedicalServiceBean> medicalService) {
            this.medicalService = medicalService;
        }

        public List<GroomingServiceBean> getGroomingService() {
            return groomingService;
        }

        public void setGroomingService(List<GroomingServiceBean> groomingService) {
            this.groomingService = groomingService;
        }

        public static class MedicalServiceBean {
            /**
             * cart_detail_service_id : 1
             * cart_service_id : 1
             * service_type : 1
             * service_id : 3
             * service_name : test
             * service_price : 10000.00
             * status : 1
             */

            @SerializedName("cart_detail_service_id")
            private int cartDetailServiceId;
            @SerializedName("cart_service_id")
            private int cartServiceId;
            @SerializedName("service_type")
            private String serviceType;
            @SerializedName("service_id")
            private int serviceId;
            @SerializedName("service_name")
            private String serviceName;
            @SerializedName("service_price")
            private String servicePrice;
            @SerializedName("status")
            private String status;

            public int getCartDetailServiceId() {
                return cartDetailServiceId;
            }

            public void setCartDetailServiceId(int cartDetailServiceId) {
                this.cartDetailServiceId = cartDetailServiceId;
            }

            public int getCartServiceId() {
                return cartServiceId;
            }

            public void setCartServiceId(int cartServiceId) {
                this.cartServiceId = cartServiceId;
            }

            public String getServiceType() {
                return serviceType;
            }

            public void setServiceType(String serviceType) {
                this.serviceType = serviceType;
            }

            public int getServiceId() {
                return serviceId;
            }

            public void setServiceId(int serviceId) {
                this.serviceId = serviceId;
            }

            public String getServiceName() {
                return serviceName;
            }

            public void setServiceName(String serviceName) {
                this.serviceName = serviceName;
            }

            public String getServicePrice() {
                return servicePrice;
            }

            public void setServicePrice(String servicePrice) {
                this.servicePrice = servicePrice;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
        }

        public static class GroomingServiceBean {
            /**
             * cart_detail_service_id : 3
             * cart_service_id : 1
             * service_type : 2
             * service_id : 3
             * service_name : test
             * service_price : 0.00
             * status : 1
             */

            @SerializedName("cart_detail_service_id")
            private int cartDetailServiceId;
            @SerializedName("cart_service_id")
            private int cartServiceId;
            @SerializedName("service_type")
            private String serviceType;
            @SerializedName("service_id")
            private int serviceId;
            @SerializedName("service_name")
            private String serviceName;
            @SerializedName("service_price")
            private String servicePrice;
            @SerializedName("status")
            private String status;

            public int getCartDetailServiceId() {
                return cartDetailServiceId;
            }

            public void setCartDetailServiceId(int cartDetailServiceId) {
                this.cartDetailServiceId = cartDetailServiceId;
            }

            public int getCartServiceId() {
                return cartServiceId;
            }

            public void setCartServiceId(int cartServiceId) {
                this.cartServiceId = cartServiceId;
            }

            public String getServiceType() {
                return serviceType;
            }

            public void setServiceType(String serviceType) {
                this.serviceType = serviceType;
            }

            public int getServiceId() {
                return serviceId;
            }

            public void setServiceId(int serviceId) {
                this.serviceId = serviceId;
            }

            public String getServiceName() {
                return serviceName;
            }

            public void setServiceName(String serviceName) {
                this.serviceName = serviceName;
            }

            public String getServicePrice() {
                return servicePrice;
            }

            public void setServicePrice(String servicePrice) {
                this.servicePrice = servicePrice;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
        }
    }
}
