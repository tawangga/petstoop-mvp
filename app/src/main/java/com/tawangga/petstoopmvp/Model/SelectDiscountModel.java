package com.tawangga.petstoopmvp.Model;

public class SelectDiscountModel {
    private boolean isSelected;
    private String discount_name;

    public SelectDiscountModel(String discount_name, boolean isSelected) {
        this.isSelected = isSelected;
        this.discount_name = discount_name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getDiscount_name() {
        return discount_name;
    }

    public void setDiscount_name(String discount_name) {
        this.discount_name = discount_name;
    }
}
