package com.tawangga.petstoopmvp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DoctorModel {

    /**
     * STATUS_CODE : 200
     * MESSAGE : SUCCESS
     * DATA : [{"doctor_id":1,"doctor_name":"Jhon","doctor_email":"Jhon@mail.com","doctor_phone":"123","doctor_address":"jl.aaaa","doctor_days":"Monday,Tuesday,Wednesday,Saturday","time_oprational_start":"07:00:00","time_oprational_end":"03:00:00","doctor_desc":"abc","doctor_photo":"1550722045.jpg","doctor_available":"2","created_by":1,"created_date":"2019-02-21 04:07:25","update_by":12,"update_date":"2019-03-20 16:04:47","status":"1"},{"doctor_id":3,"doctor_name":"Doctor 2","doctor_email":"doctor2@email.com","doctor_phone":"0210000000","doctor_address":"jl.address","doctor_days":"Monday,Tuesday,Wednesday,Sunday","time_oprational_start":"07:00:00","time_oprational_end":"06:00:00","doctor_desc":"doctor","doctor_photo":"1552528117.jpg","doctor_available":"1","created_by":12,"created_date":"2019-03-14 01:48:37","update_by":12,"update_date":"2019-03-20 16:05:10","status":"1"}]
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private List<DATABean> DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public List<DATABean> getDATA() {
        return DATA;
    }

    public void setDATA(List<DATABean> DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * doctor_id : 1
         * doctor_name : Jhon
         * doctor_email : Jhon@mail.com
         * doctor_phone : 123
         * doctor_address : jl.aaaa
         * doctor_days : Monday,Tuesday,Wednesday,Saturday
         * time_oprational_start : 07:00:00
         * time_oprational_end : 03:00:00
         * doctor_desc : abc
         * doctor_photo : 1550722045.jpg
         * doctor_available : 2
         * created_by : 1
         * created_date : 2019-02-21 04:07:25
         * update_by : 12
         * update_date : 2019-03-20 16:04:47
         * status : 1
         */

        @SerializedName("doctor_id")
        private int doctorId;
        @SerializedName("doctor_name")
        private String doctorName;
        @SerializedName("doctor_email")
        private String doctorEmail;
        @SerializedName("doctor_phone")
        private String doctorPhone;
        @SerializedName("doctor_address")
        private String doctorAddress;
        @SerializedName("doctor_days")
        private String doctorDays;
        @SerializedName("time_oprational_start")
        private String timeOprationalStart;
        @SerializedName("time_oprational_end")
        private String timeOprationalEnd;
        @SerializedName("doctor_desc")
        private String doctorDesc;
        @SerializedName("doctor_photo")
        private String doctorPhoto;
        @SerializedName("doctor_available")
        private String doctorAvailable;
        @SerializedName("created_by")
        private int createdBy;
        @SerializedName("created_date")
        private String createdDate;
        @SerializedName("update_by")
        private int updateBy;
        @SerializedName("update_date")
        private String updateDate;
        @SerializedName("status")
        private String status;

        public int getDoctorId() {
            return doctorId;
        }

        public void setDoctorId(int doctorId) {
            this.doctorId = doctorId;
        }

        public String getDoctorName() {
            return doctorName;
        }

        public void setDoctorName(String doctorName) {
            this.doctorName = doctorName;
        }

        public String getDoctorEmail() {
            return doctorEmail;
        }

        public void setDoctorEmail(String doctorEmail) {
            this.doctorEmail = doctorEmail;
        }

        public String getDoctorPhone() {
            return doctorPhone;
        }

        public void setDoctorPhone(String doctorPhone) {
            this.doctorPhone = doctorPhone;
        }

        public String getDoctorAddress() {
            return doctorAddress;
        }

        public void setDoctorAddress(String doctorAddress) {
            this.doctorAddress = doctorAddress;
        }

        public String getDoctorDays() {
            return doctorDays;
        }

        public void setDoctorDays(String doctorDays) {
            this.doctorDays = doctorDays;
        }

        public String getTimeOprationalStart() {
            return timeOprationalStart;
        }

        public void setTimeOprationalStart(String timeOprationalStart) {
            this.timeOprationalStart = timeOprationalStart;
        }

        public String getTimeOprationalEnd() {
            return timeOprationalEnd;
        }

        public void setTimeOprationalEnd(String timeOprationalEnd) {
            this.timeOprationalEnd = timeOprationalEnd;
        }

        public String getDoctorDesc() {
            return doctorDesc;
        }

        public void setDoctorDesc(String doctorDesc) {
            this.doctorDesc = doctorDesc;
        }

        public String getDoctorPhoto() {
            return doctorPhoto;
        }

        public void setDoctorPhoto(String doctorPhoto) {
            this.doctorPhoto = doctorPhoto;
        }

        public String getDoctorAvailable() {
            return doctorAvailable;
        }

        public void setDoctorAvailable(String doctorAvailable) {
            this.doctorAvailable = doctorAvailable;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public int getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(int updateBy) {
            this.updateBy = updateBy;
        }

        public String getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(String updateDate) {
            this.updateDate = updateDate;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
