package com.tawangga.petstoopmvp.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetOrders {

    /**
     * STATUS_CODE : 200
     * MESSAGE : SUCCESS!
     * DATA : [{"order_id":9,"order_type":1,"order_type_name":"Treatment","order_code":"R#1553668613-27","customer_id":5,"customer_name":"Lara A. Harding","pet_name":"Kssk","pet_id":32,"items":[{"item_id":96,"item_name":"Service Medical B","item_type":"","item_price":"0.00","item_qty":0,"item_note":""},{"item_id":98,"item_name":"Service B","item_type":"","item_price":"0.00","item_qty":0,"item_note":""}],"created_at":"2019-03-27 13:36:53"}]
     */

    @SerializedName("STATUS_CODE")
    private String STATUSCODE;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("DATA")
    private List<DATABean> DATA;

    public String getSTATUSCODE() {
        return STATUSCODE;
    }

    public void setSTATUSCODE(String STATUSCODE) {
        this.STATUSCODE = STATUSCODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public List<DATABean> getDATA() {
        return DATA;
    }

    public void setDATA(List<DATABean> DATA) {
        this.DATA = DATA;
    }

    public static class DATABean {
        /**
         * order_id : 9
         * order_type : 1
         * order_type_name : Treatment
         * order_code : R#1553668613-27
         * customer_id : 5
         * customer_name : Lara A. Harding
         * pet_name : Kssk
         * pet_id : 32
         * items : [{"item_id":96,"item_name":"Service Medical B","item_type":"","item_price":"0.00","item_qty":0,"item_note":""},{"item_id":98,"item_name":"Service B","item_type":"","item_price":"0.00","item_qty":0,"item_note":""}]
         * created_at : 2019-03-27 13:36:53
         */

        @SerializedName("order_id")
        private int orderId;
        @SerializedName("order_type")
        private int orderType;
        @SerializedName("order_type_name")
        private String orderTypeName;
        @SerializedName("order_code")
        private String orderCode;
        @SerializedName("customer_id")
        private int customerId;
        @SerializedName("customer_name")
        private String customerName;
        @SerializedName("pet_name")
        private String petName;
        @SerializedName("pet_id")
        private int petId;
        @SerializedName("order_date")
        private String orderDate;
        @SerializedName("created_at")
        private String createdAt;
        @SerializedName("items")
        private List<ItemsBean> items;

        public int getOrderId() {
            return orderId;
        }

        public void setOrderId(int orderId) {
            this.orderId = orderId;
        }

        public int getOrderType() {
            return orderType;
        }

        public void setOrderType(int orderType) {
            this.orderType = orderType;
        }

        public String getOrderTypeName() {
            return orderTypeName;
        }

        public void setOrderTypeName(String orderTypeName) {
            this.orderTypeName = orderTypeName;
        }

        public String getOrderCode() {
            return orderCode;
        }

        public void setOrderCode(String orderCode) {
            this.orderCode = orderCode;
        }

        public int getCustomerId() {
            return customerId;
        }

        public void setCustomerId(int customerId) {
            this.customerId = customerId;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getPetName() {
            return petName;
        }

        public void setPetName(String petName) {
            this.petName = petName;
        }

        public int getPetId() {
            return petId;
        }

        public void setPetId(int petId) {
            this.petId = petId;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public String getOrderDate() {
            return orderDate;
        }

        public void setOrderDate(String orderDate) {
            this.orderDate = orderDate;
        }


        public static class ItemsBean {
            /**
             * item_id : 96
             * item_name : Service Medical B
             * item_type :
             * item_price : 0.00
             * item_qty : 0
             * item_note :
             */

            @SerializedName("item_order_id")
            private int itemOrderId;
            @SerializedName("item_id")
            private int itemId;
            @SerializedName("item_name")
            private String itemName;
            @SerializedName("item_type")
            private String itemType;
            @SerializedName("item_price")
            private String itemPrice;
            @SerializedName("item_qty")
            private int itemQty;
            @SerializedName("item_disc_id")
            private int itemDiscId;

            public boolean isChecked() {
                return checked;
            }

            public void setChecked(boolean checked) {
                this.checked = checked;
            }

            @SerializedName("item_note")
            private String itemNote;
            @SerializedName("checked")
            private boolean checked = false;

            public int getItemId() {
                return itemId;
            }

            public void setItemId(int itemId) {
                this.itemId = itemId;
            }

            public String getItemName() {
                return itemName;
            }

            public void setItemName(String itemName) {
                this.itemName = itemName;
            }

            public String getItemType() {
                return itemType;
            }

            public void setItemType(String itemType) {
                this.itemType = itemType;
            }

            public String getItemPrice() {
                return itemPrice;
            }

            public void setItemPrice(String itemPrice) {
                this.itemPrice = itemPrice;
            }

            public int getItemQty() {
                return itemQty;
            }

            public void setItemQty(int itemQty) {
                this.itemQty = itemQty;
            }

            public String getItemNote() {
                return itemNote;
            }

            public void setItemNote(String itemNote) {
                this.itemNote = itemNote;
            }

            public int getItemOrderId() {
                return itemOrderId;
            }

            public void setItemOrderId(int itemOrderId) {
                this.itemOrderId = itemOrderId;
            }

            public int getItemDiscId() {
                return itemDiscId;
            }

            public void setItemDiscId(int itemDiscId) {
                this.itemDiscId = itemDiscId;
            }
        }
    }
}
