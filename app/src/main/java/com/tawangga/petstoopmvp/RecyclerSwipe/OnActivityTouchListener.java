package com.tawangga.petstoopmvp.RecyclerSwipe;

import android.view.MotionEvent;

public interface OnActivityTouchListener {
    void getTouchCoordinates(MotionEvent ev);
}
