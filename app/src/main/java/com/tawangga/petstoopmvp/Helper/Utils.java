package com.tawangga.petstoopmvp.Helper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.tawangga.petstoopmvp.BuildConfig;
import com.tawangga.petstoopmvp.R;
import com.tawangga.petstoopmvp.ServerSide.Connector;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import naturalizer.separator.Separator;
import okhttp3.FormBody;
import okhttp3.RequestBody;


public class Utils {

    private static Utils instance;

    public static Utils getInstance() {
        if (instance == null)
            instance = new Utils();
        return instance;
    }

    private String[] dateName = new String[]{"SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"};
    private static String[] orderStatus = new String[]{"", "On queue", "On progress", "Done"};
    private String[] petGender = new String[]{"", "Male", "Female"};
    private String[] petRace = new String[]{"Cat", "Dog"};

    public static void showToast(Context context, String message) {
        if (context != null) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }

    }

    public static String[] getOrderStatus() {
        return orderStatus;
    }


    public String[] getDateName() {
        return dateName;
    }

    public List<String> getDummyString() {
        List<String> list = new ArrayList<>();
        list.add("");
        list.add("");
        list.add("");
        list.add("");
        list.add("");
        list.add("");
        list.add("");
        list.add("");
        list.add("");
        list.add("");
        return list;
    }

    public List<String> getDummyString3() {
        List<String> list = new ArrayList<>();
        list.add("");
        list.add("");
        list.add("");
        list.add("");
        list.add("");
        list.add("");
        list.add("");
        list.add("");
        list.add("");
        list.add("");
        list.add("");
        list.add("");
        list.add("");
        list.add("");
        return list;
    }

    public List<String> getDummyString2() {
        List<String> list = new ArrayList<>();
        list.add("");
        list.add("");
        list.add("");
        list.add("");
        return list;
    }


    public static String getNoDataAvailableString() {
        return "No data available";
    }

    public String[] getPetGender() {
        return petGender;
    }


    public static Date convertStringToDate(String dateInString) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat(GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.locale);
        Date date = null;
        try {
            date = formatter.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    public static Date convertStringToDate(String dateInString, String initFormat) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat(initFormat);
        Date date = null;
        try {
            date = formatter.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    public static String changeDateFormat(String initialDate, String previous, String toFormat) {
        if (initialDate == null) {
            return "";
        }
        if (initialDate.equals("0000-00-00") || initialDate.equals("")) {
            return "";
        }
        String convertedDate = null;
        @SuppressLint("SimpleDateFormat") SimpleDateFormat inputFormat = new SimpleDateFormat(previous, GlobalVariable.locale);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat outputFormat = new SimpleDateFormat(toFormat, GlobalVariable.locale);

        Date date;

        try {
            date = inputFormat.parse(initialDate);
            convertedDate = outputFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertedDate + "";
    }

    public static void hideSoftKeyboard(Activity activity) {
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View focusedView = activity.getCurrentFocus();
        if (focusedView != null)
            inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public static void hideSoftKeyboardUsingView(Context context, View view) {

        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }

    public static void hideSoftKeyboardFrag(Activity activity) {
        activity.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public String[] getPetRace() {
        return petRace;
    }


    public static String getTodayDate() {
        Calendar c = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat(GlobalVariable.WEB_DATE_FORMAT, GlobalVariable.locale);
        return df.format(c.getTime());
    }

    public String getTodayDay() {
        Calendar c = Calendar.getInstance();

        return getDateName()[c.get(Calendar.DAY_OF_WEEK) - 1];
    }

    public String getDayByDate(String date) {
        Date selectedDate = Utils.convertStringToDate(date);
        Calendar c = Calendar.getInstance();
        c.setTime(selectedDate);

        return getDateName()[c.get(Calendar.DAY_OF_WEEK) - 1];
    }


    public static String toRupiah(String price) {
        if (TextUtils.isEmpty(price)) {
            price = "0";
        }

        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance(GlobalVariable.locale);
        //format.setCurrency(Currency.getInstance(currency));
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("");
        formatRp.setDecimalSeparator('.');
        formatRp.setGroupingSeparator(',');

        kursIndonesia.setMinimumFractionDigits(0);
        kursIndonesia.setDecimalFormatSymbols(formatRp);
        return kursIndonesia.format(Double.parseDouble(price)) + " IDR";
    }

    public static String toRupiah2(String price) {
        if (TextUtils.isEmpty(price)) {
            price = "0";
        }

        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance(GlobalVariable.locale);
        //format.setCurrency(Currency.getInstance(currency));
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("");
        formatRp.setDecimalSeparator('.');
        formatRp.setGroupingSeparator(',');

        kursIndonesia.setMinimumFractionDigits(0);
        kursIndonesia.setDecimalFormatSymbols(formatRp);
        return "IDR " + kursIndonesia.format(Double.parseDouble(price));
    }

    public static String toMoney(String price) {
        if (TextUtils.isEmpty(price)) {
            price = "0";
        }

        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance(GlobalVariable.locale);
        //format.setCurrency(Currency.getInstance(currency));
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("");
        formatRp.setDecimalSeparator('.');
        formatRp.setGroupingSeparator(',');

        kursIndonesia.setMinimumFractionDigits(0);
        kursIndonesia.setDecimalFormatSymbols(formatRp);
        return kursIndonesia.format(Double.parseDouble(price));
    }


    public static ProgressDialog showProgressDialog(Context context, boolean cancelable, String message) {
        if (context != null && (context instanceof Activity) && !((Activity) context).isFinishing()) {
            ProgressDialog progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(cancelable);
            progressDialog.show();
            return progressDialog;
        } else
            return null;
    }


    public static void dismissProgressDialog(ProgressDialog progressDialog) {
        if (progressDialog != null && progressDialog.isShowing()) {
            try {
                progressDialog.dismiss();
            } catch (Exception e) {
                if (BuildConfig.DEBUG) e.printStackTrace();
            }
        }
    }

    public static void setLocale(Locale locale, Context context) {
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config, null);
    }

    public static class et_Money implements TextWatcher {
        EditText et_currency;
        String current = "";

        public et_Money(EditText et_currency) {
            this.et_currency = et_currency;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (!editable.toString().equals(current)) {
                et_currency.removeTextChangedListener(this);
                String currentNum;
                if (editable.length() < 5) {
                    currentNum = editable.toString();
                } else {
                    String y = editable.toString().replaceAll("[^0-9]", "");
                    currentNum = y;
                }
                Log.d("base_price", currentNum);
                current = Separator.getInstance().doSeparate(currentNum, Locale.GERMANY);
                if (!current.equals("")) {
                    et_currency.setText(String.format("%s IDR", current));
                } else {
                    et_currency.setText(String.format("%s", current));
                }
                et_currency.setSelection(current.length());
                et_currency.addTextChangedListener(this);
            }
        }
    }

    public static double getNumFromCurrency(String value) {
        String currentNum;
        if (value.length() < 5) {
            if (value.length() == 0) {
                currentNum = "0";
            } else currentNum = value;
        } else {
            currentNum = value.replaceAll("[^0-9]", "");
        }
        return Double.parseDouble(currentNum);
    }

    public static class setswitch implements Switch.OnCheckedChangeListener {

        Context context;
        TextView tv;
        EditText et;

        public setswitch(Context context, TextView tv, EditText et) {
            this.context = context;
            this.tv = tv;
            this.et = et;
        }

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean ischecked) {
            if (ischecked) {
                tv.setTextColor(Color.BLACK);
                et.setFocusable(true);
                et.setFocusableInTouchMode(true);
            } else {
                tv.setTextColor(Color.GRAY);
                if (!et.getText().toString().equals("0 IDR")) {
                    et.setText("0");
                }
                et.setFocusable(false);
                et.setFocusableInTouchMode(false);
                hideSoftKeyboardUsingView(context, et);
            }

        }
    }

    public static void cancelInvoice(String trID, Context context, String TAG) {
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add("transaction_id", trID);
        RequestBody formBody = formBuilder.build();
        ProgressDialog dialog = Utils.showProgressDialog(context, false, "Loading...");
        Connector.newInstance(context).cancelInvoice(formBody, new Connector.ApiCallback<String>() {
            @Override
            public String getKey() {
                return TAG;
            }

            @Override
            public void success(String s, String messages) {
                Utils.dismissProgressDialog(dialog);
            }

            @Override
            public void onFailure(Throwable t) {
                Utils.dismissProgressDialog(dialog);
                Log.e("error_cancelInvoice", t.getMessage());
            }
        });
    }

    public static Date addDayToDate(Date date, int day) {
        Date dt = date;
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, day);
        dt = c.getTime();
        return dt;
    }

    public static String convertDateToString(Date date, String pattern) {
        DateFormat dateFormat = new SimpleDateFormat(pattern);
        String strDate = dateFormat.format(date);
        return strDate;
    }



    public static boolean isEmailValid(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean isPhoneValid(String phone) {
        if (phone == null || phone.length() < 6 || phone.length() > 14) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(phone).matches();
        }
    }
}
