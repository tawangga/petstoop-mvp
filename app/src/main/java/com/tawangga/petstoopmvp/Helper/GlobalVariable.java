package com.tawangga.petstoopmvp.Helper;

import java.util.Locale;

public class GlobalVariable {
    public static String APP_TOKEN = "PeTShOpQue";
//    public static String ROOT_URL = "http://178.128.62.50/petv2/api/public/api/";
    public static String ROOT_URL = "http://192.168.1.82/api/";


    public static final String WEB_DATE_FORMAT = "yyyy-MM-dd";
    public static final String DATE_FORMAT_MONTH_NAME_FULL = "dd MMMM yyyy";
    public static final String DATE_FORMAT_MONTH_NAME = "dd MMM yyyy";
    public static final String DATE_FORMAT_MONTH_STANDARD = "dd-MM-yyyy";
    public static final String TIME_FORMAT_STANDARD = "HH:mm:ss";
    public static final String TIME_FORMAT_STANDARD_2 = "HH:mm";
    public static final String TIME_FORMAT_12H = "hh:mm aa";
    public static final int LIMIT_ITEM = 15;
    public static final int LIMIT_ITEM_DOCTOR = 1;
    public static final int LIMIT_ITEM_MAX = 9999;

    public static Locale locale = Locale.US;
}
