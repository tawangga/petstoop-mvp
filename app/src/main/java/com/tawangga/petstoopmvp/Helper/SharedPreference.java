package com.tawangga.petstoopmvp.Helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.tawangga.petstoopmvp.Model.FindSite;
import com.tawangga.petstoopmvp.ModelNew.GetClients;
import com.tawangga.petstoopmvp.ModelNew.GetSite;


public class SharedPreference {
    private static final String STAFF_CATEGORY = "STAFF_CATEGORY";
    private static final String STAFF_CODE = "STAFF_CODE";
    private static final String PHOTO_OLD = "PHOTO_OLD";
    private static final String SAVED_CLIENT_SITE = "SAVED_CLIENT_SITE";
    private final SharedPreferences appPreference;
    private static final String STAFF_USERNAME = "STAFF_USERNAME";
    private static final String STAFF_ID = "STAFF_ID";
    private static final String STAFF_NAME = "STAFF_NAME";
    private static final String STAFF_EMAIL = "STAFF_EMAIL";
    private static final String STAFF_PHOTO = "STAFF_PHOTO";
    private static final String STAFF_TOKEN = "STAFF_TOKEN";
    private static final String STAFF_PHONE = "STAFF_PHONE";
    private static final String STAFF_PASSWORD = "STAFF_PASSWORD";
    private static final String SITE = "SITE";
    private static final String SAVED_SITE = "SAVED_SITE";
    private static final String SAVED_CLIENT = "SAVED_CLIENT";

    public SharedPreference(Context context) {
        appPreference = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setSession(String key, String value) {
        SharedPreferences.Editor editor = appPreference.edit();
        editor.putString(key, value);
        editor.apply();

    }

    public void clearSession() {
        SharedPreferences.Editor editor = appPreference.edit();
//        editor.clear();
        editor.remove(STAFF_CATEGORY);
        editor.remove(STAFF_CODE);
        editor.remove(PHOTO_OLD);
        editor.remove(STAFF_USERNAME);
        editor.remove(STAFF_ID);
        editor.remove(STAFF_NAME);
        editor.remove(STAFF_EMAIL);
        editor.remove(STAFF_PHOTO);
        editor.remove(STAFF_TOKEN);
        editor.remove(STAFF_PHOTO);
        editor.remove(STAFF_PASSWORD);
        editor.remove(SITE);
        editor.remove("IS_LOGIN");
        editor.apply();
//        editor.commit();
    }

    public String getSession(String key) {
        String value = appPreference.getString(key, "");
        if (TextUtils.isEmpty(value)) {
            return "";
        }
        return value;
    }

    public void setIsLogin(boolean stat) {
        SharedPreferences.Editor editor = appPreference.edit();
        editor.putBoolean("IS_LOGIN", stat);
        editor.apply();
    }

    public boolean getIsLogin() {
        boolean isLogin = appPreference.getBoolean("IS_LOGIN", false);
        return isLogin;
    }

    public void setStaffUsername(String staffUsername){
        setSession(STAFF_USERNAME,staffUsername);
    }

    public String getStaffUsername(){
        return getSession(STAFF_USERNAME);
    }

    public void setStaffId(String staffId){
        setSession(STAFF_ID,staffId);
    }

    public String getStaffId(){
        return getSession(STAFF_ID);
    }

    public void setStaffName(String staffName){
        setSession(STAFF_NAME,staffName);
    }

    public String getStaffName() {
        return getSession(STAFF_NAME);
    }

    public void setStaffEmail(String staffEmail){
        setSession(STAFF_EMAIL,staffEmail);
    }

    public String getStaffEmail() {
        return getSession(STAFF_EMAIL);
    }

    public void setStaffPhoto(String staffPhoto){
        setSession(STAFF_PHOTO,staffPhoto);
    }

    public String getStaffPhoto() {
        return getSession(STAFF_PHOTO);
    }

    public void setStaffToken(String staffToken){
        setSession(STAFF_TOKEN,staffToken);
    }

    public String getStaffToken() {
        return getSession(STAFF_TOKEN);
    }

    public void setStaffPhone(String staffPhone){
        setSession(STAFF_PHONE,staffPhone);
    }

    public String getStaffPhone() {
        return getSession(STAFF_PHONE);
    }

    public void setStaffPassword(String staffPassword){
        setSession(STAFF_PASSWORD,staffPassword);
    }

    public String getStaffPassword() {
        return getSession(STAFF_PASSWORD);
    }

    public void setStaffCategory(String staff_category) {
        setSession(STAFF_CATEGORY,staff_category);
    }

    public boolean isDoctor(){
        if (getSession(STAFF_CATEGORY).equals("Doctor")){
            return true;
        }else {
            return false;
        }
    }

    public void setStaffCode(String staffCode) {
        setSession(STAFF_CODE,staffCode);

    }

    public String getStaffCode() {
        return getSession(STAFF_CODE);
    }

    public void setPhotoOld(String photoOld) {
        setSession(PHOTO_OLD,photoOld);

    }


    public String getPhotoOld() {
        return getSession(PHOTO_OLD);
    }

    public void setSite(String site){
        setSession(SITE,site);
    }

    public String getSite(){
        return getSession(SITE);
    }

    public void setSavedSite(String siteId){
        setSession(SAVED_SITE,siteId);
    }

    public GetSite.DATABean.ItemsBean getSavedSite(){
        String json = getSession(SAVED_SITE);
        if (json.equals("")){
            return null;
        }
        return new Gson().fromJson(json, GetSite.DATABean.ItemsBean.class);
    }


    public void setSavedClient(String siteId){
        setSession(SAVED_CLIENT,siteId);
    }

    public void setSavedClientSite(String siteId){
        setSession(SAVED_CLIENT_SITE,siteId);
    }

    public FindSite.DATABean getSavedClientSite(){
        String json = getSession(SAVED_CLIENT_SITE);
        if (json.equals("")){
            return null;
        }
        return new Gson().fromJson(json, FindSite.DATABean.class);

    }

    public GetClients.DATABean.ItemsBean getSavedClient(){
        String json = getSession(SAVED_CLIENT);
        if (json.equals("")){
            return null;
        }
        return new Gson().fromJson(json, GetClients.DATABean.ItemsBean.class);
    }

}
