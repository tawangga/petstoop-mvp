package com.tawangga.petstoopmvp.Helper;

import com.mazenrashed.printooth.data.printable.Printable;
import com.mazenrashed.printooth.data.printable.TextPrintable;
import com.mazenrashed.printooth.data.printer.DefaultPrinter;

import java.util.ArrayList;

public class PrintHelper {
    private ArrayList<Printable> printables;

    public static byte ALIGN_LEFT = DefaultPrinter.Companion.getALIGNMENT_LEFT();
    public static byte ALIGN_CENTER = DefaultPrinter.Companion.getALIGNMENT_CENTER();
    public static byte ALIGN_RIGHT = DefaultPrinter.Companion.getALIGNMENT_RIGHT();

    public PrintHelper() {
        printables = new ArrayList<>();
    }

    public PrintHelper setHeader(String text) {
        printables.add(new TextPrintable.Builder().setText(text + "\n")
                .setEmphasizedMode(DefaultPrinter.Companion.getEMPHASIZED_MODE_BOLD())
                .setAlignment(ALIGN_CENTER)
                .setFontSize(DefaultPrinter.Companion.getFONT_SIZE_LARGE())
                .setLineSpacing(DefaultPrinter.Companion.getLINE_SPACING_30())
                .build());
        return this;
    }

    public PrintHelper setDesc(String text) {
        printables.add(new TextPrintable.Builder().setText(text + "\n")
                .setAlignment(ALIGN_CENTER)
                .setFontSize(DefaultPrinter.Companion.getFONT_SIZE_NORMAL())
                .build());
        return this;
    }

    public PrintHelper line() {
        printables.add(new TextPrintable.Builder().setText("================================================\n")
                .setAlignment(ALIGN_CENTER)
                .setFontSize(DefaultPrinter.Companion.getFONT_SIZE_NORMAL())
                .build());
        return this;
    }

    public PrintHelper setBody(String text, Byte aligment) {
        return setBody(text, aligment, false);
    }

    public PrintHelper setBody(String text, Byte aligment, boolean underline) {

        printables.add(new TextPrintable.Builder().setText(text)
                .setAlignment(aligment)
                .setFontSize(DefaultPrinter.Companion.getFONT_SIZE_NORMAL())
                .setUnderlined(underline ?
                        DefaultPrinter.Companion.getUNDERLINED_MODE_ON() :
                        DefaultPrinter.Companion.getUNDERLINED_MODE_OFF())
                .build());
        return this;
    }

    public ArrayList<Printable> build() {
        return printables;
    }

    public static String padRight(String s, int n) {
        return String.format("%-" + n + "s", s);
    }

    public static String padLeft(String s, int n) {
        return String.format("%" + n + "s", s);
    }

    public PrintHelper space(int i) {
        printables.add(new TextPrintable.Builder().setText("")
                .setNewLinesAfter(3)
                .build());
        return this;
    }
}
