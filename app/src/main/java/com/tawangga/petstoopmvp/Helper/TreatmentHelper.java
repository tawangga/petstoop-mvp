package com.tawangga.petstoopmvp.Helper;


import com.tawangga.petstoopmvp.Model.CartModel;
import com.tawangga.petstoopmvp.Model.DetailCustomerModel;
import com.tawangga.petstoopmvp.Model.GetOrderConfirmAnamnesis;
import com.tawangga.petstoopmvp.ModelNew.CreateCart;

public class TreatmentHelper {
    private static TreatmentHelper instance;
    private boolean isOrder;
    private boolean isShop;
    private DetailCustomerModel.DATABean selectedCustomer;
    private CartModel.DATABean cart;
    private DetailCustomerModel.DATABean.PetsBean selectedPet;
    private CreateCart.DATABean dataTreatment;
    private GetOrderConfirmAnamnesis.DATABean savedAnamnesis;


    public static TreatmentHelper getInstance() {
        if(instance == null)
            instance = new TreatmentHelper();
        return instance;
    }

    public boolean isOrder() {
        return isOrder;
    }

    public void setOrder(boolean order) {
        isOrder = order;
    }

    public boolean isShop() {
        return isShop;
    }

    public void setShop(boolean shop) {
        isShop = shop;
    }

    public DetailCustomerModel.DATABean getSelectedCustomer() {
        return selectedCustomer;
    }

    public void setSelectedCustomer(DetailCustomerModel.DATABean selectedCustomer) {
        this.selectedCustomer = selectedCustomer;
    }

    public void setCart(CartModel.DATABean dataBean) {

        cart = dataBean;
    }

    public CartModel.DATABean getCart() {
        return cart;
    }

    public void setselectedPet(DetailCustomerModel.DATABean.PetsBean selectedPet) {

        this.selectedPet = selectedPet;
    }

    public DetailCustomerModel.DATABean.PetsBean getSelectedPet() {
        return selectedPet;
    }

    public void setDataTreatment(CreateCart.DATABean dataTreatment) {

        this.dataTreatment = dataTreatment;
    }

    public CreateCart.DATABean getDataTreatment() {
        return dataTreatment;
    }

    public void setSavedAnamnesis(GetOrderConfirmAnamnesis.DATABean savedAnamnesis) {

        this.savedAnamnesis = savedAnamnesis;
    }

    public GetOrderConfirmAnamnesis.DATABean getSavedAnamnesis() {
        return savedAnamnesis;
    }
}
