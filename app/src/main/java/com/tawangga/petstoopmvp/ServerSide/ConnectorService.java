package com.tawangga.petstoopmvp.ServerSide;

import com.tawangga.petstoopmvp.Model.BoardingModel;
import com.tawangga.petstoopmvp.Model.CartModel;
import com.tawangga.petstoopmvp.Model.CustomerModel;
import com.tawangga.petstoopmvp.Model.DetailCustomerModel;
import com.tawangga.petstoopmvp.Model.DoctorModel;
import com.tawangga.petstoopmvp.Model.FindSite;
import com.tawangga.petstoopmvp.Model.GetErrorMessage;
import com.tawangga.petstoopmvp.Model.GetExamModel;
import com.tawangga.petstoopmvp.Model.GetInvoice;
import com.tawangga.petstoopmvp.Model.GetInvoiceDP;
import com.tawangga.petstoopmvp.Model.GetOrderConfirmAnamnesis;
import com.tawangga.petstoopmvp.Model.GetOrders;
import com.tawangga.petstoopmvp.Model.GetReservation;
import com.tawangga.petstoopmvp.Model.GroomingLocationModel;
import com.tawangga.petstoopmvp.Model.GroomingModel;
import com.tawangga.petstoopmvp.Model.MedicalServiceModel;
import com.tawangga.petstoopmvp.Model.PetListModel;
import com.tawangga.petstoopmvp.Model.ProcessInvoice;
import com.tawangga.petstoopmvp.Model.ProductCategories;
import com.tawangga.petstoopmvp.Model.ProductModel;
import com.tawangga.petstoopmvp.Model.PromoModel;
import com.tawangga.petstoopmvp.Model.SignInModel;
import com.tawangga.petstoopmvp.Model.TreatmentAddCustomer;
import com.tawangga.petstoopmvp.Model.UpdateStaffModel;
import com.tawangga.petstoopmvp.ModelNew.AddServiceTreatment;
import com.tawangga.petstoopmvp.ModelNew.CreateCart;
import com.tawangga.petstoopmvp.ModelNew.GetAppVersion;
import com.tawangga.petstoopmvp.ModelNew.GetBoarding;
import com.tawangga.petstoopmvp.ModelNew.GetBoardingTreatmentRecord;
import com.tawangga.petstoopmvp.ModelNew.GetClients;
import com.tawangga.petstoopmvp.ModelNew.GetCurrentVersion;
import com.tawangga.petstoopmvp.ModelNew.GetDoctor;
import com.tawangga.petstoopmvp.ModelNew.GetDoctorSchedule;
import com.tawangga.petstoopmvp.ModelNew.GetGroomingLocation;
import com.tawangga.petstoopmvp.ModelNew.GetGroomingService;
import com.tawangga.petstoopmvp.ModelNew.GetGroomingTreatmentRecord;
import com.tawangga.petstoopmvp.ModelNew.GetMedicalService;
import com.tawangga.petstoopmvp.ModelNew.GetMedicalTreatmentRecord;
import com.tawangga.petstoopmvp.ModelNew.GetSite;
import com.tawangga.petstoopmvp.ModelNew.GetTreatment;
import com.tawangga.petstoopmvp.ModelNew.GetTreatmentRecord;
import com.tawangga.petstoopmvp.Param.AddCartToInvoice;
import com.tawangga.petstoopmvp.Param.AddMedicalService;
import com.tawangga.petstoopmvp.Param.CancelMedicalServiceParam;
import com.tawangga.petstoopmvp.Param.CancelServiceParam;
import com.tawangga.petstoopmvp.Param.ConvertToOrderParam;
import com.tawangga.petstoopmvp.Param.SaveConfirmOrderAnamnesis;

import java.util.HashMap;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;
import rx.Observable;

interface ConnectorService {
    @POST("login")
    Observable<SignInModel> loginPost(@Body RequestBody body);

    @GET("customers/pet")
    Observable<PetListModel> getPetList(@Query("limit") String limit,
                                        @Query("page") String page,
                                        @Query("search") String search);
    @GET("customers-pet")
    Observable<PetListModel> getCustomerPage(@Query("limit") String limit,
                                             @Query("page") String page,
                                             @Query("search") String search);

    @GET("customers")
    Observable<CustomerModel> getCustomerList(@Query("limit") String limit,
                                              @Query("page") String page,
                                              @Query("search") String search);

    @GET("customers")
    Observable<CustomerModel> getCustomerListByPet(@Query("limit") String limit,
                                                   @Query("page") String page,
                                                   @Query("search") String search,
                                                   @Query("pet") String pet);

    @GET("products")
    Observable<ProductModel> getProduct(@Query("category_id") String category_id,
                                        @Query("limit") String limit,
                                        @Query("page") String page,
                                        @Query("search") String search,
                                        @Query("is_mobile") String isMobile,
                                        @Query("except_category_id") String except_category_id);

    @GET("products/categories")
    Observable<ProductCategories> getProdCategory();

    @Multipart
    @POST
    Observable<DetailCustomerModel> addCustomer(@Url String url,
                                                @PartMap HashMap<String, RequestBody> data,
                                                @Part MultipartBody.Part file);

    @GET
    Observable<DetailCustomerModel> detailCustomer(@Url String url);

    @Multipart
    @POST
    Observable<GetErrorMessage> addPet(
            @Url String url,
            @PartMap HashMap<String, RequestBody> data,
            @Part MultipartBody.Part file);

    @POST("cart/product")
    Observable<CartModel> createCart(@Body RequestBody body);

    @GET
    Observable<CartModel> getCartMedicalItems(@Url String body);

    @GET
    Observable<GetTreatmentRecord> getPetTreatmentRecord(@Url String body);

    @GET
    Observable<GetMedicalTreatmentRecord> getPetMedicalTreatmentRecord(@Url String body, @QueryMap Map<String, String> param);

    @GET
    Observable<GetGroomingTreatmentRecord> getPetGroomingTreatmentRecord(@Url String body, @QueryMap Map<String, String> param);

    @GET
    Observable<GetBoardingTreatmentRecord> getPetBoardingTreatmentRecord(@Url String body, @QueryMap Map<String, String> param);

    @GET("promos")
    Observable<PromoModel> getPromo(@Query("limit") String limit,
                                    @Query("page") String page,
                                    @Query("search") String search);

    @POST("cart/product/add")
    Observable<GetErrorMessage> addProductToCart(@Body RequestBody body);

    @POST("doctor")
    Observable<DoctorModel> getDoctor(@Body RequestBody formBody);

    @GET("doctors")
    Observable<GetDoctor> getDoctor(@QueryMap Map<String, String> param);

    @GET("doctor-list")
    Observable<DoctorModel> getAllDoctor();

    @GET("doctors/schedules")
    Observable<GetDoctorSchedule> getAllDoctor(@QueryMap Map<String, String> param);

    @GET("grooming-location")
    Observable<GroomingLocationModel> getGroomingLocation();

    @GET("groomings/locations")
    Observable<GetGroomingLocation> getGroomingLocation(@QueryMap Map<String, String> param);

    @GET("boarding")
    Observable<BoardingModel> getBoarding();

    @GET("cages")
    Observable<GetBoarding> getBoarding(@QueryMap Map<String, String> param);

    @GET("medical-service")
    Observable<MedicalServiceModel> getMedicalService();

    @GET("grooming-service")
    Observable<GroomingModel> getGrooming();

    @GET("groomings")
    Observable<GetGroomingService> getGroomingService(@QueryMap Map<String, String> param);

    @GET("settings/versions")
    Observable<GetAppVersion> getAppVersion(@QueryMap Map<String, String> param);

    @GET("medicals")
    Observable<GetMedicalService> getMedicalService(@QueryMap Map<String, String> param);

    @POST("treatment-add-customer")
    Observable<TreatmentAddCustomer> treatmentAddCustomer(@Body RequestBody requestBody);

    @POST("cart/service")
    Observable<CreateCart> createCartTreatment(@Body RequestBody requestBody);

    @POST("add-service/1")
    Observable<GetErrorMessage> treatmentAddService(@Body AddMedicalService requestBody);

    @POST("cart/service/add")
    Observable<AddServiceTreatment> addServiceTreatment(@Body RequestBody requestBody);

    @POST("doctor/cart/service/add")
    Observable<AddServiceTreatment> addServiceTreatmentDoctor(@Body RequestBody requestBody);

    @POST("delete-service-detail")
    Observable<GetErrorMessage> deleteServiceDetail(@Body RequestBody requestBody);

    @POST("cart/product/clean")
    Observable<GetErrorMessage> deleteShopCart(@Body RequestBody requestBody);

    @POST("cart/service/delete")
    Observable<GetErrorMessage> deleteServiceTreatment(@Body RequestBody requestBody);

    @POST
    Observable<GetErrorMessage> deleteMedicalItems(@Url String url, @Body RequestBody requestBody);

    @POST("boarding-delete")
    Observable<GetErrorMessage> deleteBoarding(@Body RequestBody requestBody);

    @POST("cart/service/reservation-to-order")
    Observable<GetErrorMessage> convertToOrder(@Body ConvertToOrderParam requestBody);

    @POST("add-reservation")
    Observable<TreatmentAddCustomer> addToReservation(@Body RequestBody requestBody);

    @POST("cart/service/reservation")
    Observable<CreateCart> reservTreatment(@Body RequestBody requestBody);

    @POST("change-date-treatment")
    Observable<TreatmentAddCustomer> treatmentChangeDate(@Body RequestBody requestBody);

    @POST("add-order")
    Observable<TreatmentAddCustomer> addToOrder(@Body RequestBody requestBody);

    @GET("cart/service")
    Observable<GetTreatment> getReservation(@QueryMap Map<String, String> param);

    @POST("treatment/order")
    Observable<GetReservation> getOrder(@Body RequestBody requestBody);

    @POST("cart/service/set-status")
    Observable<GetErrorMessage> changeOrderStatus(@Body RequestBody requestBody);

    @GET
    Observable<GetErrorMessage> deleteService(@Url String url);

    @GET
    Observable<GetOrderConfirmAnamnesis> getOrderConfirmAnamnesis(@Url String url, @Query("is_record") String is_record);

    @POST
    Observable<GetErrorMessage> saveOrderConfirmAnamnesis(@Url String url, @Body SaveConfirmOrderAnamnesis body);

    @POST("transaction/invoice/services")
    Observable<GetInvoice> addToInvoice(@Body RequestBody requestBody);

    @POST("transaction/invoice/products")
    Observable<GetInvoice> createInvoiceProduct(@Body RequestBody body);

    @GET("orders")
    Observable<GetOrders> getOrders(@Query("order_type") String order_type);

    @POST("transaction/invoice/orders")
    Observable<GetInvoice> addOrderToInvoice(@Body AddCartToInvoice requestBody);

    @POST("transaction/remove/order")
    Observable<GetInvoice> removeOrderInvoice(@Body RequestBody requestBody);

    @POST("transaction/remove/order/item")
    Observable<GetInvoice> removeOrderDetailInvoice(@Body RequestBody requestBody);

    @POST("transaction/cancel")
    Observable<GetErrorMessage> cancelInvoice(@Body RequestBody requestBody);

    @POST("transaction/payment/step1")
    Observable<ProcessInvoice> processInvoice(@Body RequestBody requestBody);

    @POST("transaction/payment/step2")
    Observable<GetErrorMessage> processInvoice2(@Body RequestBody requestBody);


    @GET("transaction/invoice-dp")
    Observable<GetInvoiceDP> getInvoiceDP();


    @GET("doctor/cart/service")
    Observable<GetTreatment> getDoctorReservation(@QueryMap Map<String, String> formBody);

    @GET
    Observable<GetOrderConfirmAnamnesis> getOrderConfirmAnamnesisDoctor(@Url String url, @Query("is_record") String is_record);

    @POST("cart/product/delete")
    Observable<GetErrorMessage> deleteProductCart(@Body RequestBody body);

    @POST("doctor/cart/service/anamnesis/{cart_service_id}/medical-items/delete")
    Observable<GetErrorMessage> deleteProductCartDoctor(@Body RequestBody body, @Path("cart_service_id") String cart_service_id);

    @POST
    Observable<GetErrorMessage> addMedicalItemToCart(@Url String url, @Body RequestBody body);

    @Multipart
    @POST("staffs/update")
    Observable<UpdateStaffModel> updateStaff(@PartMap Map<String, RequestBody> body,
                                             @Part MultipartBody.Part file);

    @GET("doctor/exams")
    Observable<GetExamModel> getExam(@QueryMap Map<String, String> param);

    @GET("clients")
    Observable<GetClients> getClient();

    @GET("clients/{client_id}/sites")
    Observable<GetSite> getSite(@Path("client_id") String client_id);

    @POST("find-site")
    Observable<FindSite> findSite(@Body RequestBody requestBody);

    @POST("cart/service/rollback")
    Observable<GetErrorMessage> rollbackService(@Body CancelServiceParam param);

    @POST("doctor/cart/service/anamnesis/medical-items/rollback")
    Observable<GetErrorMessage> rollbackMedicalItem(@Body CancelMedicalServiceParam param);

    @GET("versions/latest")
    Observable<GetCurrentVersion> getVersion();

}
