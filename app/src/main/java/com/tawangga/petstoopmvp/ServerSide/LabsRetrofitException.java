package com.tawangga.petstoopmvp.ServerSide;


import com.tawangga.petstoopmvp.BuildConfig;
import com.tawangga.petstoopmvp.Model.GetErrorMessage;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * This will handle retrofit error and erturn them as throwable
 * http://bytes.babbel.com/en/articles/2016-03-16-retrofit2-rxjava-error-handling.html
 */
public class LabsRetrofitException extends RuntimeException {
    public static LabsRetrofitException httpError(String url, Response response, Retrofit retrofit) {
        String message = response.code() + " " + response.message();
        if (response.errorBody() != null) {
            Converter<ResponseBody, GetErrorMessage> converter = retrofit.responseBodyConverter(GetErrorMessage.class, new Annotation[0]);
            try {
                final ResponseBody errorBody = response.errorBody();
                GetErrorMessage error = converter.convert(errorBody);
                message = error.getMESSAGE();

                /*if (error.get("errors") != null) {
                    if (error.get("errors").isJsonPrimitive()) {
                        message = error.get("errors").getAsString();
                    } else if (error.get("errors").isJsonObject()) {
                        JsonObject errorObj = error.get("errors").getAsJsonObject();
                        message = errorObj.get("message").getAsString();
                    }
                }
                if (error.get("error") != null) {
                    if (error.get("error").isJsonPrimitive()) {
                        message = error.get("error").getAsString();
                    } else if (error.get("error").isJsonObject()) {
                        JsonObject errorObj = error.get("error").getAsJsonObject();
                        message = errorObj.get("message").getAsString();
                    }
                }*/
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace();
                }
            }
        }

        return new LabsRetrofitException(message, url, response, Kind.HTTP, null, retrofit);
    }

    public static LabsRetrofitException networkOffline(NoConnectivityException exception) {
        return new LabsRetrofitException(exception.getMessage() , null, null, Kind.NETWORK, exception, null);
    }

    public static LabsRetrofitException networkError(IOException exception) {
        return new LabsRetrofitException("Failed to connect to server, please check internet connection", null, null, Kind.NETWORK, exception, null);
    }

    public static LabsRetrofitException unexpectedError(Throwable exception) {
        return new LabsRetrofitException(exception.getMessage(), null, null, Kind.UNEXPECTED, exception, null);
    }

    /**
     * Identifies the event kind which triggered a {@link LabsRetrofitException}.
     */
    public enum Kind {
        /**
         * An {@link IOException} occurred while communicating to the server.
         */
        NETWORK,
        /**
         * A non-200 HTTP status code was received from the server.
         */
        HTTP,
        /**
         * An internal error occurred while attempting to execute a request. It is best practice to
         * re-throw this exception so your application crashes.
         */
        UNEXPECTED
    }

    private final String url;
    private final Response response;
    private final Kind kind;
    private final Retrofit retrofit;

    private LabsRetrofitException(String message, String url, Response response, Kind kind, Throwable exception, Retrofit retrofit) {
        super(message, exception);
        this.url = url;
        this.response = response;
        this.kind = kind;
        this.retrofit = retrofit;
    }

    /**
     * The request URL which produced the error.
     */
    public String getUrl() {
        return url;
    }

    /**
     * Response object containing status code, headers, body, etc.
     */
    public Response getResponse() {
        return response;
    }

    /**
     * The event kind which triggered this error.
     */
    public Kind getKind() {
        return kind;
    }

    /**
     * The Retrofit this request was executed on
     */
    public Retrofit getRetrofit() {
        return retrofit;
    }

    /**
     * HTTP response body converted to specified {@code type}. {@code null} if there is no
     * response.
     *
     * @throws IOException if unable to convert the body to the specified {@code type}.
     */
    public <T> T getErrorBodyAs(Class<T> type) throws IOException {
        if (response == null || response.errorBody() == null) {
            return null;
        }
        Converter<ResponseBody, T> converter = retrofit.responseBodyConverter(type, new Annotation[0]);
        return converter.convert(response.errorBody());
    }
}

