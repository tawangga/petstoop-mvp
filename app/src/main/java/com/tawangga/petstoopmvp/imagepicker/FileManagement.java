package com.tawangga.petstoopmvp.imagepicker;

import android.content.Context;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

public class FileManagement {
    Context context;
    ByteArrayOutputStream byteArrayOutputStream;

    public FileManagement(Context context, ByteArrayOutputStream byteArrayOutputStream) {
        this.context = context;
        this.byteArrayOutputStream = byteArrayOutputStream;
    }

    public File getTempFileImage() {
        return new File(getRealPathUri(getUriImage()));
    }

    private Uri getUriImage() {
        return Uri.fromFile(getFileImage());
    }

    private File getFileImage() {
        File tempDirectory = new File(Environment.getExternalStorageDirectory().toString() + File.separator + "Temp");
        if (!tempDirectory.exists()) {
            if (!tempDirectory.mkdirs()) {
                return null;
            }
        }
        File temp = new File(tempDirectory.getPath() + File.separator + "temp" + new Date().getTime() + ".jpg");

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(temp);
            fileOutputStream.write(byteArrayOutputStream.toByteArray());
            fileOutputStream.close();

            MediaScannerConnection.scanFile(context, new String[]{temp.getPath()}, new String[]{"image/jpg"}, null);
        } catch (IOException e) {
        }
        return temp;
    }

    private String getRealPathUri(Uri uri) {
        String resultPath = null;
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) {
            resultPath = uri.getPath();
        } else {
            if (cursor.moveToFirst()) {
                int indexPath = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                resultPath = cursor.getString(indexPath);
            }
            cursor.close();
        }
        return resultPath;
    }
}

