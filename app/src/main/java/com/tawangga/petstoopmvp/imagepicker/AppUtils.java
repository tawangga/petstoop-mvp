package com.tawangga.petstoopmvp.imagepicker;

import android.app.Activity;
import android.net.Uri;
import android.os.Environment;

import java.io.File;
import java.io.IOException;

//import codelabs.siloam.utils.MyLog;

/**
 * Created by HIMANGI--- Patel on 31/1/18.
 */

public class AppUtils {

  static File getWorkingDirectory(Activity activity) {
    /*File directory =
        new File(Environment.getExternalStorageDirectory(), activity.getApplicationContext().getPackageName());
      if (!directory.exists()) {
          directory.mkdirs();
      }

      MyLog.logE("image : "+directory);*/
    File directory = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

//    File directory = new File(activity.getFilesDir(), "images");

//    MyLog.logE("directory : "+directory);
    return directory;
  }

  static FileUri createImageFile(Activity activity, String prefix) {
    FileUri fileUri = new FileUri();

    File image = null;
    try {
      image = File.createTempFile(prefix + String.valueOf(System.currentTimeMillis()), ".jpg",
          getWorkingDirectory(activity));
    } catch (IOException e) {
      e.printStackTrace();
    }

    if (image != null) {
      fileUri.setFile(image);
      //
//      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        /*fileUri.setImageUrl(FileProvider.getUriForFile(activity,
                  "com.misteraladin.android.provider",
                image));*/

      /*fileUri.setImageUrl(FileProvider.getUriForFile(activity,
              BuildConfig.APPLICATION_ID,
              image));*/
//      }

      //else {
        fileUri.setImageUrl(Uri.parse("file:" + image.getAbsolutePath()));
      //}
    }
    return fileUri;
  }
}
