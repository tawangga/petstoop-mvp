package com.tawangga.petstoopmvp.imagepicker;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class AdjustBitmap {
    Context context;
    Bitmap bitmap;
    Uri uri;

    public AdjustBitmap(Context context , Uri uri) {
        this.context = context;
        this.bitmap = null;
        this.uri = uri;
    }

    public AdjustBitmap adjustBitmap() {
        Bitmap adjustedBitmap = bitmap;
        try {
            ExifInterface exifInterface = new ExifInterface(getRealPathUri(uri));
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int degrees = getDegrees(orientation);

            Matrix matrix = new Matrix();
            if (degrees != 0f) {
                matrix.preRotate(degrees);
            }
            adjustedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        bitmap = adjustedBitmap;
        return this;
    }

    public AdjustBitmap resizeBitmap() {
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        bitmap = imageLoader.loadImageSync(uri.toString());
        return this;
    }

    public Bitmap getBitmap(){
        return  bitmap;
    }

    private String getRealPathUri(Uri uri) {
        String resultPath = null;
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) {
            resultPath = uri.getPath();
        } else {
            if (cursor.moveToFirst()) {
                int indexPath = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                resultPath = cursor.getString(indexPath);
            }
            cursor.close();
        }
        return resultPath;
    }

    private int getDegrees(int orientation) {
        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }
}